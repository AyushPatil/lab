package com.labware.lab.fragments
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import com.labware.lab.R
import com.labware.lab.adapters.CustomizedExpandableTestsListAdapter
import com.labware.lab.models.MedicalTestShowEvent
import com.labware.lab.models.MedicalTests
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class TestsFragment : Fragment() {

    var testsExpandableListView: ExpandableListView? = null
    var expandableListAdapter: ExpandableListAdapter? = null
    var expandableTitleList: ArrayList<String>? = null
    var expandableDetailList: HashMap<String, List<String>>? = null
    var mContext: Context? = null
    var medicalTests: ArrayList<MedicalTests>? = null

    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view: View = inflater.inflate(
            R.layout.fragment_tests, container,
            false
        )
        testsExpandableListView =
            view.findViewById(R.id.tests_expandableListView) as ExpandableListView
        medicalTests=ArrayList()
        //showExpandableTestList(view)
        return view
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onAttach(context: Context) {
        mContext=context
        super.onAttach(context)
    }
    override fun toString(): String {
        return "Drama"
    }

    private fun showExpandableTestList(view:View)
    {

        val expandableList = java.util.HashMap<String, List<String>>()
        val totalRs: MutableList<String> = java.util.ArrayList()
        var totalPrice = 0
        for (medicalTest in medicalTests!!) {
            totalRs.add(medicalTest.testName)
            totalPrice = totalPrice + Integer.parseInt(medicalTest.price)
        }

        expandableList["Total : " + totalPrice] = totalRs

        expandableDetailList = expandableList
        expandableTitleList = ArrayList(expandableDetailList!!.keys)
        expandableListAdapter =
            CustomizedExpandableTestsListAdapter(
                mContext!!,
                expandableTitleList!!,
                expandableDetailList!!,medicalTests!!
            )
        testsExpandableListView!!.setAdapter(expandableListAdapter)
        testsExpandableListView!!.setOnGroupExpandListener { groupPosition ->
            /*Toast.makeText(
               mContext,
                expandableTitleList!!.get(groupPosition) + " List Expanded.",
                Toast.LENGTH_SHORT
            ).show()*/
        }

        testsExpandableListView!!.setOnGroupCollapseListener { groupPosition ->
            /*Toast.makeText(
               mContext,
                expandableTitleList!!.get(groupPosition) + " List Collapsed.",
                Toast.LENGTH_SHORT
            ).show()*/
        }

        testsExpandableListView!!.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->
            /*Toast.makeText(
               mContext,
                expandableTitleList!!.get(groupPosition) + " -> "
                        + expandableDetailList!![expandableTitleList!!.get(groupPosition)]!![childPosition],
                Toast.LENGTH_SHORT
            ).show()*/
            false
        }

        testsExpandableListView!!.setOnGroupClickListener { parent, v, groupPosition, id ->
            setListViewHeight(parent, groupPosition)
            false
        }
    }

    private fun setListViewHeight(listView: ExpandableListView, group: Int) {
        val listAdapter = listView.expandableListAdapter as ExpandableListAdapter
        var totalHeight = 0
        val desiredWidth: Int = View.MeasureSpec.makeMeasureSpec(
            listView.width,
            View.MeasureSpec.EXACTLY
        )
        for (i in 0 until listAdapter.groupCount) {
            val groupItem: View = listAdapter.getGroupView(i, false, null, listView)
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED)
            totalHeight += groupItem.getMeasuredHeight()
            if (listView.isGroupExpanded(i) && i != group
                || !listView.isGroupExpanded(i) && i == group
            ) {
                for (j in 0 until listAdapter.getChildrenCount(i)) {
                    val listItem: View = listAdapter.getChildView(
                        i, j, false, null,
                        listView
                    )
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED)
                    totalHeight += listItem.getMeasuredHeight()
                }
            }
        }
        val params = listView.layoutParams
        var height = (totalHeight
                + listView.dividerHeight * (listAdapter.groupCount - 1))
        if (height < 10) height = 200
        params.height = height
        listView.layoutParams = params
        listView.requestLayout()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: MedicalTestShowEvent?) {
        try {
            if(event!=null) {
                medicalTests = event?.tests.distinctBy { Pair(it.testName, it.testName) } as ArrayList<MedicalTests>
                val expandableList = java.util.HashMap<String, List<String>>()
                val totalRs: MutableList<String> = java.util.ArrayList()
                var totalPrice = 0
                for (medicalTest in event?.tests.distinctBy { Pair(it.testName, it.testName) } as ArrayList<MedicalTests>) {
                    totalRs.add(medicalTest.testName)
                    totalPrice = totalPrice + Integer.parseInt(medicalTest.price)
                }

                expandableList["Total : " + totalPrice] = totalRs

                expandableDetailList = expandableList
                expandableTitleList = ArrayList(expandableDetailList!!.keys)
                expandableListAdapter =
                    CustomizedExpandableTestsListAdapter(
                        mContext!!,
                        expandableTitleList!!,
                        expandableDetailList!!,event?.tests.distinctBy { Pair(it.testName, it.testName) } as ArrayList<MedicalTests>
                    )
                testsExpandableListView!!.setAdapter(expandableListAdapter)
                testsExpandableListView!!.setOnGroupExpandListener { groupPosition ->
                    /*Toast.makeText(
                       mContext,
                        expandableTitleList!!.get(groupPosition) + " List Expanded.",
                        Toast.LENGTH_SHORT
                    ).show()*/
                }

                testsExpandableListView!!.setOnGroupCollapseListener { groupPosition ->
                    /*Toast.makeText(
                       mContext,
                        expandableTitleList!!.get(groupPosition) + " List Collapsed.",
                        Toast.LENGTH_SHORT
                    ).show()*/
                }

                testsExpandableListView!!.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->
                    /*Toast.makeText(
                       mContext,
                        expandableTitleList!!.get(groupPosition) + " -> "
                                + expandableDetailList!![expandableTitleList!!.get(groupPosition)]!![childPosition],
                        Toast.LENGTH_SHORT
                    ).show()*/
                    false
                }

                testsExpandableListView!!.setOnGroupClickListener { parent, v, groupPosition, id ->
                    setListViewHeight(parent, groupPosition)
                    false
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        EventBus.getDefault().removeStickyEvent(event)
    }

}