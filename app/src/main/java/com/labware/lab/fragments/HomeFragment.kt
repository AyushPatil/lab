package com.labware.lab.fragments;

import android.Manifest
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.*
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.view.animation.Interpolator
import android.view.animation.LinearInterpolator
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.QuerySnapshot
import com.karumi.dexter.Dexter
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.single.PermissionListener
import com.labware.lab.R
import com.labware.lab.activities.ActivityHistory
import com.labware.lab.activities.ActivitySearchCollectionPoint
import com.labware.lab.activities.ActivityViewPhlebo
import com.labware.lab.activities.ScanQRCodeActivity
import com.labware.lab.constants.AppConstantUtils
import com.labware.lab.models.*
import com.labware.lab.utils.*
import com.labware.lab.views.EditText
import kotlinx.android.synthetic.main.bottomsheet_select_phlebo.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONArray
import org.json.JSONObject
import java.util.*


class HomeFragment : Fragment(),View.OnTouchListener {

    var mMapView: MapView? = null
    var edt_collect_from: EditText? = null
    var img_hamburger: ImageView? = null
    var img_restore: ImageView? = null
    private var googleMap: GoogleMap? = null
    private var bottomNavigationView: BottomNavigationView? = null
    private var fab_qr_code_scanner: FloatingActionButton? = null
    private var phelboMarker: Marker? = null
    private var isMarkerRotating: Boolean? = false
    private var myPreferences: MyPreferences? = null
    private var mAuth: FirebaseAuth?=null
    private var mFirebaseDatabaseInstances: FirebaseFirestore?=null
    private var userId:String?=null
    private var user:FirebaseUser?=null
    private var fusedLocationProviderClient: FusedLocationProviderClient? = null
    private lateinit var locationCallback: LocationCallback
    private var currentLatLng: LatLng? = null
    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 999
    }
    private var rawArray = intArrayOf(R.raw.uber_style)
    private var mapPhleboMarker:HashMap<Marker,PhleboMarker> = HashMap<Marker,PhleboMarker>()
    private var arrayListPhleboMarker:ArrayList<PhleboMarker> = ArrayList<PhleboMarker>()
    private var phelboMarkerHashMapObj: PhleboMarker? = null
    private var ll_all: LinearLayout? = null
    private var ll_pickup: LinearLayout? = null
    private var ll_drop: LinearLayout? = null

    private var txt_all_count: com.labware.lab.views.TextView? = null
    private var txt_pickup_count: com.labware.lab.views.TextView? = null
    private var txt_drop_count: com.labware.lab.views.TextView? = null

    private var lisSizeAllPhelebo:ArrayList<String> = ArrayList<String>()
    private var lisSizePickupPhelebo:ArrayList<String> = ArrayList<String>()
    private var lisSizeDropPhelebo:ArrayList<String> = ArrayList<String>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(
            R.layout.fragment_home, container,
            false
        )

        mAuth=FirebaseAuth.getInstance()
        mFirebaseDatabaseInstances= FirebaseFirestore.getInstance()
        arrayListPhleboMarker = ArrayList<PhleboMarker>()

        lisSizeAllPhelebo = ArrayList<String>()
        lisSizePickupPhelebo = ArrayList<String>()
        lisSizeDropPhelebo = ArrayList<String>()

        user=FirebaseAuth.getInstance().currentUser
        if (user != null) {
            userId=user?.uid
        }

        val settings = FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true)
            .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
            .build()
        mFirebaseDatabaseInstances?.setFirestoreSettings(settings)

        myPreferences = MyPreferences.getPreferences(requireActivity());
        bottomNavigationView = view.findViewById<BottomNavigationView>(R.id.bottomNavigationView)
        bottomNavigationView!!.background=null
        bottomNavigationView!!.menu.getItem(1).isEnabled=false

        fab_qr_code_scanner=view.findViewById<FloatingActionButton>(R.id.fab_qr_code_scanner)
        ll_all=view.findViewById<LinearLayout>(R.id.ll_all)
        ll_pickup=view.findViewById<LinearLayout>(R.id.ll_pickup)
        ll_drop=view.findViewById<LinearLayout>(R.id.ll_drop)
        txt_all_count=view.findViewById<com.labware.lab.views.TextView>(R.id.txt_all_count)
        txt_pickup_count=view.findViewById<com.labware.lab.views.TextView>(R.id.txt_pickup_count)
        txt_drop_count=view.findViewById<com.labware.lab.views.TextView>(R.id.txt_drop_count)

        fab_qr_code_scanner!!.setOnClickListener(View.OnClickListener {

            Dexter.withContext(activity)
                .withPermission(Manifest.permission.CAMERA)
                .withListener(object : PermissionListener {
                    override fun onPermissionGranted(response: PermissionGrantedResponse) {
                        val intent = Intent(activity, ScanQRCodeActivity::class.java)
                        startActivity(intent)
                        AppAndroidUtilities.startBottomToTopAnimation(activity)
                    }

                    override fun onPermissionDenied(response: PermissionDeniedResponse) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied) {
                            // navigate user to app settings
                            showSettingsDialog()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permission: PermissionRequest?,
                        token: PermissionToken
                    ) {
                        token.continuePermissionRequest()
                    }
                }).check()

        })

        bottomNavigationView!!.setOnNavigationItemSelectedListener(
            BottomNavigationView.OnNavigationItemSelectedListener { item ->
                when (item.itemId) {
                    R.id.ic_live -> {
                        /*val intentinstruments =
                        Intent(activity, ActivitySearchCollectionPoint::class.java)
                    startActivity(intentinstruments)
                    AppAndroidUtilities.startBottomToTopAnimation(activity)*/
                    }
                    R.id.ic_history -> {
                        val intentmethods = Intent(activity, ActivityHistory::class.java)
                        startActivity(intentmethods)
                        AppAndroidUtilities.startBottomToTopAnimation(activity)
                    }
                }
                true
            })
        var activity  = getActivity()
        if(activity!=null) {
            ProgressDialogManager.showProgressDialog(activity)
        }
        Handler().postDelayed({
            if(activity!=null) {
                ProgressDialogManager.dismissProgressDialog(activity)
            }
            view.findViewById<ProgressBar>(R.id.progressBar).visibility = View.INVISIBLE
        }, 3000)


        mMapView = view!!.findViewById(R.id.mapView)
        mMapView!!.onCreate(savedInstanceState)
        mMapView!!.onResume() // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(requireContext())
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mMapView!!.getMapAsync { mMap ->
            googleMap = mMap
            // For showing a move to my location button
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(
                        requireActivity(),
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED) {
                    googleMap!!.isMyLocationEnabled = true
                }
            } else {
                googleMap!!.isMyLocationEnabled = true
            }

            val locationButton= (mMapView!!.findViewById<View>(Integer.parseInt("1")).parent as View).findViewById<View>(Integer.parseInt("2"))
            val rlp=locationButton.layoutParams as (RelativeLayout.LayoutParams)
            // position on right bottom
            rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP,0)
            rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM,RelativeLayout.TRUE)
            rlp.setMargins(0,0,300,250)
            // For dropping a marker at a point on the Map


            val bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(
                MapUtils.getLargeRiderBitmap(
                    requireContext()
                )
            )

            mFirebaseDatabaseInstances?.collection(AppConstantUtils.PHLEBO)?.get()
                ?.addOnCompleteListener(OnCompleteListener<QuerySnapshot> { task ->
                    if (task.isSuccessful) {
                        val list: MutableList<String> = ArrayList()
                        for (document in task.result) {
                            list.add(document.id)
                            Log.e("DOCUMENT ID", document.id)
                            ProgressDialogManager.showProgressDialog(requireActivity())
                            val docRef=mFirebaseDatabaseInstances?.collection(AppConstantUtils.PHLEBO)?.document(
                                document.id.replace(
                                    "+91",
                                    ""
                                )
                            )

                            /*docRef?.addSnapshotListener { snapshot, e ->
                                if (e != null) {
                                    Log.w("TAG", "Listen failed.", e)
                                    return@addSnapshotListener
                                }
                                if (snapshot != null && snapshot.exists()) {
                                    Log.d("TAG", "Current data: ${snapshot.data}")
                                    var phlebo = snapshot.toObject(Phlebo::class.java)
                                    if(phelboMarker!=null)
                                    {
                                        phelboMarker?.remove()
                                    }
                                    if(phlebo?.labId.equals(mAuth?.currentUser?.phoneNumber?.replace("+91","")) && phlebo?.isAvailable!!) {
                                        phelboMarker = googleMap!!.addMarker(
                                            MarkerOptions().position(
                                                LatLng(
                                                    phlebo?.address?.lattitude?.toDouble()!!,
                                                    phlebo?.address?.longitude?.toDouble()!!
                                                )
                                            ).icon(bitmapDescriptor)
                                        )
                                        phelboMarkerHashMapObj= PhleboMarker()
                                        phelboMarkerHashMapObj?.phleboName=phlebo.number
                                        phelboMarkerHashMapObj?.phleboName=phlebo.name
                                        phelboMarkerHashMapObj?.isAvailable=phlebo.isAvailable
                                        phelboMarkerHashMapObj?.requestId=phlebo.requestId
                                        phelboMarkerHashMapObj?.labId=phlebo.labId
                                        phelboMarkerHashMapObj?.markerId=phelboMarker?.id
                                        mapPhleboMarker.put(phelboMarker!!,phelboMarkerHashMapObj!!)

                                        if (phelboMarker != null) {
                                            Handler().postDelayed({
                                                rotateMarker(phelboMarker!!, 90f)
                                            }, 2000)

                                            phelboMarker!!.setAnchor(0.5f, 0.5f)
                                        }
                                        val cameraPosition = CameraPosition.Builder().target(
                                            LatLng(
                                                phlebo?.address?.lattitude?.toDouble()!!,
                                                phlebo?.address?.longitude?.toDouble()!!
                                            )
                                        ).zoom(15f).build()
                                        googleMap!!.animateCamera(
                                            CameraUpdateFactory.newCameraPosition(
                                                cameraPosition
                                            )
                                        )
                                    }
                                } else {
                                    Log.d("TAG", "Current data: null")
                                }
                            }*/
                            arrayListPhleboMarker?.clear()
                            lisSizeAllPhelebo?.clear()
                            lisSizePickupPhelebo?.clear()
                            lisSizeDropPhelebo?.clear()
                            docRef?.get()?.addOnSuccessListener { documentSnapshot ->
                                if (documentSnapshot != null && documentSnapshot.exists()) {
                                    Log.d("TAG", "Current data: ${documentSnapshot.data}")
                                    try {
                                        var phlebo = documentSnapshot.toObject(Phlebo::class.java)
                                        if(phlebo?.labId.equals(mAuth?.currentUser?.phoneNumber?.replace("+91",""))
                                            && phlebo?.isAvailable!! && !phlebo?.status.equals("Discarded")) {
                                            phelboMarker = googleMap!!.addMarker(
                                                MarkerOptions().position(
                                                    LatLng(
                                                        phlebo?.address?.lattitude?.toDouble()!!,
                                                        phlebo?.address?.longitude?.toDouble()!!
                                                    )
                                                ).icon(bitmapDescriptor)
                                            )
                                            phelboMarkerHashMapObj= PhleboMarker()
                                            phelboMarkerHashMapObj?.phleboName=phlebo.name
                                            phelboMarkerHashMapObj?.phleboContactNumber=phlebo.number
                                            phelboMarkerHashMapObj?.isAvailable=phlebo.isAvailable
                                            phelboMarkerHashMapObj?.requestId=phlebo.requestId
                                            phelboMarkerHashMapObj?.labId=phlebo.labId
                                            phelboMarkerHashMapObj?.status=phlebo.status
                                            phelboMarkerHashMapObj?.lattitude=phlebo.address.lattitude
                                            phelboMarkerHashMapObj?.longitude=phlebo.address.longitude
                                            phelboMarkerHashMapObj?.markerId=phelboMarker?.id.toString()
                                            //mapPhleboMarker.put(phelboMarker!!,phelboMarkerHashMapObj!!)
                                            try {
                                                if(phlebo.status.equals("Pickup"))
                                                {
                                                    lisSizePickupPhelebo?.add(phlebo.number)
                                                    txt_pickup_count?.text=lisSizePickupPhelebo.size.toString()
                                                }
                                                if(phlebo.status.equals("Drop"))
                                                {
                                                    lisSizeDropPhelebo?.add(phlebo.number)
                                                    txt_drop_count?.text=lisSizeDropPhelebo.size.toString()
                                                }
                                                if(phlebo.status.equals("Pickup") || phlebo.status.equals("Drop"))
                                                {
                                                    lisSizeAllPhelebo?.add(phlebo.number)
                                                    txt_all_count?.text=lisSizeAllPhelebo.size.toString()
                                                }
                                            } catch (e: Exception) {
                                                e.printStackTrace()
                                            }


                                            arrayListPhleboMarker?.add(phelboMarkerHashMapObj!!)

                                            if (phelboMarker != null) {
                                                Handler().postDelayed({
                                                    rotateMarker(phelboMarker!!, 90f)
                                                }, 2000)

                                                phelboMarker!!.setAnchor(0.5f, 0.5f)
                                            }
                                            val cameraPosition = CameraPosition.Builder().target(
                                                LatLng(
                                                    phlebo?.address?.lattitude?.toDouble()!!,
                                                    phlebo?.address?.longitude?.toDouble()!!
                                                )
                                            ).zoom(15f).build()
                                            googleMap!!.animateCamera(
                                                CameraUpdateFactory.newCameraPosition(
                                                    cameraPosition
                                                )
                                            )
                                        }
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                } else {
                                    Log.d("TAG", "Current data: null")
                                }
                            }

                        }
                    }
                })


            try {
                googleMap!!.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                        activity, rawArray[0]
                    )
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }

            googleMap!!.setOnMarkerClickListener(object : GoogleMap.OnMarkerClickListener {
                override fun onMarkerClick(marker: Marker): Boolean {
                    phelboMarker = marker
                    try {
                        for(markerItem in arrayListPhleboMarker)
                        {
                            if(phelboMarker?.id.toString()?.equals(markerItem.markerId))
                                {
                                showPhleboInfoBottomSheetDialog(
                                    markerItem.phleboName,
                                    markerItem.requestId,
                                    markerItem.phleboContactNumber,
                                    markerItem.status,
                                    markerItem.lattitude,
                                    markerItem.longitude
                                )
                                break;
                            }
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    return false
                }
            })

            googleMap!!.setOnMapClickListener(object : GoogleMap.OnMapClickListener {
                override fun onMapClick(latLng: LatLng) {
                    phelboMarker = null
                }
            })


        }

        edt_collect_from= view!!.findViewById<EditText>(R.id.edt_collect_from)
        img_hamburger= view!!.findViewById<ImageView>(R.id.img_hamburger)
        img_restore= view!!.findViewById<ImageView>(R.id.img_restore)
        img_hamburger!!.setOnClickListener(View.OnClickListener {
            EventBus.getDefault().postSticky(
                MessageEvent(
                    "SHOW",
                    "HomeFragment.kt"
                )
            )
        })
        img_restore!!.setOnClickListener(View.OnClickListener {
            val intent = Intent(activity, ActivitySearchCollectionPoint::class.java)
            startActivity(intent)
            AppAndroidUtilities.startBottomToTopAnimation(activity)
        })
         edt_collect_from!!.setOnTouchListener(this)

         allPhleboFilterOnMap()

         pickUpPhleboFilterOnMap()

         dropPhleboFilterOnMap()

        return view
    }

    fun allPhleboFilterOnMap()
    {
        ll_all?.setOnClickListener(View.OnClickListener {

            mFirebaseDatabaseInstances?.collection(AppConstantUtils.PHLEBO)?.get()
                ?.addOnCompleteListener(OnCompleteListener<QuerySnapshot> { task ->
                    if (task.isSuccessful) {
                        val bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(
                            MapUtils.getLargeRiderBitmap(
                                requireContext()
                            )
                        )
                        val list: MutableList<String> = ArrayList()
                        for (document in task.result) {
                            list.add(document.id)
                            Log.e("DOCUMENT ID", document.id)
                            ProgressDialogManager.showProgressDialog(requireActivity())
                            val docRef=mFirebaseDatabaseInstances?.collection(AppConstantUtils.PHLEBO)?.document(
                                document.id.replace(
                                    "+91",
                                    ""
                                )
                            )
                            arrayListPhleboMarker?.clear()
                            docRef?.get()?.addOnSuccessListener { documentSnapshot ->
                                ProgressDialogManager.dismissProgressDialog(requireActivity())
                                if (documentSnapshot != null && documentSnapshot.exists()) {
                                    Log.d("TAG", "Current data: ${documentSnapshot.data}")
                                    if(phelboMarker!=null)
                                    {
                                        phelboMarker?.remove()
                                    }
                                    try {
                                        var phlebo = documentSnapshot.toObject(Phlebo::class.java)
                                        if(phlebo?.labId.equals(mAuth?.currentUser?.phoneNumber?.replace("+91",""))
                                            && phlebo?.isAvailable!! && !phlebo?.status.equals("Discarded")) {
                                            phelboMarker = googleMap!!.addMarker(
                                                MarkerOptions().position(
                                                    LatLng(
                                                        phlebo?.address?.lattitude?.toDouble()!!,
                                                        phlebo?.address?.longitude?.toDouble()!!
                                                    )
                                                ).icon(bitmapDescriptor)
                                            )
                                            phelboMarkerHashMapObj= PhleboMarker()
                                            phelboMarkerHashMapObj?.phleboName=phlebo.name
                                            phelboMarkerHashMapObj?.phleboContactNumber=phlebo.number
                                            phelboMarkerHashMapObj?.isAvailable=phlebo.isAvailable
                                            phelboMarkerHashMapObj?.requestId=phlebo.requestId
                                            phelboMarkerHashMapObj?.labId=phlebo.labId
                                            phelboMarkerHashMapObj?.status=phlebo.status
                                            phelboMarkerHashMapObj?.lattitude=phlebo.address.lattitude
                                            phelboMarkerHashMapObj?.longitude=phlebo.address.longitude
                                            phelboMarkerHashMapObj?.markerId=phelboMarker?.id.toString()
                                            //mapPhleboMarker.put(phelboMarker!!,phelboMarkerHashMapObj!!)

                                            arrayListPhleboMarker?.add(phelboMarkerHashMapObj!!)

                                            if (phelboMarker != null) {
                                                Handler().postDelayed({
                                                    rotateMarker(phelboMarker!!, 90f)
                                                }, 2000)

                                                phelboMarker!!.setAnchor(0.5f, 0.5f)
                                            }
                                            val cameraPosition = CameraPosition.Builder().target(
                                                LatLng(
                                                    phlebo?.address?.lattitude?.toDouble()!!,
                                                    phlebo?.address?.longitude?.toDouble()!!
                                                )
                                            ).zoom(15f).build()
                                            googleMap!!.animateCamera(
                                                CameraUpdateFactory.newCameraPosition(
                                                    cameraPosition
                                                )
                                            )
                                        }
                                    } catch (e: Exception) {
                                        e.printStackTrace()
                                    }
                                } else {
                                    Log.d("TAG", "Current data: null")
                                }
                            }

                        }
                    }
                })
        })
    }

    fun pickUpPhleboFilterOnMap()
    {
        ll_pickup?.setOnClickListener(View.OnClickListener {

            getFilterDataByType("Pickup")
        })
    }

    fun dropPhleboFilterOnMap()
    {
        ll_drop?.setOnClickListener(View.OnClickListener {

            getFilterDataByType("Drop")
        })
    }

    fun getFilterDataByType(type:String)
    {
        mFirebaseDatabaseInstances?.collection(AppConstantUtils.PHLEBO)?.get()
            ?.addOnCompleteListener(OnCompleteListener<QuerySnapshot> { task ->
                if (task.isSuccessful) {
                    val bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(
                        MapUtils.getLargeRiderBitmap(
                            requireContext()
                        )
                    )
                    val list: MutableList<String> = ArrayList()
                    for (document in task.result) {
                        list.add(document.id)
                        Log.e("DOCUMENT ID", document.id)
                        ProgressDialogManager.showProgressDialog(requireActivity())
                        val docRef=mFirebaseDatabaseInstances?.collection(AppConstantUtils.PHLEBO)?.document(
                            document.id.replace(
                                "+91",
                                ""
                            )
                        )
                        arrayListPhleboMarker?.clear()
                        docRef?.get()?.addOnSuccessListener { documentSnapshot ->
                            ProgressDialogManager.dismissProgressDialog(requireActivity())
                            if (documentSnapshot != null && documentSnapshot.exists()) {
                                Log.d("TAG", "Current data: ${documentSnapshot.data}")
                                if(phelboMarker!=null)
                                {
                                    phelboMarker?.remove()
                                }
                                try {
                                    var phlebo = documentSnapshot.toObject(Phlebo::class.java)
                                    if(phlebo?.labId.equals(mAuth?.currentUser?.phoneNumber?.replace("+91",""))
                                        && phlebo?.isAvailable!! && phlebo?.status.equals(type)) {
                                        phelboMarker = googleMap!!.addMarker(
                                            MarkerOptions().position(
                                                LatLng(
                                                    phlebo?.address?.lattitude?.toDouble()!!,
                                                    phlebo?.address?.longitude?.toDouble()!!
                                                )
                                            ).icon(bitmapDescriptor)
                                        )
                                        phelboMarkerHashMapObj= PhleboMarker()
                                        phelboMarkerHashMapObj?.phleboName=phlebo.name
                                        phelboMarkerHashMapObj?.phleboContactNumber=phlebo.number
                                        phelboMarkerHashMapObj?.isAvailable=phlebo.isAvailable
                                        phelboMarkerHashMapObj?.requestId=phlebo.requestId
                                        phelboMarkerHashMapObj?.labId=phlebo.labId
                                        phelboMarkerHashMapObj?.status=phlebo.status
                                        phelboMarkerHashMapObj?.lattitude=phlebo.address.lattitude
                                        phelboMarkerHashMapObj?.longitude=phlebo.address.longitude
                                        phelboMarkerHashMapObj?.markerId=phelboMarker?.id.toString()
                                        //mapPhleboMarker.put(phelboMarker!!,phelboMarkerHashMapObj!!)

                                        arrayListPhleboMarker?.add(phelboMarkerHashMapObj!!)

                                        if (phelboMarker != null) {
                                            Handler().postDelayed({
                                                rotateMarker(phelboMarker!!, 90f)
                                            }, 2000)

                                            phelboMarker!!.setAnchor(0.5f, 0.5f)
                                        }
                                        val cameraPosition = CameraPosition.Builder().target(
                                            LatLng(
                                                phlebo?.address?.lattitude?.toDouble()!!,
                                                phlebo?.address?.longitude?.toDouble()!!
                                            )
                                        ).zoom(15f).build()
                                        googleMap!!.animateCamera(
                                            CameraUpdateFactory.newCameraPosition(
                                                cameraPosition
                                            )
                                        )
                                    }
                                } catch (e: Exception) {
                                    e.printStackTrace()
                                }
                            } else {
                                Log.d("TAG", "Current data: null")
                            }
                        }

                    }
                }
            })
    }


    override fun onTouch(view: View, motionEvent: MotionEvent): Boolean {
        when (view) {
            edt_collect_from -> {
                when (motionEvent.action) {
                    MotionEvent.ACTION_DOWN -> {
                        val intent = Intent(activity, ActivitySearchCollectionPoint::class.java)
                        startActivity(intent)
                        AppAndroidUtilities.startBottomToTopAnimation(activity)

                    }
                    MotionEvent.ACTION_UP -> {
                        view.performClick()
                    }
                }
            }
        }
        return true
    }

    private fun showPhleboInfoBottomSheetDialog(phleboName:String,requestId:String,phleboContactNumber:String,status:String,originLatLng:String,destinationsLatLng:String) {
        val bottomSheetDialog = BottomSheetDialog(
            requireContext(),
            R.style.MyTransparentBottomSheetDialogTheme
        )
        bottomSheetDialog.setContentView(R.layout.bottomsheet_select_phlebo)
        val btnView = bottomSheetDialog.findViewById<TextView>(R.id.btn_ok)
        val btnCancel = bottomSheetDialog.findViewById<TextView>(R.id.btn_cancel)
        val txtStatus = bottomSheetDialog.findViewById<TextView>(R.id.txt_status)
        val txtEtaTime = bottomSheetDialog.findViewById<TextView>(R.id.txt_eta)
        val txtAddress = bottomSheetDialog.findViewById<TextView>(R.id.txt_address)

        var origin=originLatLng+" "+destinationsLatLng
        var destination=originLatLng+" "+destinationsLatLng
        txtStatus?.text=status
        GlobalScope.launch {
            Dispatchers.IO
            val queue = Volley.newRequestQueue(context)
            val url: String =
                "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + origin + "&destinations=" + destination + "&key=AIzaSyCv019sXxlNUIrQd6InZFPaPNX31jhfGzw"
            val stringReq = StringRequest(
                Request.Method.GET, url,
                Response.Listener<String> { response ->
                    try {
                        var strResp = response.toString()
                        val json_main: JSONObject = JSONObject(strResp)
                        var jsonarray_rows: JSONArray = json_main.optJSONArray("rows")
                        var jsonarray_origin: JSONArray = json_main.optJSONArray("origin_addresses")
                        val json_element: JSONObject =
                            jsonarray_rows.optJSONObject(0).optJSONArray("elements").optJSONObject(0)
                        var json_duration: JSONObject = json_element.optJSONObject("duration")
                        var time = json_duration.optString("text")
                        txtEtaTime?.text = phleboName +" ("+time+")"
                        var placeName = jsonarray_origin.get(0).toString()
                        txtAddress?.text = placeName
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { Log.d("API", "that didn't work") })
            queue.add(stringReq)
        }
        btnView!!.setOnClickListener(View.OnClickListener
        {
            val intent = Intent(activity, ActivityViewPhlebo::class.java)
            intent.putExtra("REQUEST_ID",requestId)
            intent.putExtra("PHLEBO_CONTACT_NO",phleboContactNumber)
            startActivity(intent)
            AppAndroidUtilities.startBottomToTopAnimation(activity)
            bottomSheetDialog.dismiss()
        })
        btnCancel!!.setOnClickListener(View.OnClickListener { bottomSheetDialog.dismiss() })
        bottomSheetDialog.show()
    }

    override fun onResume() {
        super.onResume()
        mMapView!!.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        mMapView!!.onDestroy()
        fusedLocationProviderClient?.removeLocationUpdates(locationCallback)
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMapView!!.onLowMemory()
    }

    private fun rotateMarker(marker: Marker, toRotation: Float) {
        if (isMarkerRotating==false) {
            val handler = Handler()
            val start = SystemClock.uptimeMillis()
            val startRotation = marker.rotation
            val duration: Long = 1000
            val interpolator: Interpolator = LinearInterpolator()
            handler.post(object : Runnable {
                override fun run() {
                    isMarkerRotating = true
                    val elapsed = SystemClock.uptimeMillis() - start
                    val t: Float = interpolator.getInterpolation(elapsed.toFloat() / duration)
                    val rot = t * toRotation + (1 - t) * startRotation
                    marker.rotation = if (-rot > 180) rot / 2 else rot
                    if (t < 1.0) {
                        // Post again 16ms later.
                        handler.postDelayed(this, 16)
                    } else {
                        isMarkerRotating = false
                    }
                }
            })
        }
    }

    private fun showSettingsDialog() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(activity)
        builder.setTitle("Need Permissions")
        builder.setMessage("Lab app needs permission to use this feature. You can grant them in app settings.")
        builder.setPositiveButton("GOTO SETTINGS",
            DialogInterface.OnClickListener { dialog, which ->
                dialog.cancel()
                openSettings()
            })
        builder.setNegativeButton("Cancel",
            DialogInterface.OnClickListener { dialog, which -> dialog.cancel() })
        builder.show()
    }

    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts(
            "package",
            requireContext().getPackageName(),
            null
        )
        intent.data = uri
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        requireContext().startActivity(intent)
    }

    override fun onPause() {
        super.onPause()
        mMapView!!.onPause()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: MessageEvent?) {

    }

    override fun onStart() {
        super.onStart()
        bottomNavigationView!!.selectedItemId=R.id.ic_live
        requestLOcationPermission()
        getDataOneTime(user?.phoneNumber!!)

        if (currentLatLng == null) {
            when {
                PermissionUtils.isAccessFineLocationGranted(requireActivity()) -> {
                    when {
                        PermissionUtils.isLocationEnabled(requireActivity()) -> {
                            setUpLocationListener()
                        }
                        else -> {
                            PermissionUtils.showGPSNotEnabledDialog(requireActivity())
                        }
                    }
                }
                else -> {
                    PermissionUtils.requestAccessFineLocationPermission(
                        context as AppCompatActivity,
                        LOCATION_PERMISSION_REQUEST_CODE
                    )
                }
            }
        }
    }

    private fun requestLOcationPermission() {
        when {
            PermissionUtils.isAccessFineLocationGranted(requireContext()) -> {
                when {
                    PermissionUtils.isLocationEnabled(requireContext()) -> {

                    }
                    else -> {
                        PermissionUtils.showGPSNotEnabledDialog(requireContext())
                    }
                }
            }
            else -> {
                PermissionUtils.requestAccessFineLocationPermission(
                    requireContext() as AppCompatActivity,
                    LOCATION_PERMISSION_REQUEST_CODE
                )
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    when {
                        PermissionUtils.isLocationEnabled(requireContext()) -> {
                            //setUpLocationListener()
                        }
                        else -> {
                            PermissionUtils.showGPSNotEnabledDialog(requireContext())
                        }
                    }
                } else {
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.location_permission_not_granted),
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
        }
    }

    private fun getDataOneTime(phoneNumber: String){
        ProgressDialogManager.showProgressDialog(requireActivity())
        val docRef=mFirebaseDatabaseInstances?.collection(AppConstantUtils.LAB)?.document(
            phoneNumber.replace(
                "+91",
                ""
            )
        )
        docRef?.get()?.addOnSuccessListener { documentSnapshot ->
            ProgressDialogManager.dismissProgressDialog(requireActivity())
            val user=documentSnapshot.toObject(Lab::class.java)
            myPreferences?.labID=user?.number

        }
    }


    private fun enableMyLocationOnMap() {
        googleMap?.setPadding(0, ViewUtils.dpToPx(48f), 0, 0)
        googleMap?.isMyLocationEnabled = false
    }

    private fun setUpLocationListener() {
        fusedLocationProviderClient = FusedLocationProviderClient(requireActivity())
        // for getting the current location update after every 2 seconds
        val locationRequest = LocationRequest().setInterval(2000).setFastestInterval(2000)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                if (currentLatLng == null) {
                    for (location in locationResult.locations) {
                        if (currentLatLng == null) {
                            currentLatLng = LatLng(location.latitude, location.longitude)
                            enableMyLocationOnMap()
                            moveCamera(currentLatLng)
                            animateCamera(currentLatLng)

                           /*googleMap?.addCircle(
                                CircleOptions()
                                    .center(LatLng(location.latitude,location.longitude))
                                    .radius(300.0)
                                    .strokeColor(Color.parseColor("#84b3da"))
                                    .strokeWidth(5f)
                                    .fillColor(Color.parseColor("#cddde5"))
                            )*/

                        }
                    }
                }
            }
        }
        try {
            fusedLocationProviderClient?.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.myLooper()
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun moveCamera(latLng: LatLng?) {
        googleMap?.moveCamera(CameraUpdateFactory.newLatLng(latLng))
    }

    private fun animateCamera(latLng: LatLng?) {
        val cameraPosition = CameraPosition.Builder().target(latLng).zoom(15.5f).build()
        googleMap?.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

}