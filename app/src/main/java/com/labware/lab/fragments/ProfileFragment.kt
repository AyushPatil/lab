package com.labware.lab.fragments
import android.app.Activity
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.SetOptions
import com.labware.lab.R
import com.labware.lab.activities.MainActivity
import com.labware.lab.constants.AppConstantUtils
import com.labware.lab.models.Lab
import com.labware.lab.models.MessageEvent
import com.labware.lab.models.MyPreferences
import com.labware.lab.utils.AppAndroidUtilities
import com.labware.lab.utils.ProgressDialogManager
import com.labware.lab.utils.ValidationsAll
import com.labware.lab.views.Button
import com.labware.lab.views.EditText
import com.labware.lab.views.TextView
import com.squats.fittr.utils.LabResourceHelper
import com.vanillaplacepicker.presentation.builder.VanillaPlacePicker
import com.vanillaplacepicker.utils.KeyUtils
import com.vanillaplacepicker.utils.MapType
import com.vanillaplacepicker.utils.PickerLanguage
import com.vanillaplacepicker.utils.PickerType
import org.greenrobot.eventbus.EventBus
import java.util.*


class ProfileFragment : Fragment() {

    private var userId:String?=null
    var edt_name: EditText? = null
    var edt_phone_number: EditText? = null
    var txt_address: TextView? = null
    var txt_certifications: com.labware.lab.views.TextView? = null
    var mobileNumber: String? = null
    var chip_group_certifications: ChipGroup? = null
    val selectedeCertifications = mutableListOf<String>()
    private var mAuth: FirebaseAuth?=null
    private var mFirebaseDatabaseInstances:FirebaseFirestore?=null
    private var isoChip:Chip?=null
    private var nablChip:Chip?=null
    var lattitude:String?=null
    var longitude:String?=null
    var geocoder: Geocoder? = null
    var addresses: List<Address>? = null
    private var myPreferences: MyPreferences? = null

    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view: View = inflater.inflate(
            R.layout.fragment_user_profile, container,
            false
        )

        mAuth=FirebaseAuth.getInstance()
        mFirebaseDatabaseInstances= FirebaseFirestore.getInstance()
        val user=FirebaseAuth.getInstance().currentUser
        if (user != null) {
            userId=user.uid
        }

        val settings = FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true)
            .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
            .build()
        mFirebaseDatabaseInstances?.setFirestoreSettings(settings)

        edt_name = view.findViewById<EditText>(R.id.edt_name)
        edt_phone_number = view.findViewById<EditText>(R.id.edt_phone_number)
        txt_address = view.findViewById<TextView>(R.id.txt_address)
        txt_certifications = view.findViewById<com.labware.lab.views.TextView>(R.id.txt_certifications)
        chip_group_certifications = view.findViewById<ChipGroup>(R.id.chip_group_certifications)
        isoChip = view.findViewById<Chip>(R.id.iso)
        nablChip = view.findViewById<Chip>(R.id.nabl)
        edt_phone_number?.setText(user?.phoneNumber)
        edt_phone_number?.isEnabled=false
        edt_phone_number?.isClickable=false

        myPreferences = MyPreferences.getPreferences(requireActivity());
        getDataOneTime(user?.phoneNumber!!)

        for (index in 0 until 2) {
            val chip:Chip = chip_group_certifications?.getChildAt(index) as Chip
            // Set the chip checked change listener
            chip.setOnCheckedChangeListener{ view, isChecked ->
                if (isChecked){
                    selectedeCertifications.add(view.text.toString())
                }else{
                    selectedeCertifications.remove(view.text.toString())
                }
                if (selectedeCertifications.isNotEmpty()){
                    // SHow the selection
                    //toast("Selected $selectedeCertifications")
                }
                Log.e("SIZEEEE", selectedeCertifications.size.toString())
            }
        }

        view.findViewById<Button>(R.id.btn_save).setOnClickListener(View.OnClickListener {

            if (validate()) {
                val labUser =Lab()
                val address= com.labware.lab.models.Address()
                address.lattitude=lattitude
                address.longitude=longitude
                labUser.name=edt_name?.text.toString()
                labUser.number=edt_phone_number?.text.toString()
                labUser.address = address
                labUser.certifications = selectedeCertifications
                mFirebaseDatabaseInstances?.collection(AppConstantUtils.LAB)?.document(edt_phone_number?.text.toString())!!.set(labUser, SetOptions.merge())
                    .addOnSuccessListener { documentReference ->
                        AppAndroidUtilities.showBanner(
                            requireActivity(),
                            LabResourceHelper.getString(R.string.success_alert),
                            1
                        )
                        EventBus.getDefault().postSticky(MessageEvent("BACKPRESSED", "ProfileFragment.kt"))
                    }
                    .addOnFailureListener { e ->
                        AppAndroidUtilities.showBanner(requireActivity(),LabResourceHelper.getString(R.string.something_went_worng),4)
                    }


            }
        })

        txt_address?.setOnClickListener(View.OnClickListener {

            val intent = VanillaPlacePicker.Builder(requireActivity())
                .with(PickerType.MAP_WITH_AUTO_COMPLETE)
                .setMapType(MapType.NORMAL)
                .setMapStyle(R.raw.uber_style)
                .setCountry("IN")
                .setPickerLanguage(PickerLanguage.ENGLISH)
                .enableShowMapAfterSearchResult(true)
                .build()
            startActivityForResult(intent, KeyUtils.REQUEST_PLACE_PICKER)
            AppAndroidUtilities.startFwdAnimation(requireActivity())
        })

        return view
    }

    fun toast(message: String)=
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()

    override fun toString(): String {
        return "Drama"
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data != null) {
            when (requestCode) {
                KeyUtils.REQUEST_PLACE_PICKER -> {
                    val vanillaAddress = VanillaPlacePicker.onActivityResult(data)
                    lattitude = vanillaAddress!!.latitude!!.toString()
                    longitude= vanillaAddress!!.longitude!!.toString()
                    /*geocoder = Geocoder( this@ActivityUserProfile, Locale.getDefault())
                     addresses = geocoder!!.getFromLocation(
                         lattitude?.toDouble()!!,
                         longitude?.toDouble()!!,1)
                     val address = addresses?.get(0)?.getAddressLine(0)*/
                    txt_address?.setText(vanillaAddress.formattedAddress)
                }
            }
        }
    }


    private fun getDataOneTime(phoneNumber: String){
        var activity  = getActivity()
        if(activity != null) {
            ProgressDialogManager.showProgressDialog(requireActivity())
        }
        val docRef=mFirebaseDatabaseInstances?.collection(AppConstantUtils.LAB)?.document(
            phoneNumber.replace(
                "+91",
                ""
            )
        )
        docRef?.get()?.addOnSuccessListener { documentSnapshot ->
            if(activity != null) {
                ProgressDialogManager.dismissProgressDialog(requireActivity())
            }
            try {
                val user=documentSnapshot.toObject(Lab::class.java)
                edt_name?.setText(user?.name)
                edt_phone_number?.setText(user?.number)
                myPreferences?.labID=user?.number
                geocoder = Geocoder( requireActivity(), Locale.getDefault())
                addresses = geocoder!!.getFromLocation(
                    user?.address?.lattitude?.toDouble()!!,
                    user?.address?.longitude?.toDouble()!!,
                    1
                )

                lattitude = user?.address?.lattitude?.toString()!!
                longitude = user?.address?.longitude?.toString()!!

                val address = addresses?.get(0)?.getAddressLine(0)
                txt_address?.setText(address)

                for (item in user?.certifications!!) {
                    // body of loop
                    if(item.equals("ISO"))
                    {
                        isoChip?.isChecked=true
                    }
                    else if(item.equals("NABL"))
                    {
                        nablChip?.isChecked=true
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun validate(): Boolean {
        if (edt_name?.text.toString().trim().isNullOrEmpty()) {
            AppAndroidUtilities.showBanner(requireActivity(), "Please enter valid name", 4)
            return false
        } else if (edt_phone_number?.text.toString().trim()
                .isNullOrEmpty() || !ValidationsAll.checkPhone(
                edt_phone_number?.text.toString().trim()
            )
        ) {
            AppAndroidUtilities.showBanner(
                requireActivity(),
                "Please enter valid phone number",
                4
            )
            return false
        } else if (txt_address?.text.toString().trim().isNullOrEmpty() || txt_address?.text.toString().trim().equals("Enter your address")) {
            AppAndroidUtilities.showBanner(
                requireActivity(),
                "Please enter valid address",
                4
            )
            return false
        } else if (selectedeCertifications.isNullOrEmpty() || selectedeCertifications.size==0)
        {
            AppAndroidUtilities.showBanner(
                requireActivity(),
                "Please select valid certifications",
                4
            )
            return false
        }
        return true;
    }
}