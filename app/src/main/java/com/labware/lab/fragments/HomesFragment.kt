package com.labware.lab.fragments
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import android.widget.LinearLayout
import androidx.annotation.Nullable
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnFailureListener
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.QuerySnapshot
import com.labware.lab.R
import com.labware.lab.adapters.CollectionPointAdapter
import com.labware.lab.constants.AppConstantUtils
import com.labware.lab.models.*
import com.labware.lab.utils.AppAndroidUtilities
import com.labware.lab.utils.ProgressDialogManager
import com.squats.fittr.utils.LabResourceHelper
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*


class HomesFragment : Fragment(),SwipeRefreshLayout.OnRefreshListener {

    private var shimmeContainer: ShimmerFrameLayout? = null
    private var mAuth: FirebaseAuth?=null
    private var mFirebaseDatabaseInstances: FirebaseFirestore?=null
    private var recyclerView_hospital: RecyclerView?=null
    private var data:ArrayList<Hospital>?=null
    private var user:Hospital?=null
    private var myPreferences: MyPreferences? = null
    private var adapter: CollectionPointAdapter? = null
    private var from: String? = null
    private var mSwipeRefreshLayout: SwipeRefreshLayout?=null

    companion object{

        private const val KEY_FROM="from"

        fun newInstance(from:String):HomesFragment
        {
            val fragment = HomesFragment()
            val bundle = Bundle()
            bundle.putString(KEY_FROM,from)
            fragment.arguments = bundle
            return fragment
        }
    }

    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view: View = inflater.inflate(
            R.layout.fragment_clinics, container,
            false
        )

        myPreferences = MyPreferences.getPreferences(requireActivity())

        view!!.findViewById<LinearLayout>(R.id.empty_view).addView(
            AppAndroidUtilities.setNewEmptyMessage(
                activity, view!!.findViewById<LinearLayout>(
                    R.id.empty_view
                ), R.drawable.ic_empty_data, "No data found", "", "Ok", false
            )
        )
        shimmeContainer = view.findViewById<ShimmerFrameLayout>(R.id.shimmer_view_container)
        shimmeContainer!!.startShimmer()
        shimmeContainer!!.visibility=View.GONE
        Handler().postDelayed({
            shimmeContainer!!.stopShimmer()
            shimmeContainer!!.visibility = View.GONE
        }, 3000)

        recyclerView_hospital = view!!.findViewById<RecyclerView>(R.id.recyclerView_hospital)
        recyclerView_hospital?.layoutManager = LinearLayoutManager(requireActivity())

        mAuth=FirebaseAuth.getInstance()
        mFirebaseDatabaseInstances= FirebaseFirestore.getInstance()

        val settings = FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true)
            .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
            .build()
        mFirebaseDatabaseInstances?.setFirestoreSettings(settings)
        data = ArrayList<Hospital>()

        mSwipeRefreshLayout = view.findViewById<SwipeRefreshLayout>(R.id.container)
        mSwipeRefreshLayout?.setOnRefreshListener(this)
        mSwipeRefreshLayout?.setColorSchemeColors(
            LabResourceHelper.getColor(R.color.colorPrimaryDark),
            LabResourceHelper.getColor(R.color.colorAccent),
            LabResourceHelper.getColor(R.color.colorPrimaryDark));

        return view
    }

    override fun onStart() {
        super.onStart()
        data?.clear()
        val args = arguments
        if(args!=null) {
            if (args.containsKey(KEY_FROM)) {
                from = args.getString(KEY_FROM)
            }
        }
        mFirebaseDatabaseInstances?.collection(AppConstantUtils.HOME)?.get()
            ?.addOnCompleteListener(OnCompleteListener<QuerySnapshot> { task ->
                if (task.isSuccessful) {
                    val list: MutableList<String> = ArrayList()
                    for (document in task.result) {
                        list.add(document.id)
                        Log.e("DOCUMENT ID", document.id)
                        getDataOneTime(document.id)
                    }
                }
            })

        if(data.isNullOrEmpty() || data?.size==0)
        {
            recyclerView_hospital?.visibility=View.GONE
            view?.findViewById<LinearLayout>(R.id.empty_view)?.visibility=View.VISIBLE
        }
    }

    private fun getDataOneTime(phoneNumber: String){
        var activity=getActivity()
        if(activity!=null) {
            ProgressDialogManager.showProgressDialog(requireActivity())
        }

        var documentId=phoneNumber.replace("+91", "")
        Log.e("@@@documentID33333",documentId)
        val docRef=mFirebaseDatabaseInstances?.collection(AppConstantUtils.HOME)?.document(documentId)

        docRef?.get()?.addOnSuccessListener { documentSnapshot ->
            mSwipeRefreshLayout?.setRefreshing(false)
            if(activity!=null) {
                ProgressDialogManager.dismissProgressDialog(requireActivity())
            }
            try {
                user=documentSnapshot.toObject(Hospital::class.java)
                if(user?.labId.equals(myPreferences?.labID)) {
                    if (user?.type.equals(AppConstantUtils.HOME)) {
                        data?.add(user!!)
                        adapter = CollectionPointAdapter(data!!,from!!)
                        recyclerView_hospital?.adapter = adapter
                        view?.findViewById<LinearLayout>(R.id.empty_view)?.visibility = View.GONE
                        recyclerView_hospital?.visibility = View.VISIBLE
                        Log.e("data_sizeeee", data?.size.toString())
                    } else {
                        recyclerView_hospital?.visibility = View.GONE
                        view?.findViewById<LinearLayout>(R.id.empty_view)?.visibility = View.VISIBLE
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()

            }

            if(!data.isNullOrEmpty())
            {
                var controller : LayoutAnimationController =
                    AnimationUtils.loadLayoutAnimation(requireActivity(), R.anim.layout_animation_fall_down)
                recyclerView_hospital?.setLayoutAnimation(controller)
                recyclerView_hospital?.getAdapter()?.notifyDataSetChanged()
                recyclerView_hospital?.scheduleLayoutAnimation()
            }
        }
        docRef?.get()?.addOnFailureListener(OnFailureListener { mSwipeRefreshLayout?.setRefreshing(false) })
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            onStart()
            EventBus.getDefault().postSticky(CollectionPointEvent("HIDE_SWIPE_TO_COLLECT", "0","0","0","HomesFragment.kt","0","0"))
        }
    }

    override fun onRefresh() {
        onStart()
        EventBus.getDefault().postSticky(
            CollectionPointEvent(
                "HIDE_SWIPE_TO_COLLECT",
                "0",
                "0",
                "0",
                "HospitalFragment.kt",
                "0",
                "0"
            )
        )
    }
}