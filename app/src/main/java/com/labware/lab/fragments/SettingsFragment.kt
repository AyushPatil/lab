package com.labware.lab.fragments
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.annotation.Nullable
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.facebook.shimmer.ShimmerFrameLayout
import com.labware.lab.R
import com.labware.lab.utils.AppAndroidUtilities
import com.labware.lab.utils.ProgressDialogManager

class SettingsFragment : Fragment() {
    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view: View = inflater.inflate(
            R.layout.fragment_settings, container,
            false
        )
        var activity= getActivity()
        if(activity!=null) {
            ProgressDialogManager.showProgressDialog(requireActivity())
        }
        Handler().postDelayed({
            if(activity!=null) {
                ProgressDialogManager.dismissProgressDialog(requireActivity())
            }
        }, 2000)
        return view
    }

    override fun toString(): String {
        return "Drama"
    }
}