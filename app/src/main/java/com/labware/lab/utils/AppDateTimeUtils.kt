package com.labware.lab.utils

import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object AppDateTimeUtils {

    fun getCurrentDateTime(): String {
        val df = SimpleDateFormat("EEE, d MMM yyyy, HH:mm", Locale.ENGLISH)
        return df.format(Calendar.getInstance().time)
    }

    fun getCurrentDateTimeYYYYMMDDHHMMSS(): String? {
        val df = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        return df.format(Calendar.getInstance().time)
    }

    fun getCurrentDateTimeYYYYMMDD(): String? {
        val df = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        return df.format(Calendar.getInstance().time)
    }

    fun getLastTenDaysDateTimeYYYYMMDD(): String? {
        val df = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        val cal = Calendar.getInstance()
        cal.add(Calendar.DAY_OF_YEAR, -10)
        return df.format(Date(cal.timeInMillis))
    }

    fun convertServerDateInToDDMMMYYYY(datetime: String?): String {
        val fromFormat = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.ENGLISH)
        val toFormat = SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH)
        try {
            val date = fromFormat.parse(datetime as String)
            return toFormat.format(date as Date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return datetime!!
    }

    fun convertYYYYMMDDHHMMSSToDDMMMYYYY(datetime: String?): String {
        val fromFormat = SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.ENGLISH)
        val toFormat = SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH)
        try {
            val date = fromFormat.parse(datetime as String)
            return toFormat.format(date as Date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return datetime!!
    }
    fun convertServerDateInToDDMMYYYY(datetime: String?): String {
        val fromFormat = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.ENGLISH)
        val toFormat = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
        try {
            val date = fromFormat.parse(datetime as String)
            return toFormat.format(date as Date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return datetime!!
    }

    fun convertServerDateInToHHMMSS(datetime: String?): String {
        val fromFormat = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.ENGLISH)
        val toFormat = SimpleDateFormat("hh:mm:ss", Locale.ENGLISH)
        try {
            val date = fromFormat.parse(datetime as String)
            return toFormat.format(date as Date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return datetime!!
    }

    fun convertLocalDateInToYYYYMMDD(datetime: String?): String {
        val fromFormat = SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH)
        val toFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        try {
            val date = fromFormat.parse(datetime as String)
            return toFormat.format(date as Date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return datetime!!
    }

    fun convertServerDateInToDDMMYYYYHHMMAA(datetime: String?): String {
        val fromFormat = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.ENGLISH)
        val toFormat = SimpleDateFormat("dd MMM yyyy, hh:mm:a", Locale.ENGLISH)
        try {
            val date = fromFormat.parse(datetime as String)
            return toFormat.format(date as Date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return datetime!!
    }

    fun convertServerDateInToDDMMYYYYHHMM(datetime: String?): String {
        val fromFormat = SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.ENGLISH)
        val toFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        try {
            val date = fromFormat.parse(datetime as String)
            return toFormat.format(date as Date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return datetime!!
    }

    fun getTwoDateTimeDiffInDays(currentDateTime: String?, endDateTime: String?): Long {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        val currentDate = dateFormat.parse(currentDateTime!!)
        val oldDate = dateFormat.parse(endDateTime!!)

        val diff = currentDate!!.time - oldDate!!.time
        val seconds = diff / 1000
        val minutes = seconds / 60
        val hours = minutes / 60
        val days = hours / 24
        return days
    }

    fun getTwoDateTimeDiffInMinutes(currentDateTime: String?, endDateTime: String?): Long {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        val currentDate = dateFormat.parse(currentDateTime!!)
        val oldDate = dateFormat.parse(endDateTime!!)

        val diff = currentDate!!.time - oldDate!!.time
        val seconds = diff / 1000
        val minutes = seconds / 60
        val hours = minutes / 60
        val days = hours / 24
        return minutes
    }

    fun getCurrentTimeStamp(): String {
        val df = SimpleDateFormat("MMM dd, yyyy hh:mm:ss a", Locale.ENGLISH)
        return df.format(Calendar.getInstance().time)
    }

    fun getDateByTimestamp(timestamp: Long) :String {
        val calendar = Calendar.getInstance(Locale.ENGLISH)
        calendar.timeZone= TimeZone.getDefault()
        calendar.timeInMillis = timestamp * 1000L
        val date = android.text.format.DateFormat.format("dd/MM/yyyy hh:mm a",calendar).toString()
        return date
    }

}