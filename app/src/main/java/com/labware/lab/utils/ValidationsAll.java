package com.labware.lab.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationsAll {

	public static final String emailValidationString ="[A-Z0-9a-z._-]+@[A-Za-z0-9.]+\\.[A-Za-z]{2,4}";
	private final static char KOREAN_UNICODE_START = '가';
	private final static char KOREAN_UNICODE_END = '힣';
	private final static char KOREAN_UNIT = '까' - '가';
	private final static char[] KOREAN_INITIAL = { 'ㄱ', 'ㄲ', 'ㄴ', 'ㄷ', 'ㄸ',
			'ㄹ', 'ㅁ', 'ㅂ', 'ㅃ', 'ㅅ', 'ㅆ', 'ㅇ', 'ㅈ', 'ㅉ', 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ',
			'ㅎ' };

	private static Matcher matcher;

	public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern
			.compile(emailValidationString
			/*"^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,3})$"*/);

	public static final Pattern PASSWORD_PATTERN = Pattern
			.compile("[A-Z0-9a-z]{8,30}");

	public static final Pattern USERNAME_PATTERN = Pattern
			.compile("[A-Z0-9a-z]{6,20}");

	public static final Pattern NAME_PATTERN = Pattern
			.compile("[A-Za-z][a-zA-Z ]*$");
	public static final Pattern NAME = Pattern.compile("[A-Za-z][a-zA-Z]*$");
	public static final Pattern FIRST_NAME_PATTERN = Pattern
			.compile("[A-Z][a-zA-Z]*");

	public static final Pattern LAST_NAME_PATTERN = Pattern
			.compile("[a-zA-z]+([ '-][a-zA-Z]+)*");

	public static final Pattern PHONE_PATTERN = Pattern
			.compile("^[0-9\\-\\+]{9,15}$");

	public static final Pattern CODE_PATTERN = Pattern
			.compile("[a-zA-z0-9]{2,14}");
	public static final Pattern ZIP_CODE_PATTERN = Pattern
			.compile("[0-9]{2,14}");

	public static final Pattern ADDRESS_PATTERN = Pattern
			.compile("\\d+\\s+([a-zA-Z]+|[a-zA-Z]+\\s[a-zA-Z]+\\s[A-Z0-9a-z-,.])");
	private static final String DATE_PATTERN = "(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)";

	public static final Pattern pattern = Pattern.compile(DATE_PATTERN);

	private static final String DATE_TIME_PATTERN = "(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d) (1[012]|[1-9]):[0-5][0-9](\\s)?(?i)(am|pm)";

	public static final Pattern DATE_TIME_PATTERNS = Pattern
			.compile(DATE_TIME_PATTERN);

	public static boolean checkEmail(String email) {
		return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
	}

	public static boolean checkPassword(String password) {
		return PASSWORD_PATTERN.matcher(password).matches();
	}

	public static boolean checkUserName(String password) {
		return USERNAME_PATTERN.matcher(password).matches();
	}

	public static boolean checkName(String name) {
		return NAME_PATTERN.matcher(name).matches();
	}

	public static boolean checkFirstName(String name) {
		return NAME_PATTERN.matcher(name).matches();
	}

	public static boolean checkLastName(String name) {
		return NAME_PATTERN.matcher(name).matches();
	}

	public static boolean checkPhone(String name) {
		return PHONE_PATTERN.matcher(name).matches();
	}

	public static boolean checkPhone1(String name) {
		return PHONE_PATTERN.matcher(name).matches();
	}

	public static boolean checkAddress(String address) {
		return ADDRESS_PATTERN.matcher(address).matches();
	}

	public static boolean checkCode(String code) {
		return CODE_PATTERN.matcher(code).matches();
	}

	public static boolean checkZipCode(String code) {
		return ZIP_CODE_PATTERN.matcher(code).matches();
	}

	public static boolean checkDateTime(String code) {
		return DATE_TIME_PATTERNS.matcher(code).matches();
	}

	public static boolean validate(final String date) {

		matcher = pattern.matcher(date);

		if (matcher.matches()) {

			matcher.reset();

			if (matcher.find()) {

				String day = matcher.group(1);
				String month = matcher.group(2);
				int year = Integer.parseInt(matcher.group(3));

				if (day.equals("31")
						&& (month.equals("4") || month.equals("6")
								|| month.equals("9") || month.equals("11")
								|| month.equals("04") || month.equals("06") || month
									.equals("09"))) {
					return false; // only 1,3,5,7,8,10,12 has 31 days
				} else if (month.equals("2") || month.equals("02")) {
					// leap year
					if (year % 4 == 0) {
						if (day.equals("30") || day.equals("31")) {
							return false;
						} else {
							return true;
						}
					} else {
						if (day.equals("29") || day.equals("30")
								|| day.equals("31")) {
							return false;
						} else {
							return true;
						}
					}
				} else {
					return true;
				}
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public static boolean match(String value, String keyword) {
		if (value == null || keyword == null)
			return false;
		if (keyword.length() > value.length())
			return false;

		int i = 0, j = 0;
		do {
			if (isKorean(value.charAt(i)) && isInitialSound(keyword.charAt(j))) {
				if (keyword.charAt(j) == getInitialSound(value.charAt(i))) {
					i++;
					j++;
				} else if (j > 0)
					break;
				else
					i++;
			} else {
				if (keyword.charAt(j) == value.charAt(i)) {
					i++;
					j++;
				} else if (j > 0)
					break;
				else
					i++;
			}
		} while (i < value.length() && j < keyword.length());

		return (j == keyword.length()) ? true : false;
	}

	private static boolean isKorean(char c) {
		if (c >= KOREAN_UNICODE_START && c <= KOREAN_UNICODE_END)
			return true;
		return false;
	}

	private static boolean isInitialSound(char c) {
		for (char i : KOREAN_INITIAL) {
			if (c == i)
				return true;
		}
		return false;
	}

	private static char getInitialSound(char c) {
		return KOREAN_INITIAL[(c - KOREAN_UNICODE_START) / KOREAN_UNIT];
	}

}