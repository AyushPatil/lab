package com.squats.fittr.utils

import android.content.Context
import android.content.res.Resources
import android.content.res.TypedArray
import android.graphics.Color
import androidx.annotation.*
import androidx.core.content.ContextCompat
import com.labware.lab.R
import com.labware.lab.utils.LabApplication

class LabResourceHelper {
    companion object {
        fun getApplicationContext(): Context {
            return LabApplication.getInstance()
        }

        @JvmStatic
        fun getString(@StringRes id: Int) = getApplicationContext().getString(id)
        @JvmStatic
        fun getString(@StringRes id: Int, vararg formatArgs: Any?) = getApplicationContext().getString(id, *formatArgs)
        @JvmStatic
        fun getColor(@ColorRes id: Int) = getApplicationContext().getColor(id)
        @JvmStatic
        fun getColorArray(id: Int) = getApplicationContext().resources.getIntArray(id)
        @JvmStatic
        fun getDrawable(@DrawableRes id: Int) = ContextCompat.getDrawable(getApplicationContext(), id)
        fun getInteger(@IntegerRes id: Int) = getApplicationContext().resources.getInteger(id)

        @Throws(Resources.NotFoundException::class)
        fun getQuantityString(@PluralsRes id: Int, quantity: Int, vararg formatArgs: Any?): String {
            return getApplicationContext().resources.getQuantityString(id, quantity, *formatArgs)
        }
    }
}