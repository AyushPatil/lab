package com.labware.lab.utils;

import android.content.Context
import android.graphics.*
import com.google.android.gms.maps.model.LatLng
import com.labware.lab.R
import kotlin.math.abs
import kotlin.math.atan

object MapUtils {

    fun getLargeRiderBitmap(context: Context): Bitmap {
        val bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.ic_rider)
        return Bitmap.createScaledBitmap(bitmap, 200, 200, false)
    }

    fun getSmallRiderBitmap(context: Context): Bitmap {
        val bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.ic_rider)
        return Bitmap.createScaledBitmap(bitmap, 150, 150, false)
    }

    fun getOriginDestinationMarkerBitmap(): Bitmap {
        val height = 20
        val width = 20
        val bitmap = Bitmap.createBitmap(height, width, Bitmap.Config.RGB_565)
        val canvas = Canvas(bitmap)
        val paint = Paint()
        paint.color = Color.BLACK
        paint.style = Paint.Style.FILL
        paint.isAntiAlias = true
        canvas.drawRect(0F, 0F, width.toFloat(), height.toFloat(), paint)
        return bitmap
    }

    fun getOriginMarkerBitmap(context: Context): Bitmap {
        val bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.ic_origin)
        return Bitmap.createScaledBitmap(bitmap, 120, 120, false)
    }

    fun getDestinationMarkerBitmap(context: Context): Bitmap {
        val bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.ic_destination)
        return Bitmap.createScaledBitmap(bitmap, 120, 120, false)
    }

    fun getRotation(start: LatLng, end: LatLng): Float {
        val latDifference: Double = abs(start.latitude - end.latitude)
        val lngDifference: Double = abs(start.longitude - end.longitude)
        var rotation = -1F
        when {
            start.latitude < end.latitude && start.longitude < end.longitude -> {
                rotation = Math.toDegrees(atan(lngDifference / latDifference)).toFloat()
            }
            start.latitude >= end.latitude && start.longitude < end.longitude -> {
                rotation = (90 - Math.toDegrees(atan(lngDifference / latDifference)) + 90).toFloat()
            }
            start.latitude >= end.latitude && start.longitude >= end.longitude -> {
                rotation = (Math.toDegrees(atan(lngDifference / latDifference)) + 180).toFloat()
            }
            start.latitude < end.latitude && start.longitude >= end.longitude -> {
                rotation =
                    (90 - Math.toDegrees(atan(lngDifference / latDifference)) + 270).toFloat()
            }
        }
        return rotation
    }

    /**
     * This function returns the list of locations of Car during the trip i.e. from Origin to Destination
     */
    fun getListOfLocations(): ArrayList<LatLng> {
        val locationList = ArrayList<LatLng>()
        locationList.add(LatLng(18.910117, 72.819963))
        locationList.add(LatLng(18.915072, 72.825190))
        locationList.add(LatLng(18.915865, 72.824535))
        locationList.add(LatLng(18.918690, 72.825566))
        locationList.add(LatLng(18.918785, 72.825534))
        locationList.add(LatLng(18.921730, 72.826576))
        locationList.add(LatLng(18.922929, 72.826754))
        locationList.add(LatLng(18.926359, 72.826987))
        locationList.add(LatLng(18.926540, 72.827103))
        locationList.add(LatLng(18.926646, 72.827026))
        locationList.add(LatLng(18.929614, 72.821960))
        locationList.add(LatLng(18.932494, 72.823373))
        locationList.add(LatLng(18.936102, 72.824237))
        locationList.add(LatLng(18.938623, 72.824290))
        locationList.add(LatLng( 18.941641, 72.823788))
        locationList.add(LatLng(18.943359, 72.823252))
        locationList.add(LatLng(18.946519, 72.821546))
        locationList.add(LatLng(18.949627, 72.819171))
        locationList.add(LatLng(18.952920, 72.816188))
        locationList.add(LatLng(18.953572, 72.815813))
        locationList.add(LatLng(18.953968, 72.815469))
        locationList.add(LatLng(18.955349, 72.813529))
        locationList.add(LatLng(18.955937, 72.811892))
        locationList.add(LatLng(18.957138, 72.812690))
        locationList.add(LatLng(18.958447, 72.810469))
        locationList.add(LatLng(18.961105, 72.808785))
        locationList.add(LatLng(18.964687, 72.807540))
        locationList.add(LatLng(18.966213, 72.807498))
        locationList.add(LatLng(18.970253, 72.809513))
        locationList.add(LatLng(18.970462, 72.809589))
        locationList.add(LatLng(18.971962, 72.809434))
        return locationList
    }
    fun getCarBitmap(context: Context): Bitmap {
        val bitmap = BitmapFactory.decodeResource(context.resources, R.drawable.ic_rider)
        return Bitmap.createScaledBitmap(bitmap, 50, 100, false)
    }

    fun getDestinationBitmap(): Bitmap {
        val height = 20
        val width = 20
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
        val canvas = Canvas(bitmap)
        val paint = Paint()
        paint.color = Color.BLACK
        paint.style = Paint.Style.FILL
        paint.isAntiAlias = true
        canvas.drawRect(0F, 0F, width.toFloat(), height.toFloat(), paint)
        return bitmap
    }

}