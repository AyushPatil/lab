package com.labware.lab.utils;

import android.app.Application;
import android.content.Context;

import com.google.android.libraries.places.api.Places;

import com.google.maps.GeoApiContext;
import com.labware.lab.simulator.Simulator;
import com.labware.lab.R;

public class LabApplication extends Application {

    private static LabApplication mInstance;

    @Override
    public void onCreate()
    {
        mInstance = this;
        super.onCreate();
        Places.initialize(getApplicationContext(), getString(R.string.maps_api_key));
        Simulator.geoApiContext = new GeoApiContext.Builder()
                .apiKey(getString(R.string.maps_api_key))
                .build();
    }

    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(base);
    }

    public static synchronized LabApplication getInstance()
    {
        return mInstance;
    }
}