package com.labware.lab.utils

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.annotation.DrawableRes
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.emreesen.sntoast.SnToast
import com.emreesen.sntoast.Type
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.labware.lab.R


object AppAndroidUtilities {

    fun hideKeyboard(activity: Activity?) {
        val view = activity?.currentFocus
        if (view != null) {
            val inputManager =
                activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(
                view.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        }
    }

    fun hideKeyboardFragment(activity: Activity?, view: View?) {
        if (true) {
            val inputManager =
                activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(
                view?.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        }
    }

    fun showKeyboard(activity: Activity) {
        val view = activity.currentFocus
        if (view != null) {
            val inputManager =
                activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
        }
    }

    fun startFwdAnimation(activity: Activity?) {
        activity?.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }

    fun startBackAnimation(activity: Activity?) {
        activity?.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    fun startTopToBottomAnimation(activity: Activity?) {
        activity?.overridePendingTransition(R.anim.no_change, R.anim.slide_down_info)
    }

    fun startBottomToTopAnimation(activity: Activity?) {
        activity?.overridePendingTransition(R.anim.slide_up_info, R.anim.no_change)
    }

    fun setNewEmptyMessage(
        context: Context?,
        emptyView: LinearLayout,
        @DrawableRes image: Int,
        emptyViewTitle: String?,
        @Nullable emptyViewDesc: String?,
        @Nullable emptyButtonText: String?,
        showEmptyButton: Boolean
    ): View? {
        //remove previous added view
        val layout: ViewGroup = emptyView
        val command = layout.findViewById<View>(R.id.empty_posts_layout)
        layout.removeView(command)
        val inflater = LayoutInflater.from(context)
        val view: View = inflater.inflate(R.layout.new_empty_view, null)
        val emptyViewImage: ImageView = view.findViewById(R.id.empty_view_image)
        val emptyViewTextTitle: com.labware.lab.views.TextView = view.findViewById(R.id.empty_view_title)
        val emptyViewTextDescription: com.labware.lab.views.TextView = view.findViewById(R.id.empty_view_desc)
        val emptyViewButton: com.labware.lab.views.Button = view.findViewById(R.id.empty_view_button)

        //set empty data
        emptyViewTextTitle.setText(emptyViewTitle)
        emptyViewTextDescription.setText(emptyViewDesc)
        if (showEmptyButton) {
            emptyViewButton.setVisibility(View.VISIBLE)
            emptyViewButton.setText(emptyButtonText)
        }
        try {
            if (image != -1) {
                val drawable = ContextCompat.getDrawable(context!!, image)
                emptyViewImage.setImageDrawable(drawable)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return view
        }
        return view
    }


    fun showBanner(context: Context?, message: String?, bannerType: Int)
    {
        when (bannerType) {
            1 -> SnToast.Standard()
                .context(context)
                .type(Type.SUCCESS)
                .message(message)
                .textSize(14)
                .duration(2000)
                .build()
            2 -> SnToast.Standard()
                .context(context)
                .type(Type.INFORMATION)
                .message(message)
                .textSize(14)
                .duration(2000)
                .build()
            3 -> SnToast.Standard()
                .context(context)
                .type(Type.WARNING)
                .message(message)
                .textSize(14)
                .duration(2000)
                .build()
            4 -> SnToast.Standard()
                .context(context)
                .type(Type.ERROR)
                .message(message)
                .textSize(14)
                .duration(2000)
                .build()
            else -> {
            }
        }

    }

    public fun BitmapFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {
        // below line is use to generate a drawable.
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)

        // below line is use to set bounds to our vector drawable.
        vectorDrawable!!.setBounds(
            0,
            0,
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight
        )

        // below line is use to create a bitmap for our
        // drawable which we have added.
        val bitmap = Bitmap.createBitmap(
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )

        // below line is use to add bitmap in our canvas.
        val canvas = Canvas(bitmap)

        // below line is use to draw our
        // vector drawable in canvas.
        vectorDrawable.draw(canvas)

        // after generating our bitmap we are returning our bitmap.
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    fun hideSoftKeyboard(context: Context, view: View) {
        val activity = context as AppCompatActivity
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            ?: return
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showSoftKeyboard(context: Context, view: View?) {
        val activity = context as AppCompatActivity
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm?.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }


    //--- Hide soft keyboard ---//
    fun hideKeyboard(activity: AppCompatActivity?) {
        // Check if no view has focus:
        if (activity != null) {
            val view = activity.currentFocus
            if (view != null) {
                val imm =
                    activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm?.hideSoftInputFromWindow(view.windowToken, 0)
            }
        }
    }
}
