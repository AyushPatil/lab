package com.labware.lab.network

import com.labware.lab.simulator.WebSocket
import com.labware.lab.simulator.WebSocketListener

class NetworkService {

    fun createWebSocket(webSocketListener: WebSocketListener): WebSocket {
        return WebSocket(webSocketListener)
    }

}