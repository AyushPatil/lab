package com.labware.lab.constants

object AppConstantUtils {

    const val CLINIC = "Clinic"
    const val HOME = "Home"
    const val DOCTORS = "Doctors"
    const val HISTORY = "History"
    const val HOSPITAL = "Hospital"
    const val LAB = "Lab"
    const val PATIENT = "Patient"
    const val PHLEBO = "Phlebo"
    const val REQUESTS = "Requests"
    const val TESTS = "Tests"
    const val USERS = "Users"

    const val ADDRESS = "Address"
    const val NAME = "Name"
    const val PHONE = "Phone"
    const val TYPE = "Type"

    const val QUALIFICATION = "Qualification"

    const val VOICE_INSTRUCTIONS = "Voice Instructions"

    const val CERTIFICATION = "Certification"

    const val AVAILABLE = "Available"

    const val LIVE_LOCATION = "Live Location"
    const val GEO_LOCATION = "Geo Location"

    const val SAMPLE_TYPE = "Sample Type"
    const val TEST_NAME = "Test Name"
    const val TEST_PRICE = "Test Price"

    const val AUDIT_TRAIL = "Audit trail"

    const val ACKNOWLEDGE = "Acknowledge"
    const val ACKNOWLEDGE_DROP = "Acknowledge Drop"
    const val ARRIVED = "Arrived"
    const val ARRIVED_AT_DROP_OFF = "Arrived at drop off"
    const val CONTAINER_QR = "Container QR"
    const val CODE = "CODE"
    const val SCAN = "Scan"
    const val TIMESTAMP = "Timestamp"
    const val ENROUTE_COLLECTION_POINT = "Enroute Collection Point"
    const val ENROUTE_DROP_POINT = "Enroute Drop Point"
    const val HANDOVER = "Handover"
    const val SUCCESS = "Success"
    const val ORDER_OF_DRAW = "Order of draw"
    const val PROOF_OF_PRESCRIPTION = "Proof of Prescription"
    const val PHOTO = "Photo"
    const val Sign = "Sign"
    const val OTP = "OTP"
    const val VERIFY = "Verify"
    const val REGISTRATION = "Registration"
    const val BILLING = "Billing"
    const val DISCOUNT = "Discount"
    const val REFERRAL_DOCTOR = "Referral Doctor"
    const val CUT = "Cut"
    const val DOCTOR = "Doctor"
    const val TOTAL_AMOUNT = "Total Amount"
    const val TOTAL_PAID = "Total Paid"
    const val CASH = "Cash"
    const val CREDIT = "Credit"
    const val DEBIT = "Debit"
    const val DUE = "Due"
    const val PAID_TO_HOSPITAL = "Paid to Hospital"
    const val TOTAL_PAYABLE = "Total Payable"
    const val COLLECT_FROM = "Collect From"
    const val CURRENT_STATUS = "Current Status"
    const val DROP_OFF = "Drop Off"
    const val ETA = "ETA"
    const val TAT = "TAT"
    const val PHLEBOTOMIST = "Phlebotomist"

}