
package com.labware.lab.models;

import com.google.firebase.firestore.PropertyName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EnrouteDropPoint {

    @PropertyName("Timestamp")
    private String timestamp;

    @PropertyName("Timestamp")
    public String getTimestamp() {
        return timestamp;
    }

    @PropertyName("Timestamp")
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "EnrouteDropPoint{" +
                "timestamp='" + timestamp + '\'' +
                '}';
    }
}
