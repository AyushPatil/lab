
import java.util.*

object ExpandableListTestsDataItems {
    val data: HashMap<String,
            List<String>>
        get() {
            val expandableDetailList = HashMap<String, List<String>>()
            val totalRs: MutableList<String> = ArrayList()
            totalRs.add("CBC")
            totalRs.add("HIV-I")
            totalRs.add("HIV-II")
            expandableDetailList["Total : 600 Rs"] = totalRs
            return expandableDetailList
        }
}