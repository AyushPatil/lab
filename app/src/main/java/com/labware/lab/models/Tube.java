
package com.labware.lab.models;

import com.google.firebase.firestore.PropertyName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Tube {

    @PropertyName("Name")
    private String name;

    @PropertyName("Code")
    private String code;

    @PropertyName("Verify")
    private Boolean verify;

    @PropertyName("Timestamp")
    private String timestamp;

    @PropertyName("Name")
    public String getName() {
        return name;
    }
    @PropertyName("Name")
    public void setName(String name) {
        this.name = name;
    }

    @PropertyName("Code")
    public String getCode() {
        return code;
    }
    @PropertyName("Code")
    public void setCode(String code) {
        this.code = code;
    }

    @PropertyName("Verify")
    public Boolean getVerify() {
        return verify;
    }
    @PropertyName("Verify")
    public void setVerify(Boolean verify) {
        this.verify = verify;
    }

    @PropertyName("Timestamp")
    public String getTimestamp() {
        return timestamp;
    }
    @PropertyName("Timestamp")
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Tube{" +
                "name='" + name + '\'' +
                ", code='" + code + '\'' +
                ", verify=" + verify +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }
}
