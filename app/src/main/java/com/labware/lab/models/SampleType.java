
package com.labware.lab.models;

import java.util.List;

import com.google.firebase.firestore.PropertyName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SampleType {

    @PropertyName("Tube")
    private List<Tube> tube = null;

    @PropertyName("Tube")
    public List<Tube> getTube() {
        return tube;
    }
    @PropertyName("Tube")
    public void setTube(List<Tube> tube) {
        this.tube = tube;
    }

    @Override
    public String toString() {
        return "SampleType{" +
                "tube=" + tube +
                '}';
    }
}
