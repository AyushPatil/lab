package com.labware.lab.models;

import java.util.List;

import com.google.firebase.firestore.PropertyName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Lab {

    @PropertyName("Certifications")
    private List<String> certifications = null;

    @PropertyName("Address")
    private Address address;

    @PropertyName("Name")
    private String name;

    @PropertyName("Number")
    private String number;

    @PropertyName("Certifications")
    public List<String> getCertifications() {
        return certifications;
    }
    @PropertyName("Certifications")
    public void setCertifications(List<String> certifications) {
        this.certifications = certifications;
    }

    @PropertyName("Address")
    public Address getAddress() {
        return address;
    }
    @PropertyName("Address")
    public void setAddress(Address address) {
        this.address = address;
    }

    @PropertyName("Name")
    public String getName() {
        return name;
    }
    @PropertyName("Name")
    public void setName(String name) {
        this.name = name;
    }

    @PropertyName("Number")
    public String getNumber() {
        return number;
    }
    @PropertyName("Number")
    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Lab{" +
                "certifications=" + certifications +
                ", address=" + address +
                ", name='" + name + '\'' +
                ", number='" + number + '\'' +
                '}';
    }
}