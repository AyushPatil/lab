package com.labware.lab.models;

import com.google.firebase.firestore.PropertyName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Hospital {

    @PropertyName("Type")
    private String type;

    @PropertyName("Name")
    private String name;

    @PropertyName("Phone")
    private String phone;

    @PropertyName("Flat number")
    private String flatNumber;

    @PropertyName("Society name")
    private String societyName;

    @PropertyName("Locality")
    private String locality;

    @PropertyName("Voice instruction")
    private String voiceInstruction;

    @PropertyName("Address")
    private Address address;

    @PropertyName("Lab Id")
    private String labId;

    @PropertyName("Request Id")
    private String requestId;
    @PropertyName("Request Id")
    public String getRequestId() {
        return requestId;
    }
    @PropertyName("Request Id")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @PropertyName("Lab Id")
    public String getLabId() {
        return labId;
    }
    @PropertyName("Lab Id")
    public void setLabId(String labId) {
        this.labId = labId;
    }


    @PropertyName("Type")
    public String getType() {
        return type;
    }
    @PropertyName("Type")
    public void setType(String type) {
        this.type = type;
    }

    @PropertyName("Name")
    public String getName() {
        return name;
    }
    @PropertyName("Name")
    public void setName(String name) {
        this.name = name;
    }

    @PropertyName("Phone")
    public String getPhone() {
        return phone;
    }
    @PropertyName("Phone")
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @PropertyName("Flat number")
    public String getFlatNumber() {
        return flatNumber;
    }
    @PropertyName("Flat number")
    public void setFlatNumber(String flatNumber) {
        this.flatNumber = flatNumber;
    }

    @PropertyName("Society name")
    public String getSocietyName() {
        return societyName;
    }
    @PropertyName("Society name")
    public void setSocietyName(String societyName) {
        this.societyName = societyName;
    }

    @PropertyName("Locality")
    public String getLocality() {
        return locality;
    }
    @PropertyName("Locality")
    public void setLocality(String locality) {
        this.locality = locality;
    }

    @PropertyName("Voice instruction")
    public String getVoiceInstruction() {
        return voiceInstruction;
    }
    @PropertyName("Voice instruction")
    public void setVoiceInstruction(String voiceInstruction) {
        this.voiceInstruction = voiceInstruction;
    }

    @PropertyName("Address")
    public Address getAddress() {
        return address;
    }
    @PropertyName("Address")
    public void setAddress(Address address) {
        this.address = address;
    }


    @Override
    public String toString() {
        return "Hospital{" +
                "type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", flatNumber='" + flatNumber + '\'' +
                ", societyName='" + societyName + '\'' +
                ", locality='" + locality + '\'' +
                ", voiceInstruction='" + voiceInstruction + '\'' +
                ", address=" + address +
                ", labId='" + labId + '\'' +
                '}';
    }
}