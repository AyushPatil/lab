package com.labware.lab.models

class MessageEvent(var mMessage: String, var from: String) {

    fun getMessage(): String {
        return mMessage
    }

}