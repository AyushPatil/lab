package com.labware.lab.models;

public class Config {
    public static final String SHARED_PREFERENCES_NAME = "com.labware.lab.my_shared_preferences";
    public static final String LAB_ID = "lab_id";
    public static final String IS_REGISTERED = "is_registered";

}
