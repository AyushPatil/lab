
package com.labware.lab.models;

import java.util.List;

import com.google.firebase.firestore.PropertyName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProofOfPrescription {

    @PropertyName("Prescription photo")
    private String prescriptionPhoto;

    @PropertyName("Patient sign")
    private String patientSign;

    @PropertyName("Timestamp")
    private String timestamp;

    @PropertyName("Payment mode")
    private String paymentMode;

    @PropertyName("Tests")
    private List<String> tests = null;

    @PropertyName("Prescription photo")
    public String getPrescriptionPhoto() {
        return prescriptionPhoto;
    }
    @PropertyName("Prescription photo")
    public void setPrescriptionPhoto(String prescriptionPhoto) {
        this.prescriptionPhoto = prescriptionPhoto;
    }

    @PropertyName("Patient sign")
    public String getPatientSign() {
        return patientSign;
    }
    @PropertyName("Patient sign")
    public void setPatientSign(String patientSign) {
        this.patientSign = patientSign;
    }

    @PropertyName("Timestamp")
    public String getTimestamp() {
        return timestamp;
    }
    @PropertyName("Timestamp")
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @PropertyName("Payment mode")
    public String getPaymentMode() {
        return paymentMode;
    }
    @PropertyName("Payment mode")
    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    @PropertyName("Tests")
    public List<String> getTests() {
        return tests;
    }
    @PropertyName("Tests")
    public void setTests(List<String> tests) {
        this.tests = tests;
    }

    @Override
    public String toString() {
        return "ProofOfPrescription{" +
                "prescriptionPhoto='" + prescriptionPhoto + '\'' +
                ", patientSign='" + patientSign + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", paymentMode='" + paymentMode + '\'' +
                ", tests=" + tests +
                '}';
    }
}
