package com.labware.lab.models;

public class MedicalTests {
    String testName;
    String price;
    String tubeType;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    boolean isSelected;

    public String getTubeType() {
        return tubeType;
    }

    public void setTubeType(String tubeType) {
        this.tubeType = tubeType;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "MedicalTests{" +
                "testName='" + testName + '\'' +
                ", price='" + price + '\'' +
                ", tubeType='" + tubeType + '\'' +
                ", isSelected=" + isSelected +
                '}';
    }
}
