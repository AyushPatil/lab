
package com.labware.lab.models;

import com.google.firebase.firestore.PropertyName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProofOfCollection {

    @PropertyName("Patient Name")
    private String patientName;

    @PropertyName("OTP")
    private String otp;

    @PropertyName("Mobile")
    private String mobile;

    @PropertyName("Timestamp")
    private String timestamp;

    @PropertyName("Verify")
    private Boolean verify;

    @PropertyName("Patient Name")
    public String getPatientName() {
        return patientName;
    }
    @PropertyName("Patient Name")
    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    @PropertyName("OTP")
    public String getOtp() {
        return otp;
    }
    @PropertyName("OTP")
    public void setOtp(String otp) {
        this.otp = otp;
    }

    @PropertyName("Mobile")
    public String getMobile() {
        return mobile;
    }
    @PropertyName("Mobile")
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @PropertyName("Timestamp")
    public String getTimestamp() {
        return timestamp;
    }
    @PropertyName("Timestamp")
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @PropertyName("Verify")
    public Boolean getVerify() {
        return verify;
    }
    @PropertyName("Verify")
    public void setVerify(Boolean verify) {
        this.verify = verify;
    }

    @Override
    public String toString() {
        return "ProofOfCollection{" +
                "patientName='" + patientName + '\'' +
                ", otp='" + otp + '\'' +
                ", mobile='" + mobile + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", verify=" + verify +
                '}';
    }
}
