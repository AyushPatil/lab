
package com.labware.lab.models;

import com.google.firebase.firestore.PropertyName;

public class AuditTrail {

    @PropertyName("Acknowledge collection")
    private AcknowledgeCollection acknowledgeCollection;

    @PropertyName("Start collection")
    private StartCollection startCollection;

    @PropertyName("Arrived at collection")
    private ArrivedAtCollection arrivedAtCollection;

    @PropertyName("Proof of collection")
    private ProofOfCollection proofOfCollection;

    @PropertyName("Registration")
    private Registration registration;

    @PropertyName("Proof of prescription")
    private ProofOfPrescription proofOfPrescription;

    @PropertyName("Order of draw")
    private OrderOfDraw orderOfDraw;

    @PropertyName("Container QR")
    private ContainerQR containerQR;

    @PropertyName("Acknowledge drop off")
    private AcknowledgeDropOff acknowledgeDropOff;

    @PropertyName("Start drop off")
    private StartDropOff startDropOff;

    @PropertyName("Arrived at drop off")
    private ArrivedAtDropOff arrivedAtDropOff;

    @PropertyName("Enroute collection point")
    private EnrouteCollectionPoint enrouteCollectionPoint;

    @PropertyName("Enroute drop point")
    private EnrouteDropPoint enrouteDropPoint;

    @PropertyName("Handover")
    private Handover handover;

    @PropertyName("Collect from")
    private String collectFrom;

    @PropertyName("Current status")
    private String currentStatus;

    @PropertyName("Drop off")
    private String dropOff;

    @PropertyName("ETA")
    private Integer eta;

    @PropertyName("Patient")
    private String patient;

    @PropertyName("Phlebotomist")
    private String phlebotomist;

    @PropertyName("TAT")
    private Integer tat;

    @PropertyName("Timestamp")
    private String timestamp;

    @PropertyName("Acknowledge collection")
    public AcknowledgeCollection getAcknowledgeCollection() {
        return acknowledgeCollection;
    }
    @PropertyName("Acknowledge collection")
    public void setAcknowledgeCollection(AcknowledgeCollection acknowledgeCollection) {
        this.acknowledgeCollection = acknowledgeCollection;
    }

    @PropertyName("Start collection")
    public StartCollection getStartCollection() {
        return startCollection;
    }
    @PropertyName("Start collection")
    public void setStartCollection(StartCollection startCollection) {
        this.startCollection = startCollection;
    }

    @PropertyName("Arrived at collection")
    public ArrivedAtCollection getArrivedAtCollection() {
        return arrivedAtCollection;
    }
    @PropertyName("Arrived at collection")
    public void setArrivedAtCollection(ArrivedAtCollection arrivedAtCollection) {
        this.arrivedAtCollection = arrivedAtCollection;
    }

    @PropertyName("Proof of collection")
    public ProofOfCollection getProofOfCollection() {
        return proofOfCollection;
    }
    @PropertyName("Proof of collection")
    public void setProofOfCollection(ProofOfCollection proofOfCollection) {
        this.proofOfCollection = proofOfCollection;
    }

    @PropertyName("Registration")
    public Registration getRegistration() {
        return registration;
    }
    @PropertyName("Registration")
    public void setRegistration(Registration registration) {
        this.registration = registration;
    }

    @PropertyName("Proof of prescription")
    public ProofOfPrescription getProofOfPrescription() {
        return proofOfPrescription;
    }
    @PropertyName("Proof of prescription")
    public void setProofOfPrescription(ProofOfPrescription proofOfPrescription) {
        this.proofOfPrescription = proofOfPrescription;
    }

    @PropertyName("Order of draw")
    public OrderOfDraw getOrderOfDraw() {
        return orderOfDraw;
    }
    @PropertyName("Order of draw")
    public void setOrderOfDraw(OrderOfDraw orderOfDraw) {
        this.orderOfDraw = orderOfDraw;
    }

    @PropertyName("Container QR")
    public ContainerQR getContainerQR() {
        return containerQR;
    }
    @PropertyName("Container QR")
    public void setContainerQR(ContainerQR containerQR) {
        this.containerQR = containerQR;
    }

    @PropertyName("Acknowledge drop off")
    public AcknowledgeDropOff getAcknowledgeDropOff() {
        return acknowledgeDropOff;
    }
    @PropertyName("Acknowledge drop off")
    public void setAcknowledgeDropOff(AcknowledgeDropOff acknowledgeDropOff) {
        this.acknowledgeDropOff = acknowledgeDropOff;
    }

    @PropertyName("Start drop off")
    public StartDropOff getStartDropOff() {
        return startDropOff;
    }
    @PropertyName("Start drop off")
    public void setStartDropOff(StartDropOff startDropOff) {
        this.startDropOff = startDropOff;
    }

    @PropertyName("Arrived at drop off")
    public ArrivedAtDropOff getArrivedAtDropOff() {
        return arrivedAtDropOff;
    }
    @PropertyName("Arrived at drop off")
    public void setArrivedAtDropOff(ArrivedAtDropOff arrivedAtDropOff) {
        this.arrivedAtDropOff = arrivedAtDropOff;
    }

    @PropertyName("Enroute collection point")
    public EnrouteCollectionPoint getEnrouteCollectionPoint() {
        return enrouteCollectionPoint;
    }
    @PropertyName("Enroute collection point")
    public void setEnrouteCollectionPoint(EnrouteCollectionPoint enrouteCollectionPoint) {
        this.enrouteCollectionPoint = enrouteCollectionPoint;
    }

    @PropertyName("Enroute drop point")
    public EnrouteDropPoint getEnrouteDropPoint() {
        return enrouteDropPoint;
    }
    @PropertyName("Enroute drop point")
    public void setEnrouteDropPoint(EnrouteDropPoint enrouteDropPoint) {
        this.enrouteDropPoint = enrouteDropPoint;
    }

    @PropertyName("Handover")
    public Handover getHandover() {
        return handover;
    }
    @PropertyName("Handover")
    public void setHandover(Handover handover) {
        this.handover = handover;
    }

    @PropertyName("Collect from")
    public String getCollectFrom() {
        return collectFrom;
    }
    @PropertyName("Collect from")
    public void setCollectFrom(String collectFrom) {
        this.collectFrom = collectFrom;
    }

    @PropertyName("Current status")
    public String getCurrentStatus() {
        return currentStatus;
    }
    @PropertyName("Current status")
    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    @PropertyName("Drop off")
    public String getDropOff() {
        return dropOff;
    }
    @PropertyName("Drop off")
    public void setDropOff(String dropOff) {
        this.dropOff = dropOff;
    }

    @PropertyName("ETA")
    public Integer getEta() {
        return eta;
    }
    @PropertyName("ETA")
    public void setEta(Integer eta) {
        this.eta = eta;
    }

    @PropertyName("Patient")
    public String getPatient() {
        return patient;
    }
    @PropertyName("Patient")
    public void setPatient(String patient) {
        this.patient = patient;
    }

    @PropertyName("Phlebotomist")
    public String getPhlebotomist() {
        return phlebotomist;
    }
    @PropertyName("Phlebotomist")
    public void setPhlebotomist(String phlebotomist) {
        this.phlebotomist = phlebotomist;
    }

    @PropertyName("TAT")
    public Integer getTat() {
        return tat;
    }
    @PropertyName("TAT")
    public void setTat(Integer tat) {
        this.tat = tat;
    }

    @PropertyName("Timestamp")
    public String getTimestamp() {
        return timestamp;
    }
    @PropertyName("Timestamp")
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "AuditTrail{" +
                "acknowledgeCollection=" + acknowledgeCollection +
                ", startCollection=" + startCollection +
                ", arrivedAtCollection=" + arrivedAtCollection +
                ", proofOfCollection=" + proofOfCollection +
                ", registration=" + registration +
                ", proofOfPrescription=" + proofOfPrescription +
                ", orderOfDraw=" + orderOfDraw +
                ", containerQR=" + containerQR +
                ", acknowledgeDropOff=" + acknowledgeDropOff +
                ", startDropOff=" + startDropOff +
                ", arrivedAtDropOff=" + arrivedAtDropOff +
                ", enrouteCollectionPoint=" + enrouteCollectionPoint +
                ", enrouteDropPoint=" + enrouteDropPoint +
                ", handover=" + handover +
                ", collectFrom='" + collectFrom + '\'' +
                ", currentStatus='" + currentStatus + '\'' +
                ", dropOff='" + dropOff + '\'' +
                ", eta=" + eta +
                ", patient='" + patient + '\'' +
                ", phlebotomist='" + phlebotomist + '\'' +
                ", tat=" + tat +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }
}
