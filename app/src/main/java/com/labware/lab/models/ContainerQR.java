
package com.labware.lab.models;

import com.google.firebase.firestore.PropertyName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContainerQR {

    @PropertyName("Code")
    private String code;

    @PropertyName("Scan")
    private Boolean scan;

    @PropertyName("Timestamp")
    private String timestamp;

    @PropertyName("Code")
    public String getCode() {
        return code;
    }
    @PropertyName("Code")
    public void setCode(String code) {
        this.code = code;
    }

    @PropertyName("Scan")
    public Boolean getScan() {
        return scan;
    }
    @PropertyName("Scan")
    public void setScan(Boolean scan) {
        this.scan = scan;
    }

    @PropertyName("Timestamp")
    public String getTimestamp() {
        return timestamp;
    }
    @PropertyName("Timestamp")
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "ContainerQR{" +
                "code='" + code + '\'' +
                ", scan=" + scan +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }
}
