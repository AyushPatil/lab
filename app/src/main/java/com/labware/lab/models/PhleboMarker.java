package com.labware.lab.models;

public class PhleboMarker {

    String phleboName;
    String phleboContactNumber;
    String lattitude;
    String longitude;
    String requestId;
    String labId;
    String markerId;
    boolean isAvailable;
    String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMarkerId() {
        return markerId;
    }

    public void setMarkerId(String markerId) {
        this.markerId = markerId;
    }

    public String getPhleboName() {
        return phleboName;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    public void setPhleboName(String phleboName) {
        this.phleboName = phleboName;
    }

    public String getPhleboContactNumber() {
        return phleboContactNumber;
    }

    public void setPhleboContactNumber(String phleboContactNumber) {
        this.phleboContactNumber = phleboContactNumber;
    }

    public String getLattitude() {
        return lattitude;
    }

    public void setLattitude(String lattitude) {
        this.lattitude = lattitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    @Override
    public String toString() {
        return "PhleboMarker{" +
                "phleboName='" + phleboName + '\'' +
                ", phleboContactNumber='" + phleboContactNumber + '\'' +
                ", lattitude='" + lattitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", requestId='" + requestId + '\'' +
                ", labId='" + labId + '\'' +
                ", markerId='" + markerId + '\'' +
                ", isAvailable=" + isAvailable +
                '}';
    }
}
