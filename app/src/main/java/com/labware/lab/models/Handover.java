
package com.labware.lab.models;

import com.google.firebase.firestore.PropertyName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Handover {

    @PropertyName("Success")
    private Boolean success;

    @PropertyName("Timestamp")
    private String timestamp;

    @PropertyName("Success")
    public Boolean getSuccess() {
        return success;
    }

    @PropertyName("Success")
    public void setSuccess(Boolean success) {
        this.success = success;
    }

    @PropertyName("Timestamp")
    public String getTimestamp() {
        return timestamp;
    }

    @PropertyName("Timestamp")
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Handover{" +
                "success=" + success +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }
}
