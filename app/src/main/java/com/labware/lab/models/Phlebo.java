package com.labware.lab.models;

import com.google.firebase.firestore.PropertyName;

import java.util.List;

public class Phlebo {

    @PropertyName("Address")
    private Address address;

    @PropertyName("Name")
    private String name;

    @PropertyName("Number")
    private String number;

    @PropertyName("Available")
    private boolean available;

    @PropertyName("Request Id")
    private String requestId;

    @PropertyName("Lab Id")
    private String labId;

    @PropertyName("Status")
    private String status;

    @PropertyName("Status")
    public String getStatus() {
        return status;
    }
    @PropertyName("Status")
    public void setStatus(String status) {
        this.status = status;
    }

    @PropertyName("Request Id")
    public String getRequestId() {
        return requestId;
    }
    @PropertyName("Request Id")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @PropertyName("Lab Id")
    public String getLabId() {
        return labId;
    }
    @PropertyName("Lab Id")
    public void setLabId(String labId) {
        this.labId = labId;
    }

    @PropertyName("Available")
    public boolean isAvailable() {
        return available;
    }
    @PropertyName("Available")
    public void setAvailable(boolean available) {
        this.available = available;
    }

    @PropertyName("Address")
    public Address getAddress() {
        return address;
    }
    @PropertyName("Address")
    public void setAddress(Address address) {
        this.address = address;
    }

    @PropertyName("Name")
    public String getName() {
        return name;
    }
    @PropertyName("Name")
    public void setName(String name) {
        this.name = name;
    }

    @PropertyName("Number")
    public String getNumber() {
        return number;
    }
    @PropertyName("Number")
    public void setNumber(String number) {
        this.number = number;
    }

}