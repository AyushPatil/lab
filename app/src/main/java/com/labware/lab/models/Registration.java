
package com.labware.lab.models;

import java.util.List;

import com.google.firebase.firestore.PropertyName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Registration {

    @PropertyName("Patient name")
    private String patientName;

    @PropertyName("Age")
    private String age;

    @PropertyName("Gender")
    private String gender;

    @PropertyName("Medical history")
    private List<String> medicalHistory = null;

    @PropertyName("Timestamp")
    private String timestamp;

    @PropertyName("Patient name")
    public String getPatientName() {
        return patientName;
    }
    @PropertyName("Patient name")
    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    @PropertyName("Age")
    public String getAge() {
        return age;
    }
    @PropertyName("Age")
    public void setAge(String age) {
        this.age = age;
    }

    @PropertyName("Gender")
    public String getGender() {
        return gender;
    }
    @PropertyName("Gender")
    public void setGender(String gender) {
        this.gender = gender;
    }

    @PropertyName("Medical history")
    public List<String> getMedicalHistory() {
        return medicalHistory;
    }
    @PropertyName("Medical history")
    public void setMedicalHistory(List<String> medicalHistory) {
        this.medicalHistory = medicalHistory;
    }

    @PropertyName("Timestamp")
    public String getTimestamp() {
        return timestamp;
    }
    @PropertyName("Timestamp")
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }


    @Override
    public String toString() {
        return "Registration{" +
                "patientName='" + patientName + '\'' +
                ", age='" + age + '\'' +
                ", gender='" + gender + '\'' +
                ", medicalHistory=" + medicalHistory +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }
}
