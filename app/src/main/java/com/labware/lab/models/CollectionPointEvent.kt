package com.labware.lab.models

class CollectionPointEvent(var mMessage: String, var mCollectFrom:String,var mdropOff:String,var mRequestFrom:String, var mFrom: String, var mOriginLatLng:String,var mDestinationLatLng:String) {

    fun getMessage(): String {
        return mMessage
    }

    fun getCollectFrom(): String {
        return mCollectFrom
    }

    fun getDropOff(): String {
        return mdropOff
    }

    fun getFrom(): String {
        return mFrom
    }

    fun getRequestFrom(): String {
        return mRequestFrom
    }

    fun getOriginLatLng(): String {
        return mOriginLatLng
    }

    fun getDestinationLatLng(): String {
        return mDestinationLatLng
    }

}