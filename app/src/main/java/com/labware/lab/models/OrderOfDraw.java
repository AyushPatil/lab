
package com.labware.lab.models;

import com.google.firebase.firestore.PropertyName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderOfDraw {

    @PropertyName("Sample type")
    private SampleType sampleType;

    @PropertyName("Timestamp")
    private String timestamp;

    @PropertyName("Sample type")
    public SampleType getSampleType() {
        return sampleType;
    }

    @PropertyName("Sample type")
    public void setSampleType(SampleType sampleType) {
        this.sampleType = sampleType;
    }

    @PropertyName("Timestamp")
    public String getTimestamp() {
        return timestamp;
    }

    @PropertyName("Timestamp")
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "OrderOfDraw{" +
                "sampleType=" + sampleType +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }
}
