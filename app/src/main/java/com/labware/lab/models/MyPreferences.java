package com.labware.lab.models;

import android.content.Context;
import android.content.SharedPreferences;

public class MyPreferences {

    private static MyPreferences myPreferences;
    private static SharedPreferences sharedPreferences;
    private static SharedPreferences.Editor editor;

    private MyPreferences(Context context) {
        sharedPreferences = context.getSharedPreferences(Config.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.apply();
    }

    public static MyPreferences getPreferences(Context context) {
        if (myPreferences == null) myPreferences = new MyPreferences(context);
        return myPreferences;
    }

    public void setLabID(String labID){
        editor.putString(Config.LAB_ID, labID);
        editor.apply();
    }

    public String getLabID(){
        //if no data is available for Config.USER_NAME then this getString() method returns
        //a default value that is mentioned in second parameter
        return sharedPreferences.getString(Config.LAB_ID, "0");
    }

    public void setRegistrationStatus(boolean isRegistered){
        editor.putBoolean(Config.IS_REGISTERED, isRegistered);
        editor.apply();
    }

    public boolean getRegistrationStatus(){
        return sharedPreferences.getBoolean(Config.IS_REGISTERED, false);
    }
}