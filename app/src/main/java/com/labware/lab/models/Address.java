package com.labware.lab.models;
import com.google.firebase.firestore.PropertyName;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Address {

    @PropertyName("Lattitude")
    private String lattitude;

    @PropertyName("Longitude")
    private String longitude;

    @PropertyName("Lattitude")
    public String getLattitude() {
        return lattitude;
    }

    @PropertyName("Lattitude")
    public void setLattitude(String lattitude) {
        this.lattitude = lattitude;
    }

    @PropertyName("Longitude")
    public String getLongitude() {
        return longitude;
    }

    @PropertyName("Longitude")
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        return "Address{" +
                "lattitude='" + lattitude + '\'' +
                ", longitude='" + longitude + '\'' +
                '}';
    }
}
