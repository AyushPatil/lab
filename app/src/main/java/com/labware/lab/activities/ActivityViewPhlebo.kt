package com.labware.lab.activities

import `in`.shadowfax.proswipebutton.ProSwipeButton
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.beust.klaxon.JsonArray
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.AutocompleteActivity
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.labware.lab.R
import com.labware.lab.constants.AppConstantUtils
import com.labware.lab.maps.MapsActivity
import com.labware.lab.maps.MapsView
import com.labware.lab.models.MyPreferences
import com.labware.lab.network.NetworkService
import com.labware.lab.ui.maps.MapsPresenter
import com.labware.lab.utils.*
import com.labware.lab.views.TextView
import com.squats.fittr.utils.LabResourceHelper
import kotlinx.android.synthetic.main.activity_maps.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import org.jetbrains.anko.custom.async
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONObject
import java.net.URL

class ActivityViewPhlebo : AppCompatActivity(){

    var toolbar: Toolbar? = null
    var rawArray = intArrayOf(R.raw.uber_style)
    private var ll_frame: FrameLayout? = null
    companion object {
        private const val TAG = "ActivityViewPhlebo"
        private const val LOCATION_PERMISSION_REQUEST_CODE = 999
    }

    var mMapView: MapView? = null
    private var googleMap: GoogleMap? = null
    private lateinit var defaultLocation: LatLng
    private var movingCabMarker: Marker? = null
    private var previousLatLng: LatLng? = null
    private var currentLatLng: LatLng? = null
    private lateinit var handler: Handler
    private lateinit var runnable: Runnable
    private var btn_swipe_to_start_collection: ProSwipeButton? = null
    private var activity: Activity? = null
    private var mFirebaseDatabaseInstances: FirebaseFirestore? = null
    private var mAuth: FirebaseAuth? = null
    private var myPreferences: MyPreferences? = null
    private var txtTatTime: TextView? = null
    private var txtEtaTime: TextView? = null
    private var txtPlaceName: TextView? = null
    private var mContext: Context? = null
    private var originLatLng: String? = null
    private var destinationLatLng: String? = null
    private var requestId: String? = null
    private var phleboContactNumber: String? = null
    private var latLngList:ArrayList<LatLng>?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_phlebo)
        mContext=this
        toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        latLngList = ArrayList<LatLng>()
        latLngList?.clear()

        if(intent!=null)
        {
            requestId=intent.getStringExtra("REQUEST_ID")
            phleboContactNumber=intent.getStringExtra("PHLEBO_CONTACT_NO")
        }
        toolbar!!.setNavigationIcon(R.drawable.ic_arrow_back_white)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar!!.title="View phlebo details"
        toolbar!!.setNavigationOnClickListener(View.OnClickListener {
            finish()
            AppAndroidUtilities.startTopToBottomAnimation(this@ActivityViewPhlebo)
        })
        ll_frame = findViewById(R.id.ll_frame) as FrameLayout
        ll_frame?.setBackgroundColor(LabResourceHelper.getColor(R.color.map_bg_none))
        ProgressDialogManager.showProgressDialog(this@ActivityViewPhlebo)
        Handler().postDelayed({
            findViewById<ProgressBar>(R.id.progressBar).visibility = View.INVISIBLE
            ll_frame?.setBackgroundColor(LabResourceHelper.getColor(R.color.map_bg_three))
        }, 3000)

        val img_call = findViewById<ImageView>(R.id.img_call)
        img_call.setOnClickListener(View.OnClickListener {
            val callIntent = Intent(Intent.ACTION_DIAL)
            callIntent.data = Uri.parse("tel:" + phleboContactNumber)
            //callIntent.setPackage("com.google.android.dialer")
            startActivity(callIntent)
        })

        mAuth = FirebaseAuth.getInstance()
        mFirebaseDatabaseInstances = FirebaseFirestore.getInstance()
        val settings = FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true)
            .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
            .build()
        mFirebaseDatabaseInstances?.setFirestoreSettings(settings)
        myPreferences = MyPreferences.getPreferences(this)

        txtTatTime = findViewById(R.id.txt_tat_time) as TextView
        txtEtaTime = findViewById(R.id.txt_eta_time) as TextView
        txtPlaceName = findViewById(R.id.txt_place_name) as TextView

        mMapView = findViewById(R.id.mapView)
        mMapView!!.onCreate(savedInstanceState)
        mMapView!!.onResume() // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(this)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        mMapView!!.getMapAsync { mMap ->
            googleMap = mMap

            val docRef1 =
                mFirebaseDatabaseInstances?.collection(AppConstantUtils.PHLEBO)?.document(
                    mAuth?.currentUser?.phoneNumber?.replace("+91","").toString()
                )
            docRef1?.addSnapshotListener { snapshot, e ->
                if (e != null) {
                    Log.w("TAG", "Listen failed.", e)
                    return@addSnapshotListener
                }

                if (snapshot != null && snapshot.exists()) {
                    Log.d("TAG", "Current data: ${snapshot.data}")
                    val obj = snapshot.data?.get("Address") as HashMap<*, *>
                    var locationLat = obj.get("Lattitude").toString()!!
                    var locationLong = obj.get("Longitude").toString()!!
                    if (locationLat != null && locationLong!= null) {
                        // create a LatLng object from location
                        val latLng = LatLng(locationLat.toDouble(), locationLong.toDouble())
                        latLngList?.add(
                            LatLng(
                                locationLat.toDouble(),
                                locationLong.toDouble()
                            )
                        )

                        Handler().postDelayed(Runnable {
                            getETAFromAddress(mContext!!, locationLat!!, locationLong!!)
                            updateCarLocation(latLng)
                        }, 1000)

                    }
                } else {
                    Log.d("TAG", "Current data: null")
                }
            }

            val docRef =
                mFirebaseDatabaseInstances?.collection(AppConstantUtils.REQUESTS)?.document(
                   requestId!!
                )
            docRef?.get()?.addOnSuccessListener { documentSnapshot ->
                ProgressDialogManager.dismissProgressDialog(this)
                val obj = documentSnapshot.get("Audit trail") as HashMap<*, *>
                var originLatLng = obj.get("Origin LatLng").toString()!!
                var destinationLatLng = obj.get("Destination LatLng").toString()!!
                val msgArrayOriginLatLng: Array<String> = originLatLng.split("/".toRegex()).toTypedArray()
                val msgArrayDestinationLatLng: Array<String> = destinationLatLng.split("/".toRegex()).toTypedArray()
                defaultLocation = LatLng(
                    msgArrayOriginLatLng[0].toDouble(),
                    msgArrayOriginLatLng[1].toDouble()
                )

                latLngList?.add(
                    LatLng(
                        msgArrayDestinationLatLng[0].toDouble(),
                        msgArrayDestinationLatLng[1].toDouble()
                    )
                )
                showDefaultLocationOnMap(defaultLocation)

            }
            docRef?.get()?.addOnFailureListener(OnFailureListener {
            })
            try {
                googleMap?.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                        this, rawArray[0]
                    )
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }

            googleMap?.setOnMarkerClickListener(object : GoogleMap.OnMarkerClickListener {
                override fun onMarkerClick(marker: Marker): Boolean {
                    return false
                }
            })


            Handler().postDelayed(Runnable {
                showPath(latLngList!!)

            }, 1000)

        }

    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                AppAndroidUtilities.startTopToBottomAnimation(this@ActivityViewPhlebo)
                return true
            }
        }
        return super.onContextItemSelected(item)
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    finish()
                    AppAndroidUtilities.startTopToBottomAnimation(this@ActivityViewPhlebo)
                    return true
                }
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        AppAndroidUtilities.startTopToBottomAnimation(this@ActivityViewPhlebo)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home -> {
                finish()
                AppAndroidUtilities.startTopToBottomAnimation(this@ActivityViewPhlebo)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStart() {
        super.onStart()
    }

    private fun moveCamera(latLng: LatLng) {
        googleMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
    }

    private fun animateCamera(latLng: LatLng) {
        val cameraPosition = CameraPosition.Builder().target(latLng).zoom(11.7f).build()
        googleMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    private fun addCarMarkerAndGet(latLng: LatLng): Marker {
        val bitmapDescriptor =
            BitmapDescriptorFactory.fromBitmap(MapUtils.getSmallRiderBitmap(this))
        return googleMap?.addMarker(
            MarkerOptions().position(latLng).flat(true).icon(bitmapDescriptor)
        )!!
    }

    private fun addOriginMarkerAndGet(latLng: LatLng): Marker {
        val bitmapDescriptor =
            BitmapDescriptorFactory.fromBitmap(MapUtils.getOriginMarkerBitmap(this))
        return googleMap!!.addMarker(
            MarkerOptions().position(latLng).flat(true).icon(bitmapDescriptor)
        )
    }

    private fun addDestinationMarkerAndGet(latLng: LatLng): Marker {
        val bitmapDescriptor =
            BitmapDescriptorFactory.fromBitmap(MapUtils.getDestinationMarkerBitmap(this))
        return googleMap!!.addMarker(
            MarkerOptions().position(latLng).flat(true).icon(bitmapDescriptor)
        )
    }

    private fun showDefaultLocationOnMap(latLng: LatLng) {
        moveCamera(latLng)
        animateCamera(latLng)
    }

    /**
     * This function is used to draw the path between the Origin and Destination.
     */
    private fun showPath(latLngList: ArrayList<LatLng>) {

        try {
            val LatLongB = LatLngBounds.Builder()
            // Add markers
            val origin = LatLng(latLngList.get(0).latitude, latLngList.get(0).longitude)
            val destination = LatLng(latLngList.get(1).latitude, latLngList.get(1).longitude)
            val originbitmapDescriptor = BitmapDescriptorFactory.fromBitmap(
                MapUtils.getOriginMarkerBitmap(
                    this
                )
            )
            val destinationbitmapDescriptor = BitmapDescriptorFactory.fromBitmap(
                MapUtils.getDestinationMarkerBitmap(
                    this
                )
            )
            googleMap!!.addMarker(
                MarkerOptions().position(origin).flat(true).icon(
                    originbitmapDescriptor
                )
            )
            googleMap!!.addMarker(
                MarkerOptions().position(destination).flat(true).icon(
                    destinationbitmapDescriptor
                )
            )

            // Declare polyline object and set up color and width
            val options = PolylineOptions()
            options.color(Color.BLACK)
            options.width(10f)

            // build URL to call API
            val url = getURL(origin, destination)

            async {
                // Connect to URL, download content and convert into string asynchronously
                val result = URL(url).readText()
                uiThread {
                    try {
                        // When API call is done, create parser and convert into JsonObjec
                        val parser: Parser = Parser()
                        val stringBuilder: StringBuilder = StringBuilder(result)
                        val json: JsonObject = parser.parse(stringBuilder) as JsonObject
                        // get to the correct element in JsonObject
                        val routes = json.array<JsonObject>("routes")
                        val points = routes!!["legs"]["steps"][0] as JsonArray<JsonObject>
                        // For every element in the JsonArray, decode the polyline string and pass all points to a List
                        val polypts =
                            points.flatMap { decodePoly(it.obj("polyline")?.string("points")!!) }
                        // Add  points to polyline and bounds
                        options.add(origin)
                        LatLongB.include(origin)
                        for (point in polypts) {
                            options.add(point)
                            LatLongB.include(point)
                        }
                        options.add(destination)
                        LatLongB.include(destination)
                        // build bounds
                        val bounds = LatLongB.build()
                        // add polyline to the map
                        googleMap!!.addPolyline(options)
                        // show map with route centered
                        googleMap!!.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            var originsLatLng=latLngList[0].latitude.toString() +" "+ latLngList[0].longitude.toString()
            var destinationsLatLng=latLngList[latLngList.size - 1].latitude.toString() +" "+ latLngList[latLngList.size - 1].longitude.toString()
            getTATFromAddress(mContext!!, originsLatLng, destinationsLatLng)

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /**
     * This function is used to update the location of the Cab while moving from Origin to Destination
     */
    private fun updateCarLocation(latLng: LatLng) {
        try {
            if (movingCabMarker == null) {
                movingCabMarker = addCarMarkerAndGet(latLng)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (previousLatLng == null) {
            currentLatLng = latLng
            previousLatLng = currentLatLng
            movingCabMarker?.position = currentLatLng
            movingCabMarker?.setAnchor(0.5f, 0.5f)
            animateCamera(currentLatLng!!)
        } else {
            previousLatLng = currentLatLng
            currentLatLng = latLng
            val valueAnimator = AnimationUtils.carAnimator()
            valueAnimator.addUpdateListener { va ->
                if (currentLatLng != null && previousLatLng != null) {
                    val multiplier = va.animatedFraction
                    val nextLocation = LatLng(
                        multiplier * currentLatLng!!.latitude + (1 - multiplier) * previousLatLng!!.latitude,
                        multiplier * currentLatLng!!.longitude + (1 - multiplier) * previousLatLng!!.longitude
                    )
                    movingCabMarker?.position = nextLocation
                    val rotation = MapUtils.getRotation(previousLatLng!!, nextLocation)
                    if (!rotation.isNaN()) {
                        movingCabMarker?.rotation = rotation
                    }
                    movingCabMarker?.setAnchor(0.5f, 0.5f)
                    //animateCamera(nextLocation)
                    originLatLng=nextLocation.latitude.toString() +" "+nextLocation.longitude.toString()
                    destinationLatLng= latLngList!![latLngList!!.size - 1].latitude.toString() +" "+ latLngList!![latLngList!!.size - 1].longitude.toString()
                }
            }
            valueAnimator.start()
        }

        try {
            if(originLatLng!=null && destinationLatLng!=null) {
                getETAFromAddress(mContext!!, originLatLng!!, destinationLatLng!!)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    override fun onResume() {
        super.onResume()
        mMapView!!.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        mMapView!!.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMapView!!.onLowMemory()
    }

    override fun onPause() {
        super.onPause()
        mMapView!!.onPause()
    }

    fun getTATFromAddress(context: Context, originLatLng: String, destinationsLatLng: String){
        GlobalScope.launch {
            Dispatchers.IO
            val queue = Volley.newRequestQueue(context)
            val url: String =
                "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + originLatLng + "&destinations=" + destinationsLatLng + "&key=AIzaSyCv019sXxlNUIrQd6InZFPaPNX31jhfGzw"
            val stringReq = StringRequest(
                Request.Method.GET, url,
                Response.Listener<String> { response ->
                    try {
                        var strResp = response.toString()
                        val json_main: JSONObject = JSONObject(strResp)
                        var jsonarray_rows: JSONArray = json_main.optJSONArray("rows")
                        var jsonarray_origin: JSONArray = json_main.optJSONArray("origin_addresses")
                        val json_element: JSONObject =
                            jsonarray_rows.optJSONObject(0).optJSONArray("elements").optJSONObject(0)
                        var json_duration: JSONObject = json_element.optJSONObject("duration")
                        var time = json_duration.optString("text")
                        txtTatTime?.text = time
                        txtEtaTime?.text = time
                        var placeName = jsonarray_origin.get(0).toString()
                        txtPlaceName?.text = placeName
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                },
                Response.ErrorListener { Log.d("API", "that didn't work") })
            queue.add(stringReq)
        }
    }

    fun getETAFromAddress(context: Context, originLatLng: String, destinationsLatLng: String){
        GlobalScope.launch {
            Dispatchers.IO
            coroutineScope {
                async {
                    val queue = Volley.newRequestQueue(context)
                    val url: String =
                        "https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + originLatLng + "&destinations=" + destinationsLatLng + "&key=AIzaSyCv019sXxlNUIrQd6InZFPaPNX31jhfGzw"
                    val stringReq = StringRequest(
                        Request.Method.GET, url,
                        Response.Listener<String> { response ->
                            try {
                                var strResp = response.toString()
                                val json_main: JSONObject = JSONObject(strResp)
                                var jsonarray_rows: JSONArray = json_main.optJSONArray("rows")
                                var jsonarray_origin: JSONArray =
                                    json_main.optJSONArray("origin_addresses")
                                val json_element: JSONObject =
                                    jsonarray_rows.optJSONObject(0).optJSONArray("elements")
                                        .optJSONObject(0)
                                var json_duration: JSONObject =
                                    json_element.optJSONObject("duration")
                                var time = json_duration.optString("text")
                                txtEtaTime?.text = time
                                var placeName = jsonarray_origin.get(0).toString()
                                txtPlaceName?.text = placeName
                                Log.e("TIME000", time)
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        },
                        Response.ErrorListener { Log.d("API", "that didn't work") })
                    queue.add(stringReq)
                    //LabSycHttp.get(mContext, url, ETAResponseHandler())
                }
            }
        }
    }

    private fun getURL(from: LatLng, to: LatLng) : String {
        val origin = "origin=" + from.latitude + "," + from.longitude
        val dest = "destination=" + to.latitude + "," + to.longitude
        val sensor = "sensor=false"
        val params = "$origin&$dest&$sensor"
        return "https://maps.googleapis.com/maps/api/directions/json?$params"+"&key=AIzaSyCv019sXxlNUIrQd6InZFPaPNX31jhfGzw"
    }

    private fun decodePoly(encoded: String): List<LatLng> {
        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0

        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat

            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng

            val p = LatLng(
                lat.toDouble() / 1E5,
                lng.toDouble() / 1E5
            )
            poly.add(p)
        }

        return poly
    }
}