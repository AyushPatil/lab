package com.labware.lab.activities

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.location.Geocoder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.children
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionDeniedResponse
import com.karumi.dexter.listener.PermissionGrantedResponse
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.karumi.dexter.listener.single.PermissionListener
import com.labware.lab.R
import com.labware.lab.utils.AppAndroidUtilities
import com.labware.lab.utils.ValidationsAll
import com.labware.lab.views.EditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.SetOptions
import com.labware.lab.constants.AppConstantUtils
import com.labware.lab.models.Address
import com.labware.lab.models.Lab
import com.labware.lab.models.MyPreferences
import com.labware.lab.utils.ProgressDialogManager
import com.labware.lab.views.TextView
import com.squats.fittr.utils.LabResourceHelper
import com.vanillaplacepicker.presentation.builder.VanillaPlacePicker
import com.vanillaplacepicker.utils.KeyUtils
import com.vanillaplacepicker.utils.MapType
import com.vanillaplacepicker.utils.PickerLanguage
import com.vanillaplacepicker.utils.PickerType
import java.util.*

@Suppress("DEPRECATION")
class ActivityUserProfile : AppCompatActivity() {
    var toolbar: Toolbar? = null
    var edt_name: EditText? = null
    var edt_phone_number: EditText? = null
    var txt_address: TextView? = null
    var txt_certifications: com.labware.lab.views.TextView? = null
    var mobileNumber: String? = null
    var chip_group_certifications: ChipGroup? = null
    val selectedeCertifications = mutableListOf<String>()
    private var mAuth: FirebaseAuth?=null
    private var mFirebaseDatabaseInstances:FirebaseFirestore?=null
    var lattitude:String?=null
    var longitude:String?=null
    var geocoder: Geocoder? = null
    var addresses: List<android.location.Address>? = null
    private var isoChip:Chip?=null
    private var nablChip:Chip?=null
    private var myPreferences: MyPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_profile)

        edt_name = findViewById<EditText>(R.id.edt_name)
        edt_phone_number = findViewById<EditText>(R.id.edt_phone_number)
        txt_address = findViewById<TextView>(R.id.txt_address)
        txt_certifications = findViewById<com.labware.lab.views.TextView>(R.id.txt_certifications)
        chip_group_certifications = findViewById<ChipGroup>(R.id.chip_group_certifications)
        isoChip = findViewById<Chip>(R.id.iso)
        nablChip = findViewById<Chip>(R.id.nabl)
        myPreferences = MyPreferences.getPreferences(this@ActivityUserProfile)
        mAuth= FirebaseAuth.getInstance()
        mFirebaseDatabaseInstances= FirebaseFirestore.getInstance()

        val settings = FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true)
            .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
            .build()
        mFirebaseDatabaseInstances?.setFirestoreSettings(settings)

        if (intent != null) {
            mobileNumber = intent.getStringExtra("MOBILE").toString()
            edt_phone_number?.setText(mobileNumber)
            edt_phone_number?.isEnabled=false
            edt_phone_number?.isClickable=false
        }
        val user = FirebaseAuth.getInstance().currentUser
        getDataOneTime(user?.phoneNumber!!)
        findViewById<Button>(R.id.btn_save).setOnClickListener(View.OnClickListener {

            if (validate()) {
                val labUser =Lab()
                val address=Address()
                address.lattitude=lattitude
                address.longitude=longitude
                labUser.name=edt_name?.text.toString()
                labUser.number=edt_phone_number?.text.toString()
                labUser.address = address
                labUser.certifications = selectedeCertifications
                mFirebaseDatabaseInstances?.collection(AppConstantUtils.LAB)?.document(edt_phone_number?.text.toString())!!.set(labUser, SetOptions.merge())
                    .addOnSuccessListener { documentReference ->
                        myPreferences?.setRegistrationStatus(true)
                        val intent = Intent(this@ActivityUserProfile, MainActivity::class.java)
                        startActivity(intent)
                        finishAffinity()
                        AppAndroidUtilities.startFwdAnimation(this@ActivityUserProfile)
                    }
                    .addOnFailureListener { e ->
                        myPreferences?.setRegistrationStatus(false)
                       AppAndroidUtilities.showBanner(this@ActivityUserProfile,LabResourceHelper.getString(R.string.something_went_worng),4)
                    }

            }

        })

        toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar!!.setNavigationIcon(R.drawable.ic_arrow_back_white)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar!!.title = "User profile"
        toolbar!!.setNavigationOnClickListener(View.OnClickListener {
            finish()
            AppAndroidUtilities.startTopToBottomAnimation(this@ActivityUserProfile)
        })


        // Loop through the chips
        for (index in 0 until 2) {
            val chip:Chip = chip_group_certifications?.getChildAt(index) as Chip
            // Set the chip checked change listener
            chip.setOnCheckedChangeListener{view, isChecked ->
                if (isChecked){
                    selectedeCertifications.add(view.text.toString())
                }else{
                    selectedeCertifications.remove(view.text.toString())
                }

                if (selectedeCertifications.isNotEmpty()){
                    // SHow the selection
                    //toast("Selected $selectedeCertifications")
                }
                Log.e("SIZEEEE",selectedeCertifications.size.toString())
            }
        }

        txt_address?.setOnClickListener(View.OnClickListener {

            val intent = VanillaPlacePicker.Builder(this)
                .with(PickerType.MAP_WITH_AUTO_COMPLETE)
                .setMapType(MapType.NORMAL)
                .setMapStyle(R.raw.uber_style)
                .setCountry("IN")
                .setPickerLanguage(PickerLanguage.ENGLISH)
                .enableShowMapAfterSearchResult(true)
                .build()
            startActivityForResult(intent, KeyUtils.REQUEST_PLACE_PICKER)
            AppAndroidUtilities.startFwdAnimation(this@ActivityUserProfile)
        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && data != null) {
            when (requestCode) {
                KeyUtils.REQUEST_PLACE_PICKER -> {
                    val vanillaAddress = VanillaPlacePicker.onActivityResult(data)
                    lattitude = vanillaAddress!!.latitude!!.toString()
                    longitude= vanillaAddress!!.longitude!!.toString()
                   /*geocoder = Geocoder( this@ActivityUserProfile, Locale.getDefault())
                    addresses = geocoder!!.getFromLocation(
                        lattitude?.toDouble()!!,
                        longitude?.toDouble()!!,1)
                    val address = addresses?.get(0)?.getAddressLine(0)*/
                    txt_address?.setText(vanillaAddress.formattedAddress)
                    }
                }
            }
        }


    fun Context.toast(message:String)=
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show()

    private fun showSettingsDialog() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this@ActivityUserProfile)
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.")
        builder.setPositiveButton("GOTO SETTINGS",
            DialogInterface.OnClickListener { dialog, which ->
                dialog.cancel()
                openSettings()
            })
        builder.setNegativeButton("Cancel",
            DialogInterface.OnClickListener { dialog, which -> dialog.cancel() })
        builder.show()
    }

    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts("package", getPackageName(), null)
        intent.data = uri
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                AppAndroidUtilities.startBackAnimation(this@ActivityUserProfile)
                return true
            }
        }
        return super.onContextItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        AppAndroidUtilities.startTopToBottomAnimation(this@ActivityUserProfile)
    }

    private fun validate(): Boolean {
        if (edt_name?.text.toString().trim().isNullOrEmpty()) {
            AppAndroidUtilities.showBanner(this@ActivityUserProfile, "Please enter valid name", 4)
            return false
        } else if (edt_phone_number?.text.toString().trim()
                .isNullOrEmpty() || !ValidationsAll.checkPhone(
                edt_phone_number?.text.toString().trim()
            )
        ) {
            AppAndroidUtilities.showBanner(
                this@ActivityUserProfile,
                "Please enter valid phone number",
                4
            )
            return false
        } else if (txt_address?.text.toString().trim().isNullOrEmpty() || txt_address?.text.toString().trim().equals("Enter your address")) {
            AppAndroidUtilities.showBanner(
                this@ActivityUserProfile,
                "Please enter valid address",
                4
            )
            return false
        } else if (selectedeCertifications.isNullOrEmpty() || selectedeCertifications.size==0)
        {
            AppAndroidUtilities.showBanner(
                this@ActivityUserProfile,
                "Please select valid certifications",
                4
            )
            return false
        }
        return true;
    }

    private fun getDataOneTime(phoneNumber: String) {
        ProgressDialogManager.showProgressDialog(this@ActivityUserProfile)
        val docRef =
            mFirebaseDatabaseInstances?.collection(AppConstantUtils.LAB)?.document(
                phoneNumber.replace(
                    "+91",
                    ""
                )
            )
        docRef?.get()?.addOnSuccessListener { documentSnapshot ->
            try {
                ProgressDialogManager.dismissProgressDialog(this@ActivityUserProfile)
                val user = documentSnapshot.toObject(Lab::class.java)
                if(!user?.name.isNullOrEmpty()) {
                    edt_name?.setText(user?.name)
                }
                if(!user?.number.isNullOrEmpty()) {
                    edt_phone_number?.setText(user?.number)
                }
                if(!user?.address?.lattitude.isNullOrEmpty() && !user?.address?.longitude.isNullOrEmpty()) {
                    val geocoder: Geocoder
                    val addresses: List<android.location.Address>
                    geocoder = Geocoder(this, Locale.getDefault())
                    addresses = geocoder.getFromLocation(
                        user?.address?.lattitude?.toDouble()!!,
                        user?.address?.longitude?.toDouble()!!,
                        1
                    )

                    lattitude = user?.address?.lattitude?.toString()!!
                    longitude = user?.address?.longitude?.toString()!!

                    val address: String = addresses[0].getAddressLine(0)
                    txt_address?.setText(address)
                }
                if(!user?.certifications.isNullOrEmpty())
                {
                    for (item in user?.certifications!!) {
                        // body of loop
                        if (item.equals("ISO")) {
                            isoChip?.isChecked = true
                        } else if (item.equals("NABL")) {
                            nablChip?.isChecked = true
                        }
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
        docRef?.get()?.addOnFailureListener(OnFailureListener {
            ProgressDialogManager.dismissProgressDialog(this@ActivityUserProfile)
        })
    }
}
