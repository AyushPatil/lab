package com.labware.lab.activities

import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.*
import com.google.android.material.tabs.TabLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.labware.lab.R
import com.labware.lab.constants.AppConstantUtils
import com.labware.lab.fragments.AuditTrailFragment
import com.labware.lab.fragments.TestsFragment
import com.labware.lab.models.MedicalTestShowEvent
import com.labware.lab.models.MedicalTests
import com.labware.lab.utils.*
import com.labware.lab.views.TextView
import com.labware.lab.views.ViewPagerAdapter
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.custom.async
import org.jetbrains.anko.uiThread
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList

class ActivityViewHistory : AppCompatActivity() {

    private var toolbar: Toolbar? = null
    private var viewPagerAdapter: ViewPagerAdapter? = null
    private var viewPager: ViewPager? = null
    private var tabLayout: TabLayout? = null
    var mMapView: MapView? = null
    private var googleMap: GoogleMap? = null
    var rawArray = intArrayOf(R.raw.uber_style)
    private lateinit var defaultLocation: LatLng
    private var originMarker: Marker? = null
    private var destinationMarker: Marker? = null
    private var grayPolyline: Polyline? = null
    private var blackPolyline: Polyline? = null
    private var movingCabMarker: Marker? = null
    private var previousLatLng: LatLng? = null
    private var currentLatLng: LatLng? = null
    private lateinit var handler: Handler
    private lateinit var runnable: Runnable
    private var REQUEST_ID: String?=null
    private var mAuth: FirebaseAuth?=null
    private var mFirebaseDatabaseInstances: FirebaseFirestore?=null
    var geocoder: Geocoder? = null
    var originAddresses: List<Address>? = null
    var destinationAddresses: List<Address>? = null
    private var txt_patient_name: TextView?=null
    private var txt_request_status: TextView?=null
    private var txt_request_date: TextView?=null
    private var txt_request_id: TextView?=null
    private var txt_orogin: TextView?=null
    private var txt_destination: TextView?=null
    var medicalTestsListToShow: ArrayList<MedicalTests>? = null
    var medicalTests:MedicalTests?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_history)

        mAuth=FirebaseAuth.getInstance()
        mFirebaseDatabaseInstances= FirebaseFirestore.getInstance()
        val settings = FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true)
            .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
            .build()
        mFirebaseDatabaseInstances?.setFirestoreSettings(settings)

        medicalTestsListToShow= ArrayList()
        medicalTestsListToShow?.clear()

        toolbar = findViewById(R.id.toolbar) as Toolbar

        txt_patient_name = findViewById(R.id.txt_patient_name) as TextView
        txt_request_status = findViewById(R.id.txt_request_status) as TextView
        txt_request_date = findViewById(R.id.txt_request_date) as TextView
        txt_request_id = findViewById(R.id.txt_request_id) as TextView
        txt_orogin = findViewById(R.id.txt_orogin) as TextView
        txt_destination = findViewById(R.id.txt_destination) as TextView

        setSupportActionBar(toolbar)
        toolbar!!.setNavigationIcon(R.drawable.ic_arrow_back_white)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar!!.title="View history"
        toolbar!!.setNavigationOnClickListener(View.OnClickListener {
            finish()
            AppAndroidUtilities.startTopToBottomAnimation(this@ActivityViewHistory)
        })
        ProgressDialogManager.showProgressDialog(this@ActivityViewHistory)
        Handler().postDelayed({
            ProgressDialogManager.dismissProgressDialog(this@ActivityViewHistory)
            findViewById<ProgressBar>(R.id.progressBar).visibility = View.INVISIBLE
        }, 2000)

        geocoder = Geocoder(this@ActivityViewHistory, Locale.getDefault())

        if(intent!=null)
        {
            REQUEST_ID=intent.getStringExtra("REQUEST_ID")
        }
        viewPager = findViewById(R.id.viewpager)
        // setting up the adapter
        viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)
        // add the fragments
        viewPagerAdapter!!.add(TestsFragment(), "Tests")
        viewPagerAdapter!!.add(AuditTrailFragment(), "Audit trail")
        // Set the adapter
        viewPager!!.setAdapter(viewPagerAdapter)
        // The Page (fragment) titles will be displayed in the
        // tabLayout hence we need to set the page viewer
        // we use the setupWithViewPager().
        tabLayout = findViewById(R.id.tab_layout)
        tabLayout!!.setupWithViewPager(viewPager)
        mMapView = findViewById(R.id.mapView)
        mMapView!!.onCreate(savedInstanceState)
        mMapView!!.onResume() // needed to get the map to display immediately
        try {
            MapsInitializer.initialize(this)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        mMapView!!.getMapAsync { mMap ->
            googleMap = mMap
            try {
                googleMap!!.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                        this, rawArray[0]
                    )
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }

            val docRef =
                mFirebaseDatabaseInstances?.collection(AppConstantUtils.REQUESTS)?.document(
                    REQUEST_ID!!
                )
            docRef?.get()?.addOnSuccessListener { documentSnapshot ->
                val obj = documentSnapshot.get("Audit trail") as HashMap<*, *>
                var currentStatus = obj.get("Current status").toString()!!
                var requestId = obj.get("Request Id").toString()!!
                var timestamp = obj.get("Timestamp").toString()!!
                val obj2= obj.get("Registration") as HashMap<*, *>
                val obj3= obj.get("Proof of prescription") as HashMap<*, *>
                var patientName=obj2.get("Patient name").toString()!!
                var originLatLng = obj.get("Origin LatLng").toString()!!
                var destinationLatLng = obj.get("Destination LatLng").toString()!!
                val msgArrayOriginLatLng: Array<String> = originLatLng.split("/".toRegex()).toTypedArray()
                val msgArrayDestinationLatLng: Array<String> = destinationLatLng.split("/".toRegex()).toTypedArray()
                defaultLocation = LatLng(
                    msgArrayOriginLatLng[0].toDouble(),
                    msgArrayOriginLatLng[1].toDouble()
                )

                if(!patientName.isNullOrEmpty()) {
                    txt_patient_name?.text = patientName
                }
                else
                {
                    txt_patient_name?.text = "NA"
                }
                txt_request_status?.text=currentStatus.toUpperCase()
                txt_request_id?.text=requestId.substring(0, 7)
                txt_request_date?.text=timestamp

                originAddresses = geocoder!!.getFromLocation(
                    msgArrayOriginLatLng[0].toDouble()!!,
                    msgArrayOriginLatLng[1].toDouble()!!, 1
                )
                val originAddress = originAddresses?.get(0)?.getAddressLine(0)
                txt_orogin?.text=originAddress

                destinationAddresses = geocoder!!.getFromLocation(
                    msgArrayDestinationLatLng[0].toDouble()!!,
                    msgArrayDestinationLatLng[1].toDouble()!!, 1
                )
                val destinationAddress = destinationAddresses?.get(0)?.getAddressLine(0)
                txt_destination?.text=destinationAddress
                try {
                    val obj4= obj3.get("Tests") as ArrayList<Map<String, MedicalTests>>
                    //Log.e("OBJ4", obj4.toString())
                    for (group in obj4) {
                        medicalTests = MedicalTests()
                        medicalTests?.testName = group.get("testName").toString()
                        medicalTests?.price =group.get("price").toString()
                        medicalTests?.tubeType = group.get("tubeType").toString()
                        medicalTestsListToShow?.add(medicalTests!!)
                    }

                    if(!medicalTestsListToShow.isNullOrEmpty()) {
                        EventBus.getDefault().postSticky(
                            MedicalTestShowEvent(medicalTestsListToShow!!)
                        )
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                Handler().postDelayed(Runnable {
                    val latLngList = ArrayList<LatLng>()
                    latLngList.add(
                        LatLng(
                            msgArrayOriginLatLng[0].toDouble(),
                            msgArrayOriginLatLng[1].toDouble()
                        )
                    )
                    latLngList.add(
                        LatLng(
                            msgArrayDestinationLatLng[0].toDouble(),
                            msgArrayDestinationLatLng[1].toDouble()
                        )
                    )
                    showPath(latLngList)
                }, 1000)
            }
        }

    }

    private fun animateCamera(latLng: LatLng) {
        val cameraPosition = CameraPosition.Builder().target(latLng).zoom(12.5f).build()
        googleMap!!.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                AppAndroidUtilities.startTopToBottomAnimation(this@ActivityViewHistory)
                return true
            }
        }
        return super.onContextItemSelected(item)
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    finish()
                    AppAndroidUtilities.startTopToBottomAnimation(this@ActivityViewHistory)
                    return true
                }
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        AppAndroidUtilities.startTopToBottomAnimation(this@ActivityViewHistory)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home -> {
                finish()
                AppAndroidUtilities.startTopToBottomAnimation(this@ActivityViewHistory)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        mMapView!!.onResume()
    }

    override fun onPause() {
        super.onPause()
        mMapView!!.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mMapView!!.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMapView!!.onLowMemory()
    }

    private fun showPath(latLngList: ArrayList<LatLng>) {

        val LatLongB = LatLngBounds.Builder()
        // Add markers
        val origin = LatLng(latLngList.get(0).latitude, latLngList.get(0).longitude)
        val destination = LatLng(latLngList.get(1).latitude, latLngList.get(1).longitude)
        val originbitmapDescriptor = BitmapDescriptorFactory.fromBitmap(
            MapUtils.getOriginMarkerBitmap(
                this
            )
        )
        val destinationbitmapDescriptor = BitmapDescriptorFactory.fromBitmap(
            MapUtils.getDestinationMarkerBitmap(
                this
            )
        )
        googleMap!!.addMarker(
            MarkerOptions().position(origin).flat(true).icon(
                originbitmapDescriptor
            )
        )
        googleMap!!.addMarker(
            MarkerOptions().position(destination).flat(true).icon(
                destinationbitmapDescriptor
            )
        )

        // Declare polyline object and set up color and width
        val options = PolylineOptions()
        options.color(Color.BLACK)
        options.width(10f)

        // build URL to call API
        val url = getURL(origin, destination)

        async {
            // Connect to URL, download content and convert into string asynchronously
            val result = URL(url).readText()
            uiThread {
                // When API call is done, create parser and convert into JsonObjec
                val parser: Parser = Parser()
                val stringBuilder: StringBuilder = StringBuilder(result)
                val json: JsonObject = parser.parse(stringBuilder) as JsonObject
                // get to the correct element in JsonObject
                val routes = json.array<JsonObject>("routes")
                val points = routes!!["legs"]["steps"][0] as com.beust.klaxon.JsonArray<JsonObject>
                // For every element in the JsonArray, decode the polyline string and pass all points to a List
                val polypts = points.flatMap { decodePoly(it.obj("polyline")?.string("points")!!)  }
                // Add  points to polyline and bounds
                options.add(origin)
                LatLongB.include(origin)
                for (point in polypts)  {
                    options.add(point)
                    LatLongB.include(point)
                }
                options.add(destination)
                LatLongB.include(destination)
                // build bounds
                val bounds = LatLongB.build()
                // add polyline to the map
                googleMap!!.addPolyline(options)
                // show map with route centered
                googleMap!!.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 100))

            }
        }

    }

    private fun addOriginMarkerAndGet(latLng: LatLng): Marker {
        val bitmapDescriptor =
            BitmapDescriptorFactory.fromBitmap(MapUtils.getOriginMarkerBitmap(this))
        return googleMap!!.addMarker(
            MarkerOptions().position(latLng).flat(true).icon(bitmapDescriptor)
        )
    }

    private fun addDestinationMarkerAndGet(latLng: LatLng): Marker {
        val bitmapDescriptor =
            BitmapDescriptorFactory.fromBitmap(MapUtils.getDestinationMarkerBitmap(this))
        return googleMap!!.addMarker(
            MarkerOptions().position(latLng).flat(true).icon(bitmapDescriptor)
        )
    }

    private fun getURL(from: LatLng, to: LatLng) : String {
        val origin = "origin=" + from.latitude + "," + from.longitude
        val dest = "destination=" + to.latitude + "," + to.longitude
        val sensor = "sensor=false"
        val params = "$origin&$dest&$sensor"
        return "https://maps.googleapis.com/maps/api/directions/json?$params"+"&key=AIzaSyCv019sXxlNUIrQd6InZFPaPNX31jhfGzw"
    }

    private fun decodePoly(encoded: String): List<LatLng> {
        val poly = ArrayList<LatLng>()
        var index = 0
        val len = encoded.length
        var lat = 0
        var lng = 0

        while (index < len) {
            var b: Int
            var shift = 0
            var result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlat = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lat += dlat

            shift = 0
            result = 0
            do {
                b = encoded[index++].toInt() - 63
                result = result or (b and 0x1f shl shift)
                shift += 5
            } while (b >= 0x20)
            val dlng = if (result and 1 != 0) (result shr 1).inv() else result shr 1
            lng += dlng

            val p = LatLng(
                lat.toDouble() / 1E5,
                lng.toDouble() / 1E5
            )
            poly.add(p)
        }

        return poly
    }
}
