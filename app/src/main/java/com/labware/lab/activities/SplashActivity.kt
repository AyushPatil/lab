package com.labware.lab.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.labware.lab.R
import com.labware.lab.models.MyPreferences
import com.labware.lab.utils.AppAndroidUtilities

@Suppress("DEPRECATION")
class SplashActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    private var myPreferences: MyPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        FirebaseApp.initializeApp(this)
        auth = Firebase.auth

        myPreferences = MyPreferences.getPreferences(this@SplashActivity)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
    override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        updateUI(currentUser)

    }

    private fun updateUI(user: FirebaseUser? = auth.currentUser) {

        try {
            if(myPreferences?.getRegistrationStatus()==true) {
                if (!user?.uid?.isEmpty()!!) {
                    Handler().postDelayed({
                        val intent = Intent(this, MainActivity::class.java)
                        startActivity(intent)
                        AppAndroidUtilities.startFwdAnimation(this@SplashActivity)
                        finishAffinity()
                    }, 1000)
                }
            }
            else
            {
                val intent = Intent(this, GetStartedActivity::class.java)
                startActivity(intent)
                AppAndroidUtilities.startFwdAnimation(this@SplashActivity)
                finishAffinity()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Handler().postDelayed({
                val intent = Intent(this, GetStartedActivity::class.java)
                startActivity(intent)
                AppAndroidUtilities.startFwdAnimation(this@SplashActivity)
                finish()
            }, 1000) // 3000 is the delayed time in milliseconds.
        }
    }
}
