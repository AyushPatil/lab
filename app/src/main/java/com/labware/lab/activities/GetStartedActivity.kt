package com.labware.lab.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.labware.lab.R
import com.labware.lab.utils.AppAndroidUtilities

@Suppress("DEPRECATION")
class GetStartedActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_started)

        // This is used to hide the status bar and make
        // the splash screen as a full screen activity.
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        // we used the postDelayed(Runnable, time) method
        // to send a message with a delayed time.
        findViewById<Button>(R.id.btn_get_started).setOnClickListener(View.OnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            AppAndroidUtilities.startFwdAnimation(this@GetStartedActivity)
            finish()
        })
        findViewById<ImageView>(R.id.ic_next).setOnClickListener(View.OnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            AppAndroidUtilities.startFwdAnimation(this@GetStartedActivity)
            finish()
        })
    }
}
