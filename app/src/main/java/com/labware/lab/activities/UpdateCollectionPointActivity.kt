package com.labware.lab.activities

import `in`.shadowfax.proswipebutton.ProSwipeButton
import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.util.Log
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder
import cafe.adriel.androidaudiorecorder.model.AudioChannel
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate
import cafe.adriel.androidaudiorecorder.model.AudioSource
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMapClickListener
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.SetOptions
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.labware.lab.R
import com.labware.lab.constants.AppConstantUtils
import com.labware.lab.models.Hospital
import com.labware.lab.models.MyPreferences
import com.labware.lab.utils.AppAndroidUtilities
import com.labware.lab.utils.ProgressDialogManager
import com.labware.lab.utils.ValidationsAll
import com.labware.lab.views.EditText
import com.labware.lab.views.TextView
import com.vanillaplacepicker.presentation.builder.VanillaPlacePicker
import com.vanillaplacepicker.utils.KeyUtils
import com.vanillaplacepicker.utils.MapType
import com.vanillaplacepicker.utils.PickerLanguage
import com.vanillaplacepicker.utils.PickerType
import kotlinx.android.synthetic.main.activity_add_collection_point.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.util.*


class UpdateCollectionPointActivity : AppCompatActivity() {
    var mMapView: MapView? = null
    private var googleMap: GoogleMap? = null
    var rawArray =
        intArrayOf(R.raw.uber_style)
    var toolbar: Toolbar? = null
    private var mCurrLocationMarker: Marker? = null

    private var edt_contact_name: EditText? = null
    private var edt_contact_number: EditText? = null
    private var edt_flat_number: EditText? = null
    private var edt_society_name: EditText? = null
    private var edt_locality: EditText? = null
    private var txt_voice_instructions: TextView? = null
    private var img_play_recorded_audio: ImageView? = null
    val selectedeCollectionTypes = mutableListOf<String>()
    var lattitude: String? = null
    var longitude: String? = null
    var proSwipeBtn: ProSwipeButton? = null
    private val REQUEST_RECORD_AUDIO = 100
    private val AUDIO_FILE_PATH:String? ="storage/emulated/0/Download/recorded_audio.wav"
    private var mAuth: FirebaseAuth? = null
    private var mFirebaseDatabaseInstances: FirebaseFirestore? = null
    private var collectionType: String? = null
    private var collectionPhone: String? = null
    private var user: Hospital? = null
    private var myPreferences: MyPreferences? = null
    var geocoder: Geocoder? = null
    var addresses: List<Address>? = null
    var chip_group_collection_type: ChipGroup? = null
    var chip_hospital: Chip? = null
    var chip_clinic: Chip? = null
    var chip_home: Chip? = null
    private var firestoreDB: FirebaseFirestore? = null
    var storageRef: StorageReference? = null
    var riversRef: StorageReference? = null
    var uploadTask: UploadTask? = null
    var firebaseStorage: FirebaseStorage? = null
    var file: Uri? = null
    var recording = ""
    var mediaPlayer: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_collection_point)
        toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar!!.setNavigationIcon(R.drawable.ic_arrow_back_white)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar!!.title = "Update collection point"
        toolbar!!.setNavigationOnClickListener(View.OnClickListener {
            finish()
            AppAndroidUtilities.startTopToBottomAnimation(this@UpdateCollectionPointActivity)
        })
        myPreferences = MyPreferences.getPreferences(this@UpdateCollectionPointActivity)
        firestoreDB = FirebaseFirestore.getInstance();
        Handler().postDelayed({
            findViewById<ProgressBar>(R.id.progressBar).visibility = View.INVISIBLE
        }, 3000)

        mMapView = findViewById(R.id.mapView)

        edt_contact_name = findViewById(R.id.edt_contact_name)
        edt_contact_number = findViewById(R.id.edt_contact_number)
        edt_flat_number = findViewById(R.id.edt_flat_number)
        edt_society_name = findViewById(R.id.edt_society_name)
        edt_locality = findViewById(R.id.edt_locality)
        txt_voice_instructions = findViewById(R.id.txt_voice_instructions)
        img_play_recorded_audio = findViewById(R.id.img_play_recorded_audio)

        chip_group_collection_type = findViewById<ChipGroup>(R.id.chip_group_collection_type)
        chip_hospital = findViewById<Chip>(R.id.chip_hospital)
        chip_clinic = findViewById<Chip>(R.id.chip_clinic)
        chip_home = findViewById<Chip>(R.id.chip_home)

        mAuth = FirebaseAuth.getInstance()
        mFirebaseDatabaseInstances = FirebaseFirestore.getInstance()

        val settings = FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true)
            .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
            .build()
        mFirebaseDatabaseInstances?.setFirestoreSettings(settings)

        if (intent != null) {
            collectionType = intent.getStringExtra(AppConstantUtils.TYPE)
            collectionPhone = intent.getStringExtra(AppConstantUtils.PHONE)
            getDataOneTime(collectionPhone!!, collectionType!!)
        }

        txt_voice_instructions?.setOnClickListener(View.OnClickListener {

            Dexter.withContext(this@UpdateCollectionPointActivity)
                .withPermissions(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO
                )
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            // do you work now
                            val color = resources.getColor(R.color.colorPrimaryDark)
                            AndroidAudioRecorder.with(this@UpdateCollectionPointActivity) // Required
                                .setFilePath(AUDIO_FILE_PATH)
                                .setColor(color)
                                .setRequestCode(REQUEST_RECORD_AUDIO) // Optional
                                .setSource(AudioSource.MIC)
                                .setChannel(AudioChannel.STEREO)
                                .setSampleRate(AudioSampleRate.HZ_48000)
                                .setAutoStart(true)
                                .setKeepDisplayOn(true) // Start recording
                                .record()
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied) {
                            // permission is denied permenantly, navigate user to app settings
                            showSettingsDialog()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(
                        permissions: List<PermissionRequest?>?,
                        token: PermissionToken
                    ) {
                        token.continuePermissionRequest()
                    }
                })
                .onSameThread()
                .check()

        })

        mMapView!!.onCreate(savedInstanceState)
        mMapView!!.onResume() // needed to get the map to display immediately
        try {
            MapsInitializer.initialize(this)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        mMapView!!.getMapAsync { mMap ->
            googleMap = mMap
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    googleMap!!.isMyLocationEnabled = true
                }
            } else {
                googleMap!!.isMyLocationEnabled = true
            }

            googleMap?.setOnMyLocationChangeListener { arg0 ->

                val geocoder: Geocoder
                val addresses: List<Address>
                geocoder = Geocoder(this, Locale.getDefault())
                addresses = geocoder.getFromLocation(
                    arg0.latitude,
                    arg0.longitude,
                    1
                )
                val address: String =
                    addresses[0].getAddressLine(0)
            }
            try {
                googleMap!!.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                        this, rawArray[0]
                    )
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }

            googleMap!!.setOnMapClickListener(OnMapClickListener {
                //Log.d(TAG, "onMapClick")
                val intent = VanillaPlacePicker.Builder(this)
                    .with(PickerType.MAP_WITH_AUTO_COMPLETE)
                    .setMapType(MapType.NORMAL)
                    .setMapStyle(R.raw.uber_style)
                    .setCountry("IN")
                    .setPickerLanguage(PickerLanguage.ENGLISH)
                    .enableShowMapAfterSearchResult(true)
                    .build()
                startActivityForResult(intent, KeyUtils.REQUEST_PLACE_PICKER)
                AppAndroidUtilities.startFwdAnimation(this@UpdateCollectionPointActivity)

                /*val intentt = Intent(this@AddCollectionPointActivity, MapsActivity::class.java)
                startActivity(intentt)
                AppAndroidUtilities.startFwdAnimation(this@AddCollectionPointActivity)*/

            })
        }

        proSwipeBtn = findViewById<ProSwipeButton>(R.id.btn_swipe_to_save)
        proSwipeBtn?.setOnSwipeListener(object : ProSwipeButton.OnSwipeListener {
            override fun onSwipeConfirm() {
                if (validate()) {
                    val hospital = Hospital()
                    val address = com.labware.lab.models.Address()
                    address.lattitude = lattitude
                    address.longitude = longitude
                    hospital.type = selectedeCollectionTypes.get(0).toString()
                    hospital.name = edt_contact_name?.text.toString()
                    hospital.phone = edt_contact_number?.text.toString()
                    hospital.flatNumber = edt_flat_number?.text.toString()
                    hospital.societyName = edt_society_name?.text.toString()
                    hospital.locality = edt_locality?.text.toString()
                    hospital.voiceInstruction = recording
                    hospital.address = address
                    hospital.labId = mAuth?.currentUser?.phoneNumber.toString().replace("+91", "")
                    if (selectedeCollectionTypes.get(0).toString()
                            .equals(AppConstantUtils.HOSPITAL)
                    ) {
                        mFirebaseDatabaseInstances?.collection(AppConstantUtils.HOSPITAL)
                            ?.document(edt_contact_number?.text.toString()+"_"+ mAuth?.currentUser?.phoneNumber.toString().replace("+91",""))!!.set(hospital, SetOptions.merge())
                            .addOnSuccessListener { documentReference ->
                                proSwipeBtn?.showResultIcon(true)
                                finish()
                                AppAndroidUtilities.startTopToBottomAnimation(this@UpdateCollectionPointActivity)
                            }
                            .addOnFailureListener { e ->
                                proSwipeBtn?.showResultIcon(false)
                            }
                    } else if (selectedeCollectionTypes.get(0).toString()
                            .equals(AppConstantUtils.CLINIC)
                    ) {
                        mFirebaseDatabaseInstances?.collection(AppConstantUtils.CLINIC)
                            ?.document(edt_contact_number?.text.toString() + "_"+ mAuth?.currentUser?.phoneNumber.toString().replace("+91",""))!!.set(hospital, SetOptions.merge())
                            .addOnSuccessListener { documentReference ->
                                proSwipeBtn?.showResultIcon(true)
                                finish()
                                AppAndroidUtilities.startTopToBottomAnimation(this@UpdateCollectionPointActivity)
                            }
                            .addOnFailureListener { e ->
                                proSwipeBtn?.showResultIcon(false)
                            }
                    } else if (selectedeCollectionTypes.get(0).toString()
                            .equals(AppConstantUtils.HOME)
                    ) {
                        mFirebaseDatabaseInstances?.collection(AppConstantUtils.HOME)
                            ?.document(edt_contact_number?.text.toString()+"_"+ mAuth?.currentUser?.phoneNumber.toString().replace("+91",""))!!.set(hospital, SetOptions.merge())
                            .addOnSuccessListener { documentReference ->
                                proSwipeBtn?.showResultIcon(true)
                                finish()
                                AppAndroidUtilities.startTopToBottomAnimation(this@UpdateCollectionPointActivity)
                            }
                            .addOnFailureListener { e ->
                                proSwipeBtn?.showResultIcon(false)
                            }
                    }
                } else {
                    proSwipeBtn?.showResultIcon(false)
                }
            }
        })

        chip_hospital?.isClickable=false
        chip_clinic?.isClickable=false
        chip_home?.isClickable=false


        // Loop through the chips
        for (index in 0 until 3) {
            val chip: Chip = chip_group_collection_type?.getChildAt(index) as Chip
            // Set the chip checked change listener
            chip.setOnCheckedChangeListener { view, isChecked ->
                if (isChecked) {
                    selectedeCollectionTypes.add(view.text.toString())
                } else {
                    selectedeCollectionTypes.remove(view.text.toString())
                }

                if (selectedeCollectionTypes.isNotEmpty()) {
                    // SHow the selection
                    //toast("Selected $selectedeCertifications")
                }
                Log.e("SIZEEEE", selectedeCollectionTypes.size.toString())
            }
        }

        img_play_recorded_audio?.setOnClickListener(View.OnClickListener {
            if (!txt_voice_instructions?.text.toString().equals("Add voice instructions")) {
                playAudio(txt_voice_instructions?.text.toString())
            }
        })

    }

    //----- override onActivityResult function to get Vanilla Place Picker result.
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_RECORD_AUDIO) {
            if (resultCode == RESULT_OK) {
                //Toast.makeText(this, "Audio recorded successfully!", Toast.LENGTH_SHORT).show()
                ProgressDialogManager.showProgressDialog(this@UpdateCollectionPointActivity)
                img_play_recorded_audio?.visibility=View.VISIBLE
                txt_voice_instructions?.setText(AUDIO_FILE_PATH)
                uploadFile(edt_contact_number?.text.toString())
            } else if (resultCode == RESULT_CANCELED) {
                //Toast.makeText(this, "Audio was not recorded", Toast.LENGTH_SHORT).show()
            }
        }
        if (resultCode == Activity.RESULT_OK && data != null) {
            when (requestCode) {
                KeyUtils.REQUEST_PLACE_PICKER -> {
                    val vanillaAddress = VanillaPlacePicker.onActivityResult(data)
                    mMapView!!.getMapAsync { mMap ->
                        googleMap = mMap
                        if (mCurrLocationMarker != null) {
                            mCurrLocationMarker!!.remove()
                        }
                        val sydney = LatLng(
                            vanillaAddress!!.latitude!!.toDouble(),
                            vanillaAddress!!.longitude!!.toDouble()
                        )
                        mCurrLocationMarker = googleMap!!.addMarker(
                            MarkerOptions().position(sydney).title(vanillaAddress.formattedAddress)
                                .snippet(
                                    vanillaAddress.name
                                )
                        )
                        val cameraPosition =
                            CameraPosition.Builder().target(sydney).zoom(12f).build()
                        googleMap!!.animateCamera(
                            CameraUpdateFactory.newCameraPosition(
                                cameraPosition
                            )
                        )
                        try {
                            googleMap!!.setMapStyle(
                                MapStyleOptions.loadRawResourceStyle(
                                    this, rawArray[0]
                                )
                            )
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                        lattitude = vanillaAddress.latitude.toString()
                        longitude = vanillaAddress.longitude.toString()
                    }
                }
            }
        }
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                AppAndroidUtilities.startTopToBottomAnimation(this@UpdateCollectionPointActivity)
                return true
            }
        }
        return super.onContextItemSelected(item)
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    finish()
                    AppAndroidUtilities.startTopToBottomAnimation(this@UpdateCollectionPointActivity)
                    return true
                }
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        deleteFileFromFirebaseStorage(collectionPhone!!)
        finish()
        AppAndroidUtilities.startTopToBottomAnimation(this@UpdateCollectionPointActivity)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home -> {
                finish()
                AppAndroidUtilities.startTopToBottomAnimation(this@UpdateCollectionPointActivity)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        mMapView!!.onResume()
    }

    override fun onPause() {
        super.onPause()
        mMapView!!.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mMapView!!.onDestroy()
        stopAudio()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMapView!!.onLowMemory()
    }


    private fun validate(): Boolean {
        if (selectedeCollectionTypes.isNullOrEmpty() || selectedeCollectionTypes.size == 0) {
            AppAndroidUtilities.showBanner(
                this@UpdateCollectionPointActivity,
                "Please select valid type",
                4
            )
            return false
        } else if (edt_contact_name?.text.toString().trim().isNullOrEmpty()) {
            AppAndroidUtilities.showBanner(
                this@UpdateCollectionPointActivity,
                "Please enter valid contact name",
                4
            )
            return false
        } else if (edt_contact_number?.text.toString().trim()
                .isNullOrEmpty() || !ValidationsAll.checkPhone(
                edt_contact_number?.text.toString().trim()
            )
        ) {
            AppAndroidUtilities.showBanner(
                this@UpdateCollectionPointActivity,
                "Please enter valid contact number",
                4
            )
            return false
        } else if (edt_flat_number?.text.toString().trim().isNullOrEmpty()) {
            AppAndroidUtilities.showBanner(
                this@UpdateCollectionPointActivity,
                "Please enter valid flat number",
                4
            )
            return false
        } else if (edt_society_name?.text.toString().trim().isNullOrEmpty()) {
            AppAndroidUtilities.showBanner(
                this@UpdateCollectionPointActivity,
                "Please enter valid society name",
                4
            )
            return false
        } else if (edt_locality?.text.toString().trim().isNullOrEmpty()) {
            AppAndroidUtilities.showBanner(
                this@UpdateCollectionPointActivity,
                "Please enter valid locality/area",
                4
            )
            return false
        } else if (txt_voice_instructions?.text.toString().equals("Add voice instructions")) {
            AppAndroidUtilities.showBanner(
                this@UpdateCollectionPointActivity,
                "Please add valid voice instructions",
                4
            )
            return false
        } else if (lattitude.isNullOrEmpty() && longitude.isNullOrEmpty()) {
            AppAndroidUtilities.showBanner(
                this@UpdateCollectionPointActivity,
                "Please add valid source location",
                4
            )
            return false
        }
        return true;
    }

    private fun showSettingsDialog() {
        val builder: AlertDialog.Builder = AlertDialog.Builder(this@UpdateCollectionPointActivity)
        builder.setTitle("Need Permissions")
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.")
        builder.setPositiveButton("GOTO SETTINGS",
            DialogInterface.OnClickListener { dialog, which ->
                dialog.cancel()
                openSettings()
            })
        builder.setNegativeButton("Cancel",
            DialogInterface.OnClickListener { dialog, which -> dialog.cancel() })
        builder.show()
    }

    // navigating user to app settings
    private fun openSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts(
            "package",
            getPackageName(),
            null
        )
        intent.data = uri
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    private fun getDataOneTime(phoneNumber: String, type: String) {
            ProgressDialogManager.showProgressDialog(this@UpdateCollectionPointActivity)

            var documentId = phoneNumber.replace("+91", "") + "_" + mAuth?.currentUser?.phoneNumber.toString().replace("+91","")
            Log.e("@@@documentID11111",documentId)
            val docRef = mFirebaseDatabaseInstances?.collection(type)?.document(documentId)
            docRef?.get()?.addOnSuccessListener { documentSnapshot ->
            ProgressDialogManager.dismissProgressDialog(this@UpdateCollectionPointActivity)
            try {
                user = documentSnapshot.toObject(Hospital::class.java)
                if (user?.labId.equals(myPreferences?.labID)) {
                        edt_contact_name?.setText(user?.name)
                        edt_contact_number?.setText(user?.phone)
                        edt_flat_number?.setText(user?.flatNumber)
                        edt_society_name?.setText(user?.societyName)
                        edt_locality?.setText(user?.locality)
                        txt_voice_instructions?.setText(user?.voiceInstruction)
                        img_play_recorded_audio?.visibility=View.VISIBLE
                        val sydney = LatLng(
                            user?.address!!.lattitude.toDouble(),
                            user?.address!!.longitude.toDouble()
                        )

                        geocoder = Geocoder(this@UpdateCollectionPointActivity, Locale.getDefault())

                        addresses = geocoder!!.getFromLocation(
                            user?.address?.lattitude?.toDouble()!!,
                            user?.address?.longitude?.toDouble()!!,
                            1
                        )

                        lattitude = user?.address?.lattitude?.toString()!!
                        longitude = user?.address?.longitude?.toString()!!

                        val address = addresses?.get(0)?.getAddressLine(0)
                        val city = addresses?.get(0)?.locality
                        mCurrLocationMarker = googleMap!!.addMarker(
                            MarkerOptions().position(sydney).title(address)
                                .snippet(
                                    city
                                )
                        )
                        val cameraPosition =
                            CameraPosition.Builder().target(sydney).zoom(12f).build()
                        googleMap!!.animateCamera(
                            CameraUpdateFactory.newCameraPosition(
                                cameraPosition
                            )
                        )
                        try {
                            googleMap!!.setMapStyle(
                                MapStyleOptions.loadRawResourceStyle(
                                    this, rawArray[0]
                                )
                            )
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    // body of loop
                    if (user?.type!!.equals(AppConstantUtils.HOSPITAL)) {
                        chip_hospital?.isChecked = true

                    } else if (user?.type!!.equals(AppConstantUtils.CLINIC)) {
                        chip_clinic?.isChecked = true

                    } else if (user?.type!!.equals(AppConstantUtils.HOME)) {
                        chip_home?.isChecked = true
                    }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }


    fun uploadFile(documentId: String) {
        GlobalScope.launch {
            val user=FirebaseAuth.getInstance().currentUser
            Dispatchers.IO
            try {
                file = Uri.fromFile(File(AUDIO_FILE_PATH))
                firebaseStorage = FirebaseStorage.getInstance()
                firestoreDB = FirebaseFirestore.getInstance()
                storageRef = firebaseStorage?.getReference()
                riversRef = storageRef?.child("LabAudio/" + documentId +"_"+  user?.phoneNumber?.replace("+91","")  + ".wav")
                uploadTask = riversRef?.putFile(file!!)
                uploadTask?.addOnFailureListener(OnFailureListener {
                    ProgressDialogManager.dismissProgressDialog(this@UpdateCollectionPointActivity)
                })?.addOnSuccessListener(OnSuccessListener<Any?> {
                    storageRef?.child("LabAudio/" + documentId +"_"+ user?.phoneNumber?.replace("+91","")  + ".wav")
                        ?.getDownloadUrl()?.addOnSuccessListener(
                            OnSuccessListener<Uri> { uri -> // Got the download URL for 'users/me/profile.png'
                                recording = uri.toString()
                                ProgressDialogManager.dismissProgressDialog(this@UpdateCollectionPointActivity)
                                if(File (AUDIO_FILE_PATH).exists())
                                {
                                    File(AUDIO_FILE_PATH).delete()
                                }
                            })
                })
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun playAudio(audioUrl:String) {

        GlobalScope.launch {
            Dispatchers.IO
            // initializing media player
            mediaPlayer = MediaPlayer()

            // below line is use to set the audio
            // stream type for our media player.
            mediaPlayer?.setAudioStreamType(AudioManager.STREAM_MUSIC)

            // below line is use to set our
            // url to our media player.
            try {
                mediaPlayer?.setDataSource(audioUrl)
                // below line is use to prepare
                // and start our media player.
                mediaPlayer?.prepare()
                mediaPlayer?.start()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            // below line is use to display a toast message.
            //Toast.makeText(this@UpdateCollectionPointActivity, "Audio started playing..", Toast.LENGTH_SHORT).show()
        }
        AppAndroidUtilities.showBanner(
            this@UpdateCollectionPointActivity,
            "Audio started playing...",
            1
        )
    }

    private fun stopAudio()
    {
        if (mediaPlayer!=null && mediaPlayer!!.isPlaying()) {
            // pausing the media player if media player
            // is playing we are calling below line to
            // stop our media player.
            mediaPlayer?.stop();
            mediaPlayer?.reset();
            mediaPlayer?.release();
        }
    }


    fun deleteFileFromFirebaseStorage(fileName: String) {
        GlobalScope.launch {
            val user=FirebaseAuth.getInstance().currentUser
            Dispatchers.IO
            try {
                var firebaseStorage = FirebaseStorage.getInstance()
                var storageRef = firebaseStorage?.getReference()
                var riversRef = storageRef?.child("LabAudio/" + fileName +"_"+  user?.phoneNumber?.replace("+91","")  + ".wav")
                var deleteTask = riversRef?.delete()
                deleteTask?.addOnFailureListener(OnFailureListener {
                })?.addOnSuccessListener(OnSuccessListener<Any?> {
                })
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }
}