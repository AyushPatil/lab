package com.labware.lab.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.labware.lab.R
import com.labware.lab.utils.AppAndroidUtilities
import com.labware.lab.utils.ProgressDialogManager
import com.labware.lab.utils.ValidationsAll
import com.labware.lab.views.EditText

@Suppress("DEPRECATION")
class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        findViewById<Button>(R.id.btn_login).setOnClickListener(View.OnClickListener {
            findViewById<ImageView>(R.id.ic_next).performClick()

        })
        findViewById<ImageView>(R.id.ic_next).setOnClickListener(View.OnClickListener {
            if(!findViewById<EditText>(R.id.edt_mobile_number).text.toString().isEmpty()
                && ValidationsAll.checkPhone(findViewById<EditText>(R.id.edt_mobile_number).text.toString()))
            {
                val intent = Intent(this@LoginActivity, VerifyOtpActivity::class.java)
                intent.putExtra("MOBILE",findViewById<EditText>(R.id.edt_mobile_number).text.toString())
                startActivity(intent)
                AppAndroidUtilities.startFwdAnimation(this@LoginActivity)
            }
            else
            {
                AppAndroidUtilities.showBanner(this@LoginActivity,"Please enter valid mobile number",4)
            }
        })
        findViewById<TextView>(R.id.ic_back).setOnClickListener(View.OnClickListener {
            finish()
        })
    }
}
