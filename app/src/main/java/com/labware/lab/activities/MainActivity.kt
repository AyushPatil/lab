package com.labware.lab.activities;

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.labware.lab.R
import com.labware.lab.constants.AppConstantUtils
import com.labware.lab.fragments.*
import com.labware.lab.models.Lab
import com.labware.lab.models.MessageEvent
import com.labware.lab.models.MyPreferences
import com.labware.lab.utils.AppAndroidUtilities
import com.labware.lab.utils.CircleTransform
import com.squats.fittr.utils.LabResourceHelper
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*


class MainActivity : AppCompatActivity() {

    private var navigationView: NavigationView? = null
    private var drawer: DrawerLayout? = null
    private var navHeader: View? = null
    private var imgProfile: ImageView? = null
    private var txtName: com.labware.lab.views.TextView? = null
    private var toolbar: Toolbar? = null
    private var fab: FloatingActionButton? = null
    private var mAuth: FirebaseAuth?=null
    private var mFirebaseDatabaseInstances:FirebaseFirestore?=null
    private lateinit var activityTitles: Array<String>
    private lateinit var auth: FirebaseAuth
    private val shouldLoadHomeFragOnBackPress = true
    private var mHandler: Handler? = null
    private var myPreferences: MyPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        FirebaseApp.initializeApp(this)
        auth = Firebase.auth

        mAuth=FirebaseAuth.getInstance()
        mFirebaseDatabaseInstances= FirebaseFirestore.getInstance()
        val user=FirebaseAuth.getInstance().currentUser

        val settings = FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true)
            .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
            .build()
        mFirebaseDatabaseInstances?.setFirestoreSettings(settings)
        myPreferences = MyPreferences.getPreferences(this@MainActivity)
        mHandler = Handler()
        drawer = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        fab = findViewById<View>(R.id.fab) as FloatingActionButton
        EventBus.getDefault().register(this@MainActivity)

        navHeader = navigationView!!.getHeaderView(0)
        txtName = navHeader!!.findViewById<View>(R.id.name) as com.labware.lab.views.TextView
        imgProfile = navHeader!!.findViewById<View>(R.id.img_profile) as ImageView

        activityTitles = resources.getStringArray(R.array.nav_item_activity_titles)
        fab!!.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }


        getDataOneTime(user?.phoneNumber!!)

        setUpNavigationView()
        if (savedInstanceState == null) {
            navItemIndex = 0
            CURRENT_TAG = TAG_HOME
            loadHomeFragment()
        }

        subscribe()
    }

    private fun loadNavHeader(name: String) {
        // name, website
        txtName!!.text = name
        // loading header background image
        /*Glide.with(this).load(urlNavHeaderBg)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgNavHeaderBg!!)*/

        // Loading profile image
        Glide.with(this).load(R.drawable.nav_profile)
                .thumbnail(0.5f)
                .placeholder(R.drawable.nav_profile)
                .bitmapTransform(CircleTransform(this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgProfile)
    }

    private fun loadHomeFragment() {
        selectNavMenu()
        setToolbarTitle()
        if (supportFragmentManager.findFragmentByTag(CURRENT_TAG) != null) {
            drawer!!.closeDrawers()
            AppAndroidUtilities.hideKeyboard(this@MainActivity)
            toggleFab()
            return
        }
        val mPendingRunnable = Runnable {
            val fragment: Fragment = homeFragment
            val fragmentTransaction: FragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.setCustomAnimations(
                android.R.anim.fade_in,
                android.R.anim.fade_out
            )
            fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG)
            fragmentTransaction.commitAllowingStateLoss()
        }
        if (mPendingRunnable != null) {
            mHandler!!.post(mPendingRunnable)
        }
        toggleFab()
        drawer!!.closeDrawers()
        AppAndroidUtilities.hideKeyboard(this@MainActivity)
        invalidateOptionsMenu()
    }

    // home
    private val homeFragment: Fragment
        private get() = when (navItemIndex) {
            0 -> {
                toolbar!!.visibility = View.GONE
                HomeFragment()
            }
            1 -> {
                toolbar!!.visibility = View.VISIBLE
                ProfileFragment()
            }
            2 -> {
                toolbar!!.visibility = View.VISIBLE
                HospitalFragment.newInstance("HOME")
            }
            3 -> {
                toolbar!!.visibility = View.VISIBLE
                ClinicsFragment.newInstance("HOME")
            }
            4 -> {
                toolbar!!.visibility = View.VISIBLE
                SettingsFragment()
            }
            else -> HomeFragment()
        }

    private fun setToolbarTitle() {
        supportActionBar!!.setTitle(activityTitles[navItemIndex])
    }

    private fun selectNavMenu() {
        navigationView!!.menu.getItem(navItemIndex).isChecked = true
    }

    private fun setUpNavigationView() {
        navigationView!!.setNavigationItemSelectedListener(NavigationView.OnNavigationItemSelectedListener { menuItem ->
            AppAndroidUtilities.hideKeyboard(this@MainActivity)
            when (menuItem.itemId) {
                R.id.nav_home -> {
                    navItemIndex = 0
                    CURRENT_TAG = TAG_HOME
                }
                R.id.nav_profile -> {
                    navItemIndex = 1
                    CURRENT_TAG = TAG_PROFILE
                }
                R.id.nav_hospital -> {
                    navItemIndex = 2
                    CURRENT_TAG = TAG_HOSPITAL
                }
                R.id.nav_clinics -> {
                    navItemIndex = 3
                    CURRENT_TAG = TAG_CLINICS
                }
                R.id.nav_settings -> {
                    navItemIndex = 4
                    CURRENT_TAG = TAG_SETTINGS
                }
                R.id.nav_about_us -> {
                    // launch new intent instead of loading fragment
                    startActivity(Intent(this@MainActivity, AboutUsActivity::class.java))
                    AppAndroidUtilities.startBottomToTopAnimation(this@MainActivity)
                    drawer!!.closeDrawers()
                    AppAndroidUtilities.hideKeyboard(this@MainActivity)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.nav_privacy_policy -> {
                    startActivity(Intent(this@MainActivity, TermsAndConditionActivity::class.java))
                    AppAndroidUtilities.startBottomToTopAnimation(this@MainActivity)
                    drawer!!.closeDrawers()
                    AppAndroidUtilities.hideKeyboard(this@MainActivity)
                    return@OnNavigationItemSelectedListener true
                }
                R.id.nav_logout -> {
                    showLogOutBottomSheetDialog()
                    drawer!!.closeDrawers()
                    AppAndroidUtilities.hideKeyboard(this@MainActivity)
                    return@OnNavigationItemSelectedListener true
                }
                else -> navItemIndex = 0
            }

            //Checking if the item is in checked state or not, if not make it in checked state
            if (menuItem.isChecked) {
                menuItem.isChecked = false
            } else {
                menuItem.isChecked = true
            }
            menuItem.isChecked = true
            loadHomeFragment()
            true
        })

        val actionBarDrawerToggle = object : ActionBarDrawerToggle(
            this,
            drawer,
            toolbar,
            R.string.openDrawer,
            R.string.closeDrawer
        ) {
            /** Called when a drawer has settled in a completely closed state.  */
            override fun onDrawerClosed(view: View) {
                super.onDrawerClosed(view)
                // Do whatever you want here
                val user=FirebaseAuth.getInstance().currentUser
                getDataOneTime(user?.phoneNumber!!)
            }

            /** Called when a drawer has settled in a completely open state.  */
            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                // Do whatever you want here
                val user=FirebaseAuth.getInstance().currentUser
                getDataOneTime(user?.phoneNumber!!)
            }
        }
        actionBarDrawerToggle.getDrawerArrowDrawable().setColor(Color.parseColor("#FFFFFF"))
        drawer!!.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.syncState()

    }

    override fun onBackPressed() {
        if (drawer!!.isDrawerOpen(GravityCompat.START)) {
            drawer!!.closeDrawers()
            AppAndroidUtilities.hideKeyboard(this@MainActivity)
            return
        }
        if (shouldLoadHomeFragOnBackPress) {
            if (navItemIndex != 0) {
                navItemIndex = 0
                CURRENT_TAG = TAG_HOME
                loadHomeFragment()
                return
            }
        }
        super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (navItemIndex == 0) {
            menuInflater.inflate(R.menu.main, menu)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.action_logout) {
            Toast.makeText(applicationContext, "Logout user!", Toast.LENGTH_LONG).show()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    // show or hide the fab
    private fun toggleFab() {
        if (navItemIndex == 0) fab!!.hide() else fab!!.hide()
    }

    companion object {
        private const val urlNavHeaderBg = "https://s3.amazonaws.com/www-inside-design/uploads/2018/11/material-hero-bg-284x402.png"
        private const val urlProfileImg = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRrSFDhYJUT8wKwUrI7xR9DJtz8TvwVVsT6RQ&usqp=CAU"
        var navItemIndex = 0
        private const val TAG_HOME = "home"
        private const val TAG_PROFILE = "profile"
        private const val TAG_HOSPITAL = "hospital"
        private const val TAG_CLINICS = "clinics"
        private const val TAG_SETTINGS = "settings"
        private const val TAG_PRIVACY_POLICY= "privacy policy"
        private const val TAG_ABOUT_US= "about us"
        private const val TAG_LOGOUT= "logout"
        var CURRENT_TAG = TAG_HOME
    }

    private fun showLogOutBottomSheetDialog() {
        val bottomSheetDialog = BottomSheetDialog(this, R.style.MyTransparentBottomSheetDialogTheme)
        bottomSheetDialog.setContentView(R.layout.bottom_sheet_logout_dialog)
        val btnSignout = bottomSheetDialog.findViewById<TextView>(R.id.btn_yes)
        val btnCancel = bottomSheetDialog.findViewById<TextView>(R.id.btn_no)
        btnSignout!!.setOnClickListener(View.OnClickListener {
            myPreferences?.setRegistrationStatus(false)
            auth.signOut()
            unsubscribe()
            val intent = Intent(this, SplashActivity::class.java)
            startActivity(intent)
            AppAndroidUtilities.startBackAnimation(this@MainActivity)
            finishAffinity()
            bottomSheetDialog.dismiss()
        })
        btnCancel!!.setOnClickListener(View.OnClickListener { bottomSheetDialog.dismiss() })
        bottomSheetDialog.show()
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this@MainActivity)
        super.onDestroy()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: MessageEvent?) {

        val msg = event!!.getMessage()
        val sender: String = event.from
        Log.e("###MSG", msg)
        if(msg.equals("SHOW"))
         {
             drawer!!.openDrawer(GravityCompat.START)
         }
        else if(msg.equals("BACKPRESSED"))
         {
            onBackPressed()
         }
        else if(msg.equals("COLLECTION_REQUEST_PLACED"))
        {
            AppAndroidUtilities.showBanner(this@MainActivity,
                LabResourceHelper.getString(R.string.collection_request_placed_successfully),1)

        }
        else if(msg.equals("SUCCESS_ALERT"))
        {
            AppAndroidUtilities.showBanner(this@MainActivity,LabResourceHelper.getString(R.string.success_update_alert),1)
        }
        EventBus.getDefault().removeStickyEvent(event)
    }

    private fun getDataOneTime(phoneNumber: String){
        val docRef=mFirebaseDatabaseInstances?.collection(AppConstantUtils.LAB)?.document(
            phoneNumber.replace(
                "+91",
                ""
            )
        )
        docRef?.get()?.addOnSuccessListener { documentSnapshot ->
            try {
                val user=documentSnapshot.toObject(Lab::class.java)
                loadNavHeader(user?.name!!)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun subscribe() {
        FirebaseMessaging.getInstance().subscribeToTopic("news")
    }

    fun unsubscribe() {
        FirebaseMessaging.getInstance().unsubscribeFromTopic("news")
    }
}