package com.labware.lab.activities
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import android.widget.LinearLayout
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager.widget.ViewPager
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.material.tabs.TabLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.QuerySnapshot
import com.labware.lab.R
import com.labware.lab.adapters.CollectionPointAdapter
import com.labware.lab.adapters.RequestsHistoryAdapter
import com.labware.lab.constants.AppConstantUtils
import com.labware.lab.fragments.HomeFragment
import com.labware.lab.fragments.HospitalFragment
import com.labware.lab.models.Hospital
import com.labware.lab.models.MyPreferences
import com.labware.lab.models.Requests
import com.labware.lab.utils.AppAndroidUtilities
import com.labware.lab.utils.ProgressDialogManager
import com.labware.lab.views.Button

import com.labware.lab.views.ViewPagerAdapter
import com.squats.fittr.utils.LabResourceHelper

class ActivityHistory : AppCompatActivity(),SwipeRefreshLayout.OnRefreshListener {

    private var toolbar: Toolbar? = null
    private var mAuth: FirebaseAuth?=null
    private var mFirebaseDatabaseInstances: FirebaseFirestore?=null
    private var recylerview_history: RecyclerView?=null
    private var mSwipeRefreshLayout: SwipeRefreshLayout?=null
    private var data:ArrayList<Requests>?=null
    private var requests: Requests?=null
    private var myPreferences: MyPreferences? = null
    private var adapter: RequestsHistoryAdapter? = null
    private var from: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)

        toolbar = findViewById(R.id.toolbar) as Toolbar
        recylerview_history = findViewById(R.id.recylerview_history) as RecyclerView
        recylerview_history?.layoutManager = LinearLayoutManager(this)
        myPreferences = MyPreferences.getPreferences(this)
        setSupportActionBar(toolbar)
        toolbar!!.setNavigationIcon(R.drawable.ic_arrow_back_white)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        findViewById<LinearLayout>(R.id.empty_view).addView(
            AppAndroidUtilities.setNewEmptyMessage(
                this, findViewById<LinearLayout>(
                    R.id.empty_view
                ), R.drawable.ic_empty_data, "No data found", "", "Ok", false
            )
        )
        toolbar!!.title="History"
        toolbar!!.setNavigationOnClickListener(View.OnClickListener {
            finish()
            AppAndroidUtilities.startTopToBottomAnimation(this@ActivityHistory)
        })

        mAuth=FirebaseAuth.getInstance()
        mFirebaseDatabaseInstances= FirebaseFirestore.getInstance()
        val settings = FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true)
            .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
            .build()
        mFirebaseDatabaseInstances?.setFirestoreSettings(settings)
        data = ArrayList<Requests>()
        data?.clear()
        mFirebaseDatabaseInstances?.collection(AppConstantUtils.REQUESTS)?.get()
            ?.addOnCompleteListener(OnCompleteListener<QuerySnapshot> { task ->
                if (task.isSuccessful) {
                    val list: MutableList<String> = ArrayList()
                    for (document in task.result) {
                        list.add(document.id)
                        Log.e("DOCUMENT ID", document.id)
                        getDataOneTime(document.id)
                    }
                }
            })
        mSwipeRefreshLayout = findViewById<SwipeRefreshLayout>(R.id.container)
        mSwipeRefreshLayout?.setOnRefreshListener(this)
        mSwipeRefreshLayout?.setColorSchemeColors(
            LabResourceHelper.getColor(R.color.colorPrimaryDark),
            LabResourceHelper.getColor(R.color.colorAccent),
            LabResourceHelper.getColor(R.color.colorPrimaryDark));

    }

    override fun onResume() {
        super.onResume()
    }
    override fun onContextItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                AppAndroidUtilities.startTopToBottomAnimation(this@ActivityHistory)
                return true
            }
        }
        return super.onContextItemSelected(item)
    }


    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    finish()
                    AppAndroidUtilities.startTopToBottomAnimation(this@ActivityHistory)
                    return true
                }
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        AppAndroidUtilities.startTopToBottomAnimation(this@ActivityHistory)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home -> {
                finish()
                AppAndroidUtilities.startTopToBottomAnimation(this@ActivityHistory)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getDataOneTime(requestId: String){
        ProgressDialogManager.showProgressDialog(this)
        Log.e("@@@requestId111111", requestId)
        val docRef=mFirebaseDatabaseInstances?.collection(AppConstantUtils.REQUESTS)?.document(
            requestId
        )
        docRef?.get()?.addOnSuccessListener { documentSnapshot ->
            ProgressDialogManager.dismissProgressDialog(this)
            findViewById<ProgressBar>(R.id.progressBar).visibility=View.INVISIBLE
            mSwipeRefreshLayout?.setRefreshing(false)
            try {
                val obj = documentSnapshot.get("Audit trail") as HashMap<*, *>
                var currentStatus = obj.get("Current status").toString()!!
                requests= Requests()
                val obj2= obj.get("Registration") as HashMap<*, *>
                requests?.patientName=obj2.get("Patient name").toString()!!
                requests?.requestId=obj.get("Request Id").toString()!!
                requests?.timestamp=obj.get("Timestamp").toString()!!
                requests?.currentStatus=obj.get("Current status").toString()!!
                requests?.destinationLatLng=obj.get("Destination LatLng").toString()!!

                if(!currentStatus.equals("Open")) {
                    data?.add(requests!!)
                    adapter = RequestsHistoryAdapter(data!!)
                    recylerview_history?.adapter = adapter
                    findViewById<LinearLayout>(R.id.empty_view)?.visibility = View.GONE
                    recylerview_history?.visibility = View.VISIBLE
                    recylerview_history?.adapter?.notifyDataSetChanged()
                    Log.e("data_sizeeee", data.toString())
                }

                if(data?.size==0)
                {
                    recylerview_history?.visibility = View.GONE
                    findViewById<LinearLayout>(R.id.empty_view)?.visibility = View.VISIBLE
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }

            if(!data.isNullOrEmpty())
            {
                var controller : LayoutAnimationController =
                    AnimationUtils.loadLayoutAnimation(
                        this,
                        R.anim.layout_animation_fall_down
                    )
                recylerview_history?.setLayoutAnimation(controller)
                recylerview_history?.getAdapter()?.notifyDataSetChanged()
                recylerview_history?.scheduleLayoutAnimation()
            }
        }
        docRef?.get()?.addOnFailureListener(OnFailureListener { mSwipeRefreshLayout?.setRefreshing(false) })
    }

    override fun onRefresh() {
        data?.clear()
        mFirebaseDatabaseInstances?.collection(AppConstantUtils.REQUESTS)?.get()
            ?.addOnCompleteListener(OnCompleteListener<QuerySnapshot> { task ->
                if (task.isSuccessful) {
                    val list: MutableList<String> = ArrayList()
                    for (document in task.result) {
                        list.add(document.id)
                        Log.e("DOCUMENT ID", document.id)
                        getDataOneTime(document.id)
                    }
                }
            })
    }

}
