package com.labware.lab.activities

import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.budiyev.android.codescanner.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.SetOptions
import com.labware.lab.R
import com.labware.lab.constants.AppConstantUtils
import com.labware.lab.models.MessageEvent
import com.labware.lab.models.MyPreferences
import com.labware.lab.utils.AppAndroidUtilities
import com.labware.lab.utils.AppDateTimeUtils
import com.labware.lab.utils.ProgressDialogManager
import com.squats.fittr.utils.LabResourceHelper
import org.greenrobot.eventbus.EventBus

class ScanQRCodeActivity : AppCompatActivity() {

    private lateinit var codeScanner: CodeScanner
    var toolbar: Toolbar? = null
    private var mAuth: FirebaseAuth? = null
    private var mFirebaseDatabaseInstances: FirebaseFirestore? = null
    private var myPreferences: MyPreferences? = null
    private var containerQR:String? ="123456"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_qr_code)

        mAuth = FirebaseAuth.getInstance()
        mFirebaseDatabaseInstances = FirebaseFirestore.getInstance()
        val settings = FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true)
            .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
            .build()
        mFirebaseDatabaseInstances?.setFirestoreSettings(settings)
        myPreferences = MyPreferences.getPreferences(this@ScanQRCodeActivity)

        val scannerView = findViewById<CodeScannerView>(R.id.scanner_view)
        codeScanner = CodeScanner(this@ScanQRCodeActivity, scannerView)
        // Parameters (default values)
        codeScanner.camera = CodeScanner.CAMERA_BACK // or CAMERA_FRONT or specific camera id
        codeScanner.formats = CodeScanner.ALL_FORMATS // list of type BarcodeFormat,
        // ex. listOf(BarcodeFormat.QR_CODE)
        codeScanner.autoFocusMode = AutoFocusMode.SAFE // or CONTINUOUS
        codeScanner.scanMode = ScanMode.SINGLE // or CONTINUOUS or PREVIEW
        codeScanner.isAutoFocusEnabled = true // Whether to enable auto focus or not
        codeScanner.isFlashEnabled = false // Whether to enable flash or not
        // Callbacks
        codeScanner.decodeCallback = DecodeCallback {
            runOnUiThread {
                //Toast.makeText(this, "Scan result: ${it.text}", Toast.LENGTH_LONG).show()
                if(containerQR.equals(it.text))
                {
                    val collectionDoc: DocumentReference = mFirebaseDatabaseInstances?.collection(
                        AppConstantUtils.REQUESTS)
                        ?.document("21b7668d-dedf-4cdd-a333-2e6b9ae297bc")!!
                    var requestsMap: MutableMap<String, Any> = java.util.HashMap()
                    val mapAuditTrail: MutableMap<String, Any> = HashMap()
                    mapAuditTrail.put("Timestamp", AppDateTimeUtils.getCurrentTimeStamp())
                    mapAuditTrail.put("Current status","Completed")
                    requestsMap.put("Audit trail",mapAuditTrail);
                    collectionDoc.set(requestsMap, SetOptions.merge())
                        .addOnSuccessListener { documentReference ->
                            EventBus.getDefault().postSticky(
                                MessageEvent(
                                    "SUCCESS_ALERT",
                                    "ScanQRCodeActivity.kt"
                                )
                            )
                            onBackPressed()
                        }
                        .addOnFailureListener { e ->
                            AppAndroidUtilities.showBanner(this@ScanQRCodeActivity,LabResourceHelper.getString(R.string.something_went_worng),4)
                        }
                }
                else
                {
                    AppAndroidUtilities.showBanner(this@ScanQRCodeActivity,LabResourceHelper.getString(R.string.qr_code_error),4)
                }
            }
        }
        codeScanner.errorCallback = ErrorCallback { // or ErrorCallback.SUPPRESS
            runOnUiThread {
                AppAndroidUtilities.showBanner(this@ScanQRCodeActivity,LabResourceHelper.getString(R.string.something_went_worng),4)
            }
        }
        scannerView.setOnClickListener {
            codeScanner.startPreview()
        }

        toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar!!.setNavigationIcon(R.drawable.ic_arrow_back_white)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar!!.title="Scan QR code"
        toolbar!!.setNavigationOnClickListener(View.OnClickListener {
            finish()
            AppAndroidUtilities.startTopToBottomAnimation(this@ScanQRCodeActivity)
        })

        ProgressDialogManager.showProgressDialog(this@ScanQRCodeActivity)
        Handler().postDelayed({
            findViewById<ProgressBar>(R.id.progressBar).visibility=View.INVISIBLE
            ProgressDialogManager.dismissProgressDialog(this@ScanQRCodeActivity)
        }, 3000)
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                AppAndroidUtilities.startTopToBottomAnimation(this@ScanQRCodeActivity)
                return true
            }
        }
        return super.onContextItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()
    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        AppAndroidUtilities.startTopToBottomAnimation(this@ScanQRCodeActivity)
    }
}