package com.labware.lab.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.chaos.view.PinView
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.labware.lab.R
import com.labware.lab.utils.AppAndroidUtilities
import com.labware.lab.utils.ProgressDialogManager
import java.util.concurrent.TimeUnit


@Suppress("DEPRECATION")
class VerifyOtpActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private var storedVerificationId: String? = ""
    private lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    private var txt_resend_code:com.labware.lab.views.TextView?=null
    private var firstPinView:PinView?=null
    var mobileNumber:String=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_otp)

        ProgressDialogManager.showProgressDialog(this@VerifyOtpActivity)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        mobileNumber = intent.getStringExtra("MOBILE").toString()
        findViewById<com.labware.lab.views.TextView>(R.id.txt_enter_your_no).
        setText("Enter the 6-digit code sent to your +91 " + mobileNumber)


        txt_resend_code=findViewById<com.labware.lab.views.TextView>(R.id.txt_resend_code)
        firstPinView=findViewById<PinView>(R.id.firstPinView)

        FirebaseApp.initializeApp(this)
        auth = Firebase.auth

        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                Log.d(TAG, "onVerificationCompleted:$credential")
                var code = credential.getSmsCode();
                if (code != null) {
                    firstPinView?.setText(code);
                    verifyCode(code);
                }

            }

            override fun onVerificationFailed(e: FirebaseException) {
                Log.w(TAG, "onVerificationFailed", e)
                ProgressDialogManager.dismissProgressDialog(this@VerifyOtpActivity)
                if (e is FirebaseAuthInvalidCredentialsException) {
                    AppAndroidUtilities.showBanner(
                        this@VerifyOtpActivity,
                     "We have blocked all requests from this device due to unusual activity. Try again later.",
                        4
                    )
                } else if (e is FirebaseTooManyRequestsException) {
                    AppAndroidUtilities.showBanner(
                        this@VerifyOtpActivity,
                       "We have blocked all requests from this device due to unusual activity. Try again later.",
                        4
                    )
                }
            }

            override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken
            ) {
                ProgressDialogManager.dismissProgressDialog(this@VerifyOtpActivity)
                Log.d(TAG, "onCodeSent:$verificationId")
                storedVerificationId = verificationId
                resendToken = token
            }
        }

        startPhoneNumberVerification("+91" + mobileNumber)

        txt_resend_code?.setOnClickListener(View.OnClickListener {
            try {
                ProgressDialogManager.showProgressDialog(this@VerifyOtpActivity)
                resendVerificationCode("+91"+mobileNumber, resendToken)
            } catch (e: Exception) {
                e.printStackTrace()
                ProgressDialogManager.dismissProgressDialog(this@VerifyOtpActivity)
                AppAndroidUtilities.showBanner(
                    this@VerifyOtpActivity,
                    "Something went wrong please try again later.",
                    4
                )
            }
        })

        findViewById<TextView>(R.id.ic_back).setOnClickListener(View.OnClickListener {
            finish()
            AppAndroidUtilities.startBackAnimation(this@VerifyOtpActivity)
        })

        findViewById<Button>(R.id.btn_login).setOnClickListener(View.OnClickListener {
            if (!firstPinView?.editableText.toString().isEmpty()) {
                ProgressDialogManager.showProgressDialog(this@VerifyOtpActivity)
                verifyPhoneNumberWithCode(
                    storedVerificationId,
                    firstPinView?.editableText.toString().trim()
                )
            } else {
                ProgressDialogManager.dismissProgressDialog(this@VerifyOtpActivity)
                AppAndroidUtilities.showBanner(this@VerifyOtpActivity, "Invalid OTP", 4)
            }
        })
    }

    private fun verifyCode(code: String) {
        val credential = PhoneAuthProvider.getCredential(storedVerificationId!!, code)
        signInWithPhoneAuthCredential(credential)
    }

    override fun onStart() {
        super.onStart()
        val currentUser = auth.currentUser
        updateUI(currentUser)

    }

    private fun startPhoneNumberVerification(phoneNumber: String) {
        val options = PhoneAuthOptions.newBuilder(auth)
            .setPhoneNumber(phoneNumber)
            .setTimeout(60L, TimeUnit.SECONDS)
            .setActivity(this)
            .setCallbacks(callbacks)
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)

    }

    private fun verifyPhoneNumberWithCode(verificationId: String?, code: String) {
        try {
            val credential = PhoneAuthProvider.getCredential(verificationId!!, code)
            signInWithPhoneAuthCredential(credential)
        } catch (e: Exception) {
            e.printStackTrace()
            ProgressDialogManager.dismissProgressDialog(this@VerifyOtpActivity)
            AppAndroidUtilities.showBanner(this@VerifyOtpActivity, "Invalid OTP", 4);
        }
    }

    private fun resendVerificationCode(
        phoneNumber: String,
        token: PhoneAuthProvider.ForceResendingToken?
    ) {
        val optionsBuilder = PhoneAuthOptions.newBuilder(auth)
            .setPhoneNumber(phoneNumber)       // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(this)                 // Activity (for callback binding)
            .setCallbacks(callbacks)          // OnVerificationStateChangedCallbacks
        if (token != null) {
            optionsBuilder.setForceResendingToken(token) // callback's ForceResendingToken
        }
        PhoneAuthProvider.verifyPhoneNumber(optionsBuilder.build())
    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    if(!(this@VerifyOtpActivity as AppCompatActivity).isFinishing) {
                        AppAndroidUtilities.showBanner(
                            this@VerifyOtpActivity,
                            "Login successfully",
                            1
                        )
                    }
                    Log.d(TAG, "signInWithCredential:success")
                    val user = task.result?.user
                    Handler().postDelayed({
                        ProgressDialogManager.dismissProgressDialog(this@VerifyOtpActivity)
                        val intent = Intent(this, ActivityUserProfile::class.java)
                        intent.putExtra("MOBILE", mobileNumber)
                        startActivity(intent)
                        AppAndroidUtilities.startFwdAnimation(this@VerifyOtpActivity)
                        finishAffinity()
                    }, 2000)
                } else {
                    ProgressDialogManager.dismissProgressDialog(this@VerifyOtpActivity)
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                    if (task.exception is FirebaseAuthInvalidCredentialsException) {
                    }
                    AppAndroidUtilities.showBanner(this@VerifyOtpActivity, "Invalid OTP", 4)
                }
            }
    }

    private fun updateUI(user: FirebaseUser? = auth.currentUser) {

    }

    companion object {
        private const val TAG = "PhoneAuthActivity"
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                AppAndroidUtilities.startBackAnimation(this@VerifyOtpActivity)
                return true
            }
        }
        return super.onContextItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        AppAndroidUtilities.startBackAnimation(this@VerifyOtpActivity)
    }
}
