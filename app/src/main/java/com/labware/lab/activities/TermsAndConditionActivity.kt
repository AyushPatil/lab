package com.labware.lab.activities

import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.labware.lab.R
import com.labware.lab.utils.AppAndroidUtilities
import com.labware.lab.utils.ProgressDialogManager
import com.labware.lab.views.Button
import com.labware.lab.views.ObservableWebView
import kotlinx.android.synthetic.main.activity_terms_constion.*


class TermsAndConditionActivity : AppCompatActivity(), ObservableWebView.ObservableWebInterface {
    private var webView: ObservableWebView? = null
    var toolbar: Toolbar? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms_constion)
        webView = findViewById(R.id.webView)
        setWebViewInApp()
        toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        toolbar!!.setNavigationIcon(R.drawable.ic_arrow_back_white)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar!!.title="Privacy & policy"
        toolbar!!.setNavigationOnClickListener(View.OnClickListener {
            finish()
            AppAndroidUtilities.startTopToBottomAnimation(this@TermsAndConditionActivity)
        })
        ProgressDialogManager.showProgressDialog(this@TermsAndConditionActivity)
        webView!!.setObservableScrollListener(this)
    }
    private fun updateButtonIAccept() {
        Thread(Runnable {
            runOnUiThread {
                buttonIAccept.background =
                    ContextCompat.getDrawable(this, R.drawable.bg_button_gradient)
                buttonIAccept.isEnabled = true
            }
        }).start()
    }
    override fun onContextItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                AppAndroidUtilities.startTopToBottomAnimation(this@TermsAndConditionActivity)
                return true
            }
        }
        return super.onContextItemSelected(item)
    }

    override fun atBottomOfScrollView(atBottom: Boolean) {
        updateButtonIAccept()
    }
    fun setWebViewInApp() {
        webView!!.settings.javaScriptEnabled = true
        webView!!.settings.loadsImagesAutomatically = true
        webView!!.scrollBarStyle = View.SCROLLBARS_INSIDE_OVERLAY
        webView!!.settings.setSupportZoom(true)
        webView!!.settings.builtInZoomControls = true
        webView!!.settings.displayZoomControls = false
        webView!!.settings.setRenderPriority(WebSettings.RenderPriority.HIGH)
        if (Build.VERSION.SDK_INT >= 19) {
            // chromium, enable hardware acceleration
            webView!!.setLayerType(View.LAYER_TYPE_HARDWARE, null)
        } else {
            // older android version, disable hardware acceleration
            webView!!.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        }
        val activity: Activity = this
        webView!!.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, progress: Int) {
                // Activities and WebViews measure progress with different scales.
                // The progress meter will automatically disappear when we reach 100%
                activity.setProgress(progress * 1000)
            }
        }
        webView!!.webViewClient = object : WebViewClient() {
            override fun onReceivedError(
                view: WebView,
                errorCode: Int,
                description: String,
                failingUrl: String
            ) {
                findViewById<ProgressBar>(R.id.progressBar).visibility=View.INVISIBLE
                Toast.makeText(
                    activity,
                    "Something went wrong please try again later.$description", Toast.LENGTH_SHORT
                ).show()
            }
        }
        webView!!.loadUrl("https://www.aarogyasetu.gov.in/terms-conditions/")

        webView!!.webViewClient = object : WebViewClient() {

            override fun onPageFinished(view: WebView, url: String) {
                findViewById<ProgressBar>(R.id.progressBar).visibility=View.GONE
                ProgressDialogManager.dismissProgressDialog(this@TermsAndConditionActivity)
            }
        }
        Handler().postDelayed({
            findViewById<ProgressBar>(R.id.progressBar).visibility=View.GONE
        }, 5000)

        findViewById<Button>(R.id.buttonIAccept).setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    if (webView!!.canGoBack()) {
                        webView!!.goBack()
                    } else {
                        //Intent mainIntent = new Intent(PaymentActivity.this, SubscriptionActivity.class);
                        //startActivity(mainIntent);
                        finish()
                        AppAndroidUtilities.startTopToBottomAnimation(this@TermsAndConditionActivity)
                    }
                    return true
                }
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        AppAndroidUtilities.startTopToBottomAnimation(this@TermsAndConditionActivity)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home -> {
                finish()
                AppAndroidUtilities.startTopToBottomAnimation(this@TermsAndConditionActivity)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}