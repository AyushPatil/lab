package com.labware.lab.activities

import `in`.shadowfax.proswipebutton.ProSwipeButton
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.tabs.TabLayout
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.SetOptions
import com.google.firebase.messaging.FirebaseMessaging
import com.labware.lab.R
import com.labware.lab.constants.AppConstantUtils
import com.labware.lab.fragments.*
import com.labware.lab.models.*
import com.labware.lab.utils.AppAndroidUtilities
import com.labware.lab.utils.AppDateTimeUtils
import com.labware.lab.views.Button
import com.labware.lab.views.ViewPagerAdapter
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList
import com.squats.fittr.utils.LabResourceHelper

class ActivitySearchCollectionPoint : AppCompatActivity() {

    private var viewPagerAdapter: ViewPagerAdapter? = null
    private var viewPager: ViewPager? = null
    private var tabLayout: TabLayout? = null
    private var btnAddNew: Button? = null
    private var toolbar: Toolbar? = null
    private var btn_swipe_right_to_collect: ProSwipeButton? = null
    private var mAuth: FirebaseAuth?=null
    private var mFirebaseDatabaseInstances: FirebaseFirestore?=null
    private var tests: ArrayList<String>?=null
    private var tubes: ArrayList<Tube>?=null
    private var collectFrom: String?="0"
    private var dropOff: String?="0"
    var tube:Tube?=null
    private val AUTH_KEY = "key=AAAAErOZeYE:APA91bFCjqPncIVnT7ddXZ7zGI4iSjAfxU5xrRJ6FKkqN6UvJBKONXmO_tVu3RCB4d7ttGNnpOSkF3DkiGnXGO6lbkBP1OR6prph7EgZcpREcxmCKBDBb2PNihHocXRlU-fitW_YCa8y"
    private var token: String? = null
    private var from: String? = null
    private var labId: String? = null
    private var originLatLang: String? = null
    private var destinationLatLng: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_collection_point)
        EventBus.getDefault().register(this@ActivitySearchCollectionPoint)
        toolbar = findViewById(R.id.toolbar) as Toolbar
        btn_swipe_right_to_collect = findViewById(R.id.btn_swipe_right_to_collect) as ProSwipeButton
        tests= ArrayList()
        tests?.add("0")
        tubes= ArrayList()
        tube= Tube()
        tube?.name="0"
        tube?.code="0"
        tube?.verify=false
        tube?.timestamp=AppDateTimeUtils.getCurrentTimeStamp()
        tubes?.add(tube!!)
        mAuth=FirebaseAuth.getInstance()
        mFirebaseDatabaseInstances= FirebaseFirestore.getInstance()

        labId= mAuth?.currentUser?.phoneNumber.toString().replace("+91","")

        val settings = FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true)
            .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
            .build()
        mFirebaseDatabaseInstances?.setFirestoreSettings(settings)

        setSupportActionBar(toolbar)
        toolbar!!.setNavigationIcon(R.drawable.ic_arrow_back_white)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        toolbar!!.title="Search collection point"
        toolbar!!.setNavigationOnClickListener(View.OnClickListener {
            finish()
            AppAndroidUtilities.startTopToBottomAnimation(this@ActivitySearchCollectionPoint)
        })

        viewPager = findViewById(R.id.viewpager)

        viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)

        viewPagerAdapter!!.add(HospitalFragment.newInstance("SEARCH"), "Hospital")
        viewPagerAdapter!!.add(ClinicsFragment.newInstance("SEARCH"), "Clinic")
        viewPagerAdapter!!.add(HomesFragment.newInstance("SEARCH"), "Home")

        viewPager!!.setAdapter(viewPagerAdapter)

        tabLayout = findViewById(R.id.tab_layout)
        btnAddNew = findViewById(R.id.bnt_add_new)
        btnAddNew!!.setOnClickListener(View.OnClickListener {
            val intentinstruments =
                Intent(this, AddCollectionPointActivity::class.java)
            startActivity(intentinstruments)
            AppAndroidUtilities.startBottomToTopAnimation(this)
        })
        tabLayout!!.setupWithViewPager(viewPager)

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {

            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                viewPager!!.currentItem = tabLayout!!.selectedTabPosition
            }


        })


        Handler().postDelayed({
            findViewById<ProgressBar>(R.id.progressBar).visibility = View.INVISIBLE
        }, 3000)

        btn_swipe_right_to_collect?.setOnSwipeListener(object : ProSwipeButton.OnSwipeListener {
            override fun onSwipeConfirm() {
                var requestsMap: MutableMap<String, Any> = HashMap()
                var subRequestMap: MutableMap<String, Any> = HashMap()
                var timeStamp: MutableMap<String, Any> = HashMap()
                var proofOfCollection: MutableMap<String, Any> = HashMap()
                var registration: MutableMap<String, Any> = HashMap()
                var proofOfPrescription: MutableMap<String, Any> = HashMap()
                var orderOfDraw: MutableMap<String, Any> = HashMap()
                var containerQR: MutableMap<String, Any> = HashMap()
                var acknowledgeDropOff: MutableMap<String, Any> = HashMap()
                var startDropOff: MutableMap<String, Any> = HashMap()
                var arrivedAtDropOff: MutableMap<String, Any> = HashMap()
                var enrouteCollectionPoint: MutableMap<String, Any> = HashMap()
                var enrouteDropPoint: MutableMap<String, Any> = HashMap()
                var handover: MutableMap<String, Any> = HashMap()
                var eta: MutableMap<String, Any> = HashMap()
                var patient: MutableMap<String, Any> = HashMap()
                var phlebotomist: MutableMap<String, Any> = HashMap()
                var tat: MutableMap<String, Any> = HashMap()

                var requestId=UUID.randomUUID().toString()

                tat.put("TAT", "")
                phlebotomist.put("Phlebotomist", "")
                eta.put("ETA", "")
                patient.put("Patient", "")

                handover.put("Success", false)
                handover.put("Timestamp", "")
                enrouteCollectionPoint.put("Timestamp", "")
                arrivedAtDropOff.put("Timestamp", "")
                startDropOff.put("Timestamp", "")
                acknowledgeDropOff.put("Timestamp", "")

                containerQR.put("Container QR", "")
                containerQR.put("Code", "")
                containerQR.put("Scan", false)
                containerQR.put("Timestamp", "")

                orderOfDraw.put("Sample type", "")
                orderOfDraw.put("Timestamp", "")

                proofOfPrescription.put("Prescription photo", "")
                proofOfPrescription.put("Patient sign", "")
                proofOfPrescription.put("Timestamp", "")
                proofOfPrescription.put("Payment mode", "")
                proofOfPrescription.put("Tests", "")

                registration.put("Patient name", "")
                registration.put("Age", "")
                registration.put("Gender", "")
                registration.put("Medical history", "")
                registration.put("Timestamp", "")

                proofOfCollection.put("Patient name", "")
                proofOfCollection.put("OTP", "")
                proofOfCollection.put("Mobile", "")
                proofOfCollection.put("Timestamp", "")
                proofOfCollection.put("Verify", false)
                timeStamp.put("Timestamp", "")

                subRequestMap.put("Acknowledge collection", timeStamp)
                subRequestMap.put("Start collection", timeStamp)
                subRequestMap.put("Arrived at collection", timeStamp)
                subRequestMap.put("Proof of collection", proofOfCollection)
                subRequestMap.put("Registration", registration)
                subRequestMap.put("Proof of prescription", proofOfPrescription)
                subRequestMap.put("Order of draw", orderOfDraw)
                subRequestMap.put("Container QR", containerQR)
                subRequestMap.put("Acknowledge drop off", acknowledgeDropOff)
                subRequestMap.put("Start drop off", startDropOff)
                subRequestMap.put("Arrived at drop off", arrivedAtDropOff)
                subRequestMap.put("Enroute collection point", enrouteCollectionPoint)
                subRequestMap.put("Enroute drop point", enrouteDropPoint)
                subRequestMap.put("Handover", handover)
                subRequestMap.put("Collect from", collectFrom!!)
                subRequestMap.put("Current status", "Open")
                subRequestMap.put("Drop off", dropOff!!)
                subRequestMap.put("ETA", eta)
                subRequestMap.put("Patient", patient)
                subRequestMap.put("Phlebotomist", phlebotomist)
                subRequestMap.put("TAT", tat)
                subRequestMap.put("Timestamp", AppDateTimeUtils.getCurrentTimeStamp())
                subRequestMap.put("Request Id", requestId)

                subRequestMap.put("Origin LatLng", originLatLang!!)
                subRequestMap.put("Destination LatLng", destinationLatLng!!)

                requestsMap.put("Audit trail", subRequestMap)

                mFirebaseDatabaseInstances?.collection(AppConstantUtils.REQUESTS)?.document(
                    requestId
                )
                    ?.set(requestsMap, SetOptions.merge())
                    ?.addOnSuccessListener { documentReference ->
                        EventBus.getDefault().postSticky(
                            MessageEvent(
                                "COLLECTION_REQUEST_PLACED",
                                "ActivitySearchCollectionPoint.kt"
                            )
                        )
                        onBackPressed()
                        btn_swipe_right_to_collect?.showResultIcon(true)
                        sendTopic(requestId,from!!,labId!!)
                    }
                    ?.addOnFailureListener { e ->
                        AppAndroidUtilities.showBanner(this@ActivitySearchCollectionPoint,LabResourceHelper.getString(R.string.something_went_worng),4)
                        btn_swipe_right_to_collect?.showResultIcon(false)
                    }

            }
        })

        FirebaseMessaging.getInstance().token
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }
                token = task.result
            })
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                AppAndroidUtilities.startTopToBottomAnimation(this@ActivitySearchCollectionPoint)
                return true
            }
        }
        return super.onContextItemSelected(item)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (event.action == KeyEvent.ACTION_DOWN) {
            when (keyCode) {
                KeyEvent.KEYCODE_BACK -> {
                    finish()
                    AppAndroidUtilities.startTopToBottomAnimation(this@ActivitySearchCollectionPoint)
                    return true
                }
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        AppAndroidUtilities.startTopToBottomAnimation(this@ActivitySearchCollectionPoint)
    }

    override fun onDestroy() {
        EventBus.getDefault().unregister(this@ActivitySearchCollectionPoint)
        super.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.home -> {
                finish()
                AppAndroidUtilities.startTopToBottomAnimation(this@ActivitySearchCollectionPoint)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: CollectionPointEvent?) {

        var msg = event!!.getMessage()
        var mCollectFrom: String = event.getCollectFrom()
        var mDropOff: String = event.getDropOff()
        from= event.getRequestFrom()
        originLatLang= event.getOriginLatLng()
        destinationLatLng= event.getDestinationLatLng()
        Log.e("###MSG", msg)
        Log.e("###originLatLang", originLatLang.toString())
        Log.e("###destinationLatLng", destinationLatLng.toString())
        if(msg.equals("SHOW_SWIPE_TO_COLLECT"))
        {
           btn_swipe_right_to_collect?.visibility=View.VISIBLE
           btnAddNew?.visibility=View.GONE
           collectFrom=mCollectFrom
           dropOff=mDropOff
        }
        else if(msg.equals("HIDE_SWIPE_TO_COLLECT"))
        {
            btn_swipe_right_to_collect?.visibility=View.GONE
            btnAddNew?.visibility=View.VISIBLE
            collectFrom="0"
            dropOff="0"
        }
        EventBus.getDefault().removeStickyEvent(event)
    }

    fun subscribe() {
        FirebaseMessaging.getInstance().subscribeToTopic("news")
    }

    fun unsubscribe() {
        FirebaseMessaging.getInstance().unsubscribeFromTopic("news")
    }

    fun sendToken(requestId:String) {
        sendWithOtherThread("token",requestId,"","")
    }

    fun sendTokens(requestId:String) {
        sendWithOtherThread("tokens",requestId,"","")
    }

    fun sendTopic(requestId:String,from:String,labId:String) {
        sendWithOtherThread("topic",requestId,from,labId)
    }

    private fun sendWithOtherThread(type: String,requestId:String,from:String,labId:String) {
        Thread { pushNotification(type,requestId,from,labId) }.start()
    }

    private fun pushNotification(type: String,requestId:String,from:String,labId:String) {
        val jPayload = JSONObject()
        //val jNotification = JSONObject()
        val jData = JSONObject()
        try {
            jData.put("title", "Lab")
            jData.put("body", "You have received collection request from-"+from)
            jData.put("sound", "default")
            jData.put("requestId", requestId)
            jData.put("labId", labId)
            jData.put("click_action", "OPEN_ACTIVITY_1")
            jData.put("icon", "ic_notification")
            jData.put("picture", "https://miro.medium.com/max/1400/1*QyVPcBbT_jENl8TGblk52w.png")
            when (type) {
                "tokens" -> {
                    val ja = JSONArray()
                    ja.put("AAAAErOZeYE:APA91bFCjqPncIVnT7ddXZ7zGI4iSjAfxU5xrRJ6FKkqN6UvJBKONXmO_tVu3RCB4d7ttGNnpOSkF3DkiGnXGO6lbkBP1OR6prph7EgZcpREcxmCKBDBb2PNihHocXRlU-fitW_YCa8y")
                    ja.put(token)
                    jPayload.put("registration_ids", ja)
                }
                "topic" -> jPayload.put("to", "/topics/news")
                "condition" -> jPayload.put("condition", "'sport' in topics || 'news' in topics")
                else -> jPayload.put("to", token)
            }
            jPayload.put("priority", "high")
            //jPayload.put("notification", jNotification)
            jPayload.put("data", jData)
            val url = URL("https://fcm.googleapis.com/fcm/send")
            val conn: HttpURLConnection = url.openConnection() as HttpURLConnection
            conn.setRequestMethod("POST")
            conn.setRequestProperty("Authorization", AUTH_KEY)
            conn.setRequestProperty("Content-Type", "application/json")
            conn.setDoOutput(true)

            // Send FCM message content.
            val outputStream: OutputStream = conn.getOutputStream()
            outputStream.write(jPayload.toString().toByteArray())

            // Read FCM response.
            val inputStream: InputStream = conn.getInputStream()
            val resp = convertStreamToString(inputStream)
            val h: Handler = Handler(Looper.getMainLooper())
            h.post {}
        } catch (e: JSONException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun convertStreamToString(`is`: InputStream): String {
        val s: Scanner = Scanner(`is`).useDelimiter("\\A")
        return if (s.hasNext()) s.next().replace(",", ",\n") else ""
    }
}
