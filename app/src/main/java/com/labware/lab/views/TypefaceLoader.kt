package com.labware.lab.views

import android.content.Context
import android.graphics.Typeface
import android.util.Log
import java.util.Hashtable

object TypefaceLoader {

    private val TAG = "TypefaceLoader"

    private val cache = Hashtable<String, Typeface>()

    operator fun get(context: Context, assetPath: String): Typeface? {
        synchronized(cache) {
            if (!cache.containsKey(assetPath)) {
                try {
                    val t = Typeface.createFromAsset(context.assets, assetPath)
                    cache[assetPath] = t
                } catch (e: Exception) {
                    Log.e(TAG, "Could not get typeface '" + assetPath + "' because " + e.message)
                    return get(context, assetPath)
                }

            }
            return cache[assetPath]
        }
    }
}
