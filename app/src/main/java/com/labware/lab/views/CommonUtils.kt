package com.labware.lab.views

import android.os.Environment
import android.util.Patterns
import java.util.regex.Pattern

object CommonUtils {

    val DATA_DIR = ".schoolApp"
    val EMAIL_PATTERN = "^[a-zA-Z0-9_+&*-]+(?:\\." +
            "[a-zA-Z0-9_+&*-]+)*@" +
            "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
            "A-Z]{2,7}$"
    //public static final String MOBILE_NUMBER_PATTERN = "^[789]\\\\d{9}$";
    val MOBILE_NUMBER_PATTERN = "[7-9][0-9]{9}"
    val DOB_PATTERN = "dd-MM-yyyy"
    var summeryState: Boolean = false

    /**
     * Checks storage availability
     *
     * @return True-If storage available <br></br> False- In storage not available
     */
    val isExtStorageAvailable: Boolean
        get() {
            val state = Environment.getExternalStorageState()
            return Environment.MEDIA_MOUNTED == state
        }

    /**
     * Check for valid url
     *
     * @param url
     * @return True-If valid <br></br> False- If invalid
     */
    fun validateURL(url: String): Boolean {
        return Patterns.WEB_URL.matcher(url).matches()
    }


    /**
     * Validate the email address
     *
     * @param email
     * @return
     */
    fun validateEmail(email: String): Boolean {
        val emailPattern = Pattern.compile(EMAIL_PATTERN)
        val matcher = emailPattern.matcher(email)
        return matcher.matches()
    }

    /**
     * Validate the mobile
     */
    fun validateMobileNumber(mobNum: String): Boolean {
        val mobile = Pattern.compile(MOBILE_NUMBER_PATTERN)
        val matcher = mobile.matcher(mobNum)
        return matcher.matches()
    }

    /**
     * Returns the font name.
     *
     * @param index
     * @return
     */

    fun getFontName(index: Int): String {
        var font = "fonts/Lato-Regular.ttf"
        when (index) {
            2 -> font = "fonts/Lato-Black.ttf"
            3 -> font = "fonts/Lato-BlackItalic.ttf"
            4 -> font = "fonts/Lato-Bold.ttf"
            5 -> font = "fonts/Lato-BoldItalic.ttf"
            6 -> font = "fonts/Lato-Italic.ttf"
            7 -> font = "fonts/Lato-Light.ttf"
            8 -> font = "fonts/Lato-LightItalic.ttf"
            9 -> font = "fonts/Lato-Thin.ttf"
            10 -> font = "fonts/Lato-ThinItalic.ttf"
        }
        return font
    }

}