package com.labware.lab.views

import android.content.Context
import android.util.AttributeSet
import android.widget.Switch
import com.labware.lab.R

class SwitchCompat : Switch {

    private var mContext: Context? = null
    private val font = "Lato-Regular.ttf"

    constructor(context: Context) : super(context) {
        this.mContext = context
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        this.mContext = context
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        this.mContext = context
        init(context, attrs)
    }

    private fun init() {
        if (!isInEditMode) {
            val typeface = TypefaceLoader.get(context!!, "fonts/$font")
            setTypeface(typeface)
        }
    }

    private fun init(context: Context, attrs: AttributeSet) {
        if (!isInEditMode) {
            val styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.SwitchCompat)
            val index = styledAttrs.getInt(R.styleable.Switch_customTypeface, 1)
            styledAttrs.recycle()
            val typeface = TypefaceLoader.get(context, CommonUtils.getFontName(index))
            setTypeface(typeface)
        }
    }
}
