package com.labware.lab.views

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatEditText
import com.labware.lab.R

class EditText : AppCompatEditText {

    private var mContext: Context? = null
    private val font = "Lato-Regular.ttf"

    constructor(context: Context) : super(context) {
        this.mContext = context
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        this.mContext = context
        init(context, attrs)
    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    ) {
        this.mContext = context
        init(context, attrs)
    }

    private fun init() {
        if (!isInEditMode) {
            val typeface = TypefaceLoader[context!!, "fonts/$font"]
            setTypeface(typeface)
        }
    }

    private fun init(context: Context, attrs: AttributeSet) {
        if (!isInEditMode) {
            val styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.EditText)
            val index = styledAttrs.getInt(R.styleable.EditText_customTypeface, 1)
            styledAttrs.recycle()
            val typeface = TypefaceLoader[context, CommonUtils.getFontName(index)]
            setTypeface(typeface)
        }
    }
}
