package com.labware.lab.views;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.webkit.WebView;

public class ObservableWebView extends WebView {

    private final String TAG = getClass().getSimpleName();

    private ObservableWebInterface observableWebInteface;
    private int paddingOffset = 200;
    private boolean bottomReached;

    public ObservableWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ObservableWebView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ObservableWebView(Context context) {
        super(context);
    }

    public void setObservableScrollListener(Context context) {
        try {
            observableWebInteface = (ObservableWebInterface) context;
        } catch (ClassCastException ex) {
            Log.e(TAG, "UNABLE TO CAST CONTEXT TO EULAWebInterface");
            ex.printStackTrace();
            throw new ClassCastException();
        }
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        if (this.computeVerticalScrollRange() <= (this.computeVerticalScrollOffset() +
                this.computeVerticalScrollExtent() + this.paddingOffset)) {
            if (!bottomReached) {
                bottomReached = true;
                if (observableWebInteface != null)
                    observableWebInteface.atBottomOfScrollView(true);
            }
        } else {
            if (bottomReached) {
                bottomReached = false;
                if (observableWebInteface != null)
                    observableWebInteface.atBottomOfScrollView(false);
            }
        }
        super.onScrollChanged(l, t, oldl, oldt);
    }

    public interface ObservableWebInterface {
        void atBottomOfScrollView(boolean atBottom);
    }
}
