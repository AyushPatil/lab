package com.labware.lab.adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.storage.FirebaseStorage
import com.labware.lab.R
import com.labware.lab.activities.UpdateCollectionPointActivity
import com.labware.lab.constants.AppConstantUtils
import com.labware.lab.models.CollectionPointEvent
import com.labware.lab.models.Hospital
import com.labware.lab.models.Lab
import com.labware.lab.utils.AppAndroidUtilities
import com.labware.lab.utils.ProgressDialogManager
import com.squats.fittr.utils.LabResourceHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class CollectionPointAdapter(public val mList: ArrayList<Hospital>,var from:String) : RecyclerView.Adapter<CollectionPointAdapter.ViewHolder>() {

    var lattitude:String?=null
    var longitude:String?=null
    var labLattitude:String?=null
    var labLongitude:String?=null
    var geocoder: Geocoder? = null
    var addresses: List<Address>? = null
    var mContext:Context?=null
    private val selectCheck: ArrayList<Int> = ArrayList()
    private var mAuth: FirebaseAuth?=null
    private var mFirebaseDatabaseInstances:FirebaseFirestore?=null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mContext= parent.context
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_collection_point, parent, false)
        for (i in 0 until mList.size) {
            selectCheck.add(0)
        }
        return ViewHolder(view)
    }

    // binds the list items to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val ItemsViewModel = mList[position]
        holder.txt_collection_name?.text = ItemsViewModel.name
        geocoder = Geocoder(mContext, Locale.getDefault())
        addresses = geocoder!!.getFromLocation(
            mList?.get(position).address?.lattitude?.toDouble()!!,
            mList?.get(position).address?.longitude?.toDouble()!!, 1
        )
        lattitude = mList?.get(position).address?.lattitude?.toString()!!
        longitude = mList?.get(position).address?.longitude?.toString()!!
        val address = addresses?.get(0)?.getAddressLine(0)
        holder.txt_collection_address?.setText(address)

        mAuth= FirebaseAuth.getInstance()
        mFirebaseDatabaseInstances= FirebaseFirestore.getInstance()
        if(from.equals("HOME"))
        {
            holder.cb_collection_point?.visibility=View.GONE
        }

        val settings = FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true)
            .setCacheSizeBytes(FirebaseFirestoreSettings.CACHE_SIZE_UNLIMITED)
            .build()
        mFirebaseDatabaseInstances?.setFirestoreSettings(settings)

        holder.txt_collection_more?.setOnClickListener(View.OnClickListener {
            showEditDeleteOutBottomSheetDialog(mContext!!, position)
        })

        if (selectCheck[position] == 1) {
            holder.cb_collection_point?.setChecked(true)
        } else {
            holder.cb_collection_point?.setChecked(false)
        }

       holder.cb_collection_point?.setOnClickListener(View.OnClickListener {
            for (k in selectCheck.indices) {
                if (k == position) {
                    selectCheck[k] = 1
                    val docRef =
                        mFirebaseDatabaseInstances?.collection(AppConstantUtils.LAB)?.document(
                            mList.get(position).labId.replace(
                                "+91",
                                ""
                            )
                        )
                    docRef?.get()?.addOnSuccessListener { documentSnapshot ->
                        try {
                            val user = documentSnapshot.toObject(Lab::class.java)
                            labLattitude = user?.address?.lattitude?.toString()!!
                            labLongitude = user?.address?.longitude?.toString()!!

                            var originLatLng=labLattitude+"/"+labLongitude
                            var destinationLatLng=mList.get(position).address.lattitude+"/"+mList.get(position).address.longitude
                            EventBus.getDefault().postSticky(CollectionPointEvent("SHOW_SWIPE_TO_COLLECT", mList.get(position).phone, mList.get(position).labId,mList.get(position).name,"CollectionPointAdapter.kt",originLatLng,destinationLatLng))

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    docRef?.get()?.addOnFailureListener(OnFailureListener {})

                } else {
                    selectCheck[k] = 0
                }
            }
            notifyDataSetChanged()
        })

    }

    // return the number of the items in the list
    override fun getItemCount(): Int {
        return mList.size
    }

    // Holds the views for adding it to image and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        var txt_collection_name: com.labware.lab.views.TextView? = itemView.findViewById(R.id.txt_collection_name)
        var txt_collection_address: com.labware.lab.views.TextView?= itemView.findViewById(R.id.txt_collection_address)
        var txt_collection_more: com.labware.lab.views.TextView?= itemView.findViewById(R.id.txt_collection_more)
        var cb_collection_point: CheckBox?= itemView.findViewById(R.id.cb_collection_point)
    }

    private fun showEditDeleteOutBottomSheetDialog(mContext: Context, selectedPosition: Int) {
        val bottomSheetDialog = BottomSheetDialog(
            mContext,
            R.style.MyTransparentBottomSheetDialogTheme
        )
        bottomSheetDialog.setContentView(R.layout.bottom_sheet_edit_delete)
        val btnEdit = bottomSheetDialog.findViewById<TextView>(R.id.btn_edit)
        val btnDelete = bottomSheetDialog.findViewById<TextView>(R.id.btn_delete)
        val btnCancel = bottomSheetDialog.findViewById<TextView>(R.id.btn_cancel)
        btnEdit!!.setOnClickListener(View.OnClickListener {
            val intent = Intent(mContext, UpdateCollectionPointActivity::class.java)
            intent.putExtra(AppConstantUtils.TYPE, mList.get(selectedPosition).type)
            intent.putExtra(AppConstantUtils.PHONE, mList.get(selectedPosition).phone)
            mContext.startActivity(intent)
            AppAndroidUtilities.startBottomToTopAnimation(mContext as (Activity))
            bottomSheetDialog.dismiss()
        })
        btnDelete!!.setOnClickListener(View.OnClickListener {
            ProgressDialogManager.showProgressDialog(mContext)
            var documentId = mList.get(selectedPosition).phone + "_" + mAuth?.currentUser?.phoneNumber.toString().replace("+91","")
            Log.e("@@@documentID00000",documentId)
            showDeleteRecordBottomSheetDialog(mContext,documentId,selectedPosition)
            bottomSheetDialog.dismiss()
        })
        btnCancel!!.setOnClickListener(View.OnClickListener { bottomSheetDialog.dismiss() })
        bottomSheetDialog.show()
    }

    private fun showDeleteRecordBottomSheetDialog(mContext:Context,documentId:String,selectedPosition:Int) {
        val bottomSheetDialog = BottomSheetDialog(mContext!!, R.style.MyTransparentBottomSheetDialogTheme)
        bottomSheetDialog.setContentView(R.layout.bottom_sheet_logout_dialog)
        val btnSignout = bottomSheetDialog.findViewById<android.widget.TextView>(R.id.btn_yes)
        val txt_title = bottomSheetDialog.findViewById<android.widget.TextView>(R.id.txt_title)
        btnSignout?.text="Yes"
        txt_title?.text="Are you sure you want to delete this record?"
        val btnCancel = bottomSheetDialog.findViewById<android.widget.TextView>(R.id.btn_no)
        btnCancel?.text="No"
        btnSignout!!.setOnClickListener(View.OnClickListener {
            mFirebaseDatabaseInstances?.collection(mList.get(selectedPosition).type)?.document(documentId)!!.delete()
                .addOnSuccessListener {
                    ProgressDialogManager.dismissProgressDialog(mContext)
                    deleteFileFromFirebaseStorage(mList.get(selectedPosition).phone)
                    AppAndroidUtilities.showBanner(
                        mContext,
                        LabResourceHelper.getString(R.string.success_delete_alert),
                        1
                    )
                    if (mList.size != 0) {
                        mList.removeAt(selectedPosition)
                        notifyItemRemoved(selectedPosition)
                        notifyItemRangeChanged(selectedPosition, mList.size)
                    }
                    bottomSheetDialog.dismiss()
                }
                .addOnFailureListener {
                    ProgressDialogManager.dismissProgressDialog(mContext)
                    AppAndroidUtilities.showBanner(
                        mContext,
                        LabResourceHelper.getString(R.string.failure_delete_alert),
                        4
                    )
                    bottomSheetDialog.dismiss()
                }

        })
        btnCancel!!.setOnClickListener(View.OnClickListener { bottomSheetDialog.dismiss() })
        bottomSheetDialog.show()
    }

    fun deleteFileFromFirebaseStorage(fileName: String) {
        GlobalScope.launch {
            val user=FirebaseAuth.getInstance().currentUser
            Dispatchers.IO
            try {
                var firebaseStorage = FirebaseStorage.getInstance()
                var storageRef = firebaseStorage?.getReference()
                var riversRef = storageRef?.child("LabAudio/" + fileName +"_"+  user?.phoneNumber?.replace("+91","")  + ".wav")
                var deleteTask = riversRef?.delete()
                deleteTask?.addOnFailureListener(OnFailureListener {
                })?.addOnSuccessListener(OnSuccessListener<Any?> {
                })
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }
    }
}
