package com.labware.lab.adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.storage.FirebaseStorage
import com.labware.lab.R
import com.labware.lab.activities.ActivityHistory
import com.labware.lab.activities.ActivityViewHistory
import com.labware.lab.activities.UpdateCollectionPointActivity
import com.labware.lab.constants.AppConstantUtils
import com.labware.lab.models.CollectionPointEvent
import com.labware.lab.models.Hospital
import com.labware.lab.models.Lab
import com.labware.lab.models.Requests
import com.labware.lab.utils.AppAndroidUtilities
import com.labware.lab.utils.AppDateTimeUtils
import com.labware.lab.utils.ProgressDialogManager
import com.squats.fittr.utils.LabResourceHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.textColor
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class RequestsHistoryAdapter(public val mList: ArrayList<Requests>) : RecyclerView.Adapter<RequestsHistoryAdapter.ViewHolder>() {

    var mContext:Context?=null
    var geocoder: Geocoder? = null
    var destinationAddresses: List<Address>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mContext= parent.context
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_history, parent, false)
        geocoder = Geocoder( mContext, Locale.getDefault())
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val ItemsViewModel = mList[position]
        if(!ItemsViewModel.patientName.isNullOrEmpty())
        {
            holder.txt_patient_name?.text = ItemsViewModel.patientName
        }
        else
        {
            holder.txt_patient_name?.text = "NA"
        }
        holder.txt_request_id?.text = ItemsViewModel.requestId.substring(0,7)
        holder.txt_request_time?.text = ItemsViewModel.timestamp
        holder.txt_request_status?.text = ItemsViewModel.currentStatus.toUpperCase()
        if(ItemsViewModel.currentStatus.toUpperCase().equals("DISCARDED"))
        {
            holder.txt_request_status?.setTextColor(LabResourceHelper.getColor(R.color.red))
        }
        else
        {
            holder.txt_request_status?.setTextColor(LabResourceHelper.getColor(R.color.veg_color))
        }

        val msgArrayDestinationLatLng: Array<String> = ItemsViewModel.destinationLatLng.split("/".toRegex()).toTypedArray()
        destinationAddresses = geocoder!!.getFromLocation(
            msgArrayDestinationLatLng[0].toDouble()!!,
            msgArrayDestinationLatLng[1].toDouble()!!,1)
        val destinationAddress = destinationAddresses?.get(0)?.getAddressLine(0)
        holder.txt_request_address?.text = destinationAddress

        holder.ll_main_history?.setOnClickListener(View.OnClickListener {
            val intentmethods = Intent(mContext, ActivityViewHistory::class.java)
            intentmethods.putExtra("REQUEST_ID",ItemsViewModel.requestId)
            mContext?.startActivity(intentmethods)
            AppAndroidUtilities.startBottomToTopAnimation(mContext as Activity)
        })
    }

    // return the number of the items in the list
    override fun getItemCount(): Int {
        return mList.size
    }

    // Holds the views for adding it to image and text
    class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        var txt_patient_name: com.labware.lab.views.TextView? = itemView.findViewById(R.id.txt_patient_name)
        var txt_request_time: com.labware.lab.views.TextView? = itemView.findViewById(R.id.txt_request_time)
        var txt_request_address: com.labware.lab.views.TextView? = itemView.findViewById(R.id.txt_request_address)
        var txt_request_status: com.labware.lab.views.TextView? = itemView.findViewById(R.id.txt_request_status)
        var txt_request_id: com.labware.lab.views.TextView? = itemView.findViewById(R.id.txt_request_id)
        var ll_main_history: LinearLayout? = itemView.findViewById(R.id.ll_main_history)
    }

}
